from django.contrib import admin
from django.urls import path, include, re_path
from django.conf import settings
from django.conf.urls.static import static

from slack_message_api.api import ChallengesResource
from lead.api import LeadResource 
from tastypie.api import Api

#api config part
#challenges_resource = ChallengesResource()
v1_api = Api(api_name='v1')
v1_api.register(ChallengesResource())
v1_api.register(LeadResource())
 
admin.site.site_header	= 'UNEEDCOMMS'
admin.site.site_title	= 'UNEEDCOMMS'
admin.site.index_title	= 'UNEEDCOMMS'

urlpatterns = [
	re_path(r'^jet/', include('jet.urls', 'jet')),
    re_path(r'^jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    re_path(r'^tinymce/', include('tinymce.urls')),

    path('uneedadmin/', admin.site.urls, name='admin'),
	path('admin/', include('admin_honeypot.urls', namespace='admin_honeypot')),
    path('login/', include('admin_honeypot.urls', namespace='login_honeypot')),
    path('signin/', include('admin_honeypot.urls', namespace='signin_honeypot')),

	path('api/', include(v1_api.urls)),
	path('uneedadmin/doc/', include('django.contrib.admindocs.urls')),

# keepgrow route
	path('', include('keepgrow.urls', namespace='keepgrow')),
	path('grow/', include('lead.urls', namespace='lead')),
]

#if settings.DEBUG:
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)




