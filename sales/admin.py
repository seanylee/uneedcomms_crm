from django.contrib import admin
from django.db.models import Sum, Count
from django.db import models
from django.forms import Textarea
from django.urls import path
from django.utils import timezone
from .models import DealObject, DealMeta, DealStage
from crm.models import EventLog
from adminsortable2.admin import SortableAdminMixin, SortableInlineAdminMixin
from django.template.response import TemplateResponse
from django.utils.safestring import mark_safe

#=========experiment========
from crm.models import ContactObject, CompanyObject
from django.shortcuts import render
from django.views.generic import ListView
from main.conf import slack_msg
from bs4 import BeautifulSoup


class DealSuccess(DealObject):
    class Meta:
        proxy = True

class DealFail(DealObject):
    class Meta:
        proxy = True

class DealObjectDealStageInlineForEdit(admin.StackedInline):
    model					= DealStage
    verbose_name			= '거래 상세 (최대 10개)'
    verbose_name_plural		= '거래 상세 (최대 10개)'
    extra					= 0
    max_num					= 15
    show_change_link		= True
    fields					= [c.name for c in DealStage._meta.fields]

class DealObjectDealStageInlineForOrder(SortableInlineAdminMixin, admin.TabularInline):
    model					= DealStage
    verbose_name			= '거래 순서(최대 10개)'
    verbose_name_plural		= '거래 순서(최대 10개)'
    extra					= 0
    max_num					= 15
    show_change_link		= False
    fields					= ('order','deal_meta','deal_stage_name','expected_stage_duration',
								'deal_probability','deal_stage_policy',)

class DealObjectEventLogInline(admin.TabularInline):
    model                   = EventLog
    verbose_name            = '활동 로그'
    verbose_name_plural     = '활동 로그'
    extra                   = 0
    fields                  = ('event_created_at','event_related_company','event_type','event_contents')
    readonly_fields         = ('event_created_at','event_related_company')
     
#    def contact_related_event(self, instance):
#        return instance.deal_contact
#    contact_related_event.allow_tags            = True
#    contact_related_event.short_description     = '직원'
 
#    def company_related_event(self, instance):
#        return instance.deal_company
#    company_related_event.allow_tags            = True
#    company_related_event.short_description     = '업체'

    def get_queryset(self, request):
        return super(DealObjectEventLogInline,self).get_queryset(request).filter(event_hierarchy='deal')


@admin.register(DealSuccess)
class DealSuccessAdmin(admin.ModelAdmin):
    class Meta:
        model = DealObject

    list_display			= ('deal_status','deal_customer','deal_pipeline','amount_pprint','before_tax_pprint',
								'after_tax_pprint','deal_owner','successed_at',)
    list_display_links		= ('deal_status','deal_customer','deal_pipeline',)
    list_filter				= ('deal_pipeline','deal_owner','currency',)
    search_fields			= ('amount','deal_customer__name','deal_company__company_name',)
    list_per_page           = 50
    empty_value_display     = '-'
    date_hierarchy          = 'created_at'
    readonly_fields			= ('deal_company','deal_pipeline','deal_customer','amount_pprint','currency','deal_stage',
								'deal_owner','updated_at','created_at','successed_at','commission_pprint',
								'before_tax_pprint','after_tax_pprint','deal_type','deal_renew_period',
								'commission_rate','rate_pprint',)
    fieldsets = (   
                    ('결제 상세 정보',{
                        'fields': ('deal_company','deal_customer','deal_owner','deal_pipeline','deal_type',
									'deal_renew_period','amount_pprint','currency','rate_pprint','commission_pprint',
									'before_tax_pprint','after_tax_pprint','successed_at','created_at',),
                        'description':('결제 완료 정보 입니다.'),
                        }
                    ),
                )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

# =============================|Custom|================================

    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(request, extra_context)
        try:
            qs = response.context_data['cl'].queryset
        except (AttributeError, KeyError):
            return response

        metrics = {
            'total': Sum('amount'),
            'total_sales': Sum('amount')
		}

        response.context_data['summary'] = list(
			qs.values('amount').annotate(**metrics).order_by('-id')
		  )
        response.context_data['summary_total'] = dict(
			qs.aggregate(**metrics)
		  )
        return response

# ====================================================================

    def rate_pprint(self, dealobject):
        try:
            raw_rate = int(dealobject.commission_rate)
        except:
            raw_rate = 0
        pprint = f'{raw_rate} %'
        return pprint
    rate_pprint.short_description = '수수료율(%)'

    def commission_pprint(self, dealobject):
        try:
            raw_amount = int(round(dealobject.commission_amount))
        except:
            raw_amount = 0
        raw_currency = dealobject.currency
        pprint = ('{0:,d}'.format(raw_amount)+' '+raw_currency)
        return pprint
    commission_pprint.short_description = '수수료'
    commission_pprint.help_text = '수수료율이 반영된 수수료 금액입니다.'

    def before_tax_pprint(self, dealobject):
        try:
            raw_amount = int(round(dealobject.before_tax_amount))
        except:
            raw_amount = 0
        raw_currency = dealobject.currency
        pprint = ('{0:,d}'.format(raw_amount) + ' ' + raw_currency)
        return pprint
    before_tax_pprint.short_description='부가세 이전 금액'
    before_tax_pprint.help_text = '부가세 이전 금액입니다. (서비스 금액 + 수수료)'

    def after_tax_pprint(self, dealobject):
        try:
            raw_amount = int(round(dealobject.after_tax_amount))
        except:
            raw_amount = 0
        raw_currency = dealobject.currency
        pprint = ('{0:,d}'.format(raw_amount) + ' ' + raw_currency)
        return pprint
    after_tax_pprint.short_description='부가세 포함 금액'
    after_tax_pprint.help_text = '부가세 포함 금액입니다. (부가세 이전 금액 + 부가세 10%)'

    def amount_pprint(self, dealobject):
        try:
            raw_amount = int(round(dealobject.amount))
        except:
            raw_amount = 0
        raw_currency = dealobject.currency
        pprint = ('{0:,d}'.format(raw_amount) + ' ' + raw_currency)
        return pprint
    amount_pprint.short_description = '서비스 금액'
    amount_pprint.help_text = '서비스 금액입니다.'

    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        extra_context['show_save'] = False
        return super(DealSuccessAdmin, self).changeform_view(request, object_id, extra_context=extra_context)

    def get_queryset(self, request):
        return super(DealSuccessAdmin,self).get_queryset(request).filter(deal_status="success")

@admin.register(DealFail)
class DealFailAdmin(admin.ModelAdmin):
    class Meta:
        model = DealObject

    list_display            = ('deal_status','fail_type','deal_customer','deal_pipeline','amount_pprint',
								'before_tax_pprint','after_tax_pprint','deal_owner','failed_at',)
    list_display_links      = ('deal_status','fail_type','deal_customer','deal_pipeline',)
    list_filter             = ('deal_pipeline','deal_owner','fail_type','currency',)
    search_fields           = ('amount','deal_customer__name','deal_company__company_name',)
    list_per_page           = 50
    empty_value_display     = '-'
    date_hierarchy          = 'created_at'
    readonly_fields         = ('deal_pipeline','deal_customer','amount_pprint','currency','deal_stage','deal_owner',
								'failed_at','updated_at','created_at','before_tax_pprint','after_tax_pprint',
								'commission_pprint','deal_company')
    fieldsets = (
                    (
                     '결제 상세 정보',{
                           'fields': ('deal_pipeline','deal_company','deal_customer','deal_owner',
										'fail_type','fail_detail','amount_pprint','commission_pprint',
										'before_tax_pprint','after_tax_pprint','failed_at','created_at',),
                            'description':('결제 실패 정보 입니다.'),
                        }
                    ),
                )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        extra_context['show_save'] = True
        return super(DealFailAdmin, self).changeform_view(request, object_id, extra_context=extra_context)

    def get_queryset(self, request):
        return super(DealFailAdmin,self).get_queryset(request).filter(deal_status="fail")

    def rate_pprint(self, dealobject):
        try:
            raw_rate = int(dealobject.commission_rate)
        except:
            raw_rate = 0
        pprint = f'{raw_rate} %'
        return pprint
    rate_pprint.short_description = '수수료율(%)'

    def commission_pprint(self, dealobject):
        try:
            raw_amount = int(round(dealobject.commission_amount))
        except:
            raw_amount = 0
        raw_currency = dealobject.currency
        pprint = ('{0:,d}'.format(raw_amount)+' '+raw_currency)
        return pprint
    commission_pprint.short_description = '수수료'
    commission_pprint.help_text = '수수료율이 반영된 수수료 금액입니다.'

    def before_tax_pprint(self, dealobject):
        try:
            raw_amount = int(round(dealobject.before_tax_amount))
        except:
            raw_amount = 0
        raw_currency = dealobject.currency
        pprint = ('{0:,d}'.format(raw_amount) + ' ' + raw_currency)
        return pprint
    before_tax_pprint.short_description='부가세 이전 금액'
    before_tax_pprint.help_text = '부가세 이전 금액입니다. (서비스 금액 + 수수료)'

    def after_tax_pprint(self, dealobject):
        try:
            raw_amount = int(round(dealobject.after_tax_amount))
        except:
            raw_amount = 0
        raw_currency = dealobject.currency
        pprint = ('{0:,d}'.format(raw_amount) + ' ' + raw_currency)
        return pprint
    after_tax_pprint.short_description='부가세 포함 금액'
    after_tax_pprint.help_text = '부가세 포함 금액입니다. (부가세 이전 금액 + 부가세 10%)'

    def amount_pprint(self, dealobject):
        try:
            raw_amount = int(round(dealobject.amount))
        except:
            raw_amount = 0
        raw_currency = dealobject.currency
        pprint = ('{0:,d}'.format(raw_amount) + ' ' + raw_currency)
        return pprint
    amount_pprint.short_description = '서비스 금액'
    amount_pprint.help_text = '서비스 금액입니다.'

@admin.register(DealObject)
class DealObjectAdmin(admin.ModelAdmin):
    event_contents_overrides = {
        models.TextField: {'widget': Textarea(attrs={'rows':2, 'cols':30})},
    }
    class Meta:
        model = DealObject

    raw_id_fields			= ('deal_customer',)
    list_display			= ('deal_customer','deal_pipeline','before_tax_pprint','deal_stage','deal_owner',
								'created_at','days_since_creation')
    list_display_links		= ('deal_customer','deal_pipeline')
    list_filter				= ('deal_pipeline', 'deal_stage','deal_owner','currency',)
    search_fields			= ('amount','deal_customer__name','deal_company__company_name',)
    list_per_page			= 50
    empty_value_display		= '-'
    list_editable			= ('deal_stage',)
    date_hierarchy          = 'created_at'
    inlines					= (DealObjectEventLogInline,)
    radio_fields			= {'deal_stage': admin.HORIZONTAL}
    readonly_fields			= ('created_at','updated_at','commission_pprint','before_tax_pprint','after_tax_pprint',
								'commission_pprint')
    actions                 = ('dealsuccess','dealfail','dealduplicate')
    fieldsets = (
					(
					 '거래 상세 정보',{
							'fields': ('deal_company','deal_customer','deal_stage','deal_owner','deal_pipeline',
										('deal_type','deal_renew_period'),('amount','currency'), 
										'commission_rate','commission_pprint','before_tax_pprint','after_tax_pprint',
										'created_at','updated_at',),
							'description':('거래 상세 정보 입니다.'),
						}
					),
				)

    def get_queryset(self, request):
        return super(DealObjectAdmin,self).get_queryset(request).filter(deal_status="ongoing")

    def dealsuccess(self, request, queryset):
        rows_updated = queryset.update(deal_status="success", successed_at=timezone.now())
        self.message_user(request, f'{rows_updated} 개의 항목에 대해 결제가 완료 처리 되었습니다.')
    dealsuccess.short_description = "선택된 항목들에 대해 결제완료처리 합니다."

    def dealfail(self, request, queryset):
        rows_updated = queryset.update(deal_status="fail",failed_at=timezone.now())
        self.message_user(request, 
			(f'{rows_updated} 개의 항목에 대해 결제가 실패 처리 되었습니다.') + 
			('\"결제실패\" 탭에서 실패 상세 내역을 입력해 주시기 바랍니다.'))
    dealfail.short_description = "선택된 항목들에 대해 결제실패처리 합니다."

    def dealduplicate(self, request, queryset):
        for object in queryset:
            object.id = None
            object.save()
        self.message_user(request, '거래가 성공적으로 복사되었습니다.')
    dealduplicate.short_description = "선택된 항목들에 대해 거래복사를 진행합니다."

    def days_since_creation(self, dealobject):
        diff_days = timezone.now().date() - dealobject.created_at
        return f'{diff_days.days} 일째'
    days_since_creation.short_description='경과일'

    def commission_pprint(self, dealobject):
        try:
            raw_amount = int(round(dealobject.commission_amount))
        except:
            raw_amount = 0
        raw_currency = dealobject.currency
        pprint = ('{0:,d}'.format(raw_amount)+' '+raw_currency)
        return pprint
    commission_pprint.short_description='수수료'

    def before_tax_pprint(self, dealobject):
        try:
            raw_amount = int(round(dealobject.before_tax_amount))
        except:
            raw_amount = 0
        raw_currency = dealobject.currency
        pprint = ('{0:,d}'.format(raw_amount) + ' ' + raw_currency)
        return pprint
    before_tax_pprint.short_description='부가세 이전 금액'

    def after_tax_pprint(self, dealobject):
        try:
            raw_amount = int(round(dealobject.after_tax_amount))
        except:
            raw_amount = 0
        raw_currency = dealobject.currency
        pprint = ('{0:,d}'.format(raw_amount) + ' ' + raw_currency)
        return pprint
    after_tax_pprint.short_description='부가세 포함 최종 금액'

    def save_formset(self, request, form, formset, change):
        target = str(form)
        soup = BeautifulSoup(target)
        company_id = soup.find("select", {"id" : "id_deal_company"}).find('option', {'selected':True})
        company_id_value = company_id['value']

        contact_id = soup.find("input", {"id" : "id_deal_customer"})
        contact_id_value = contact_id['value']

        instances = formset.save(commit=False)
        for instance in instances:
            instance.event_created_at = timezone.now
            instance.event_created_by = request.user
            instance.event_hierarchy = 'deal'
            instance.event_related_company_id = company_id_value
            instance.event_related_contact_id = contact_id_value
            instance.save()
        formset.save_m2m()

    def get_urls(self):
        urls                = super(DealObjectAdmin, self).get_urls()
        dealobject_urls		= [
								path('calendar/', self.admin_site.admin_view(self.calendar_integration)),
								path('experiment/',self.admin_site.admin_view(self.index)),
								]
        return dealobject_urls + urls

    def calendar_integration(self, request):
        context = dict(
            self.admin_site.each_context(request),
        )
        return TemplateResponse(request, "sales/calendar.html", context)

#==========experience==========	
    def index(self, request):
        return render(request, 'sales/index.html')

@admin.register(DealMeta)
class DealMetaAdmin(SortableAdminMixin, admin.ModelAdmin):
    class Meta:
        model =					DealMeta
    verbose_name_plural =	'서비스 상세'
    verbose_name =			'서비스 상세'
    list_display =			('order','domestic_service_name','international_service_name',
							'service_code','service_version','domestic_is_activate','international_is_activate',
							'engineer', 'released_at',)
    list_display_links =	('domestic_service_name','international_service_name',)
    fieldsets = (
					(
					'서비스 기본 정보', {
						'fields':
							(('domestic_is_activate', 'international_is_activate',),
							 ('domestic_service_name','international_service_name',),
							 ('service_code','service_version',),'engineer', 'released_at'
							),
						}
					),
				)

@admin.register(DealStage)
class DealStageAdmin(admin.ModelAdmin):
    class Meta:
        model = DealStage
    
    list_display = [c.name for c in DealStage._meta.fields]





# =============================== TEST =============================


from django.contrib.admin.views.main import ChangeList
from django.db.models import Sum, Avg
 
 
class TotalAveragesChangeList(ChangeList):
    #provide the list of fields that we need to calculate averages and totals
    fields_to_total = ['amount', 'total_sum', 'paid_by_cash',
                       'paid_by_transfer',]
 
    def get_total_values(self, queryset):
        """
        Get the totals
        """
        #basically the total parameter is an empty instance of the given model
        total =  MyModelName()
        total.custom_alias_name = "Totals" #the label for the totals row
        for field in self.fields_to_total:
            setattr(total, field, queryset.aggregate(Sum(field)).items()[0][1])
        return total
 
 
    def get_average_values(self, queryset):
        """
        Get the averages
        """
        average =  MyModelName()
        average.custom_alias_name = "Averages" #the label for the averages row
 
        for field in self.fields_to_total:
            setattr(average, field, queryset.aggregate(Avg(field)).items()[0][1])
        return average
 
 
    def get_results(self, request):
        """
        The model admin gets queryset results from this method
        and then displays it in the template
        """
        super(TotalAveragesChangeList, self).get_results(request)
        #first get the totals from the current changelist
        total = self.get_total_values(self.query_set)
        #then get the averages
        average = self.get_average_values(self.query_set)
        #small hack. in order to get the objects loaded we need to call for 
        #queryset results once so simple len function does it
        len(self.result_list)
        #and finally we add our custom rows to the resulting changelist
        self.result_list._result_cache.append(total)
        self.result_list._result_cache.append(average)



