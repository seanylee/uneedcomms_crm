from django.db import models
from django.utils.text import slugify
from tastypie.utils.timezone import now
from django.contrib.postgres.fields import JSONField


class Challenges(models.Model):
    token = models.TextField(null=True, blank=True)
    team_id = models.TextField(blank=True)
    api_app_id = models.TextField(blank=True)
    event = JSONField(blank=True,null=True)
    type = models.TextField(blank=True)
    event_id = models.TextField(blank=True)
    event_time = models.TextField(blank=True)
    event_received_time = models.DateTimeField(default=now)
    challenge = models.TextField(null=True, blank=True)
    
    def __str__(self):
        return self.token
    
    def save(self, *args, **kwargs):       
        return super(Challenges, self).save(*args, **kwargs)
