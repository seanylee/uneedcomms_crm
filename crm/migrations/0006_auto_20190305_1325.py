# Generated by Django 2.1.5 on 2019-03-05 13:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0005_remove_companyobject_mannual_validation'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='ContactEvent',
            new_name='EventLog',
        ),
        migrations.AlterModelOptions(
            name='eventlog',
            options={'ordering': ['-id'], 'verbose_name': 'Event', 'verbose_name_plural': 'Events'},
        ),
    ]
