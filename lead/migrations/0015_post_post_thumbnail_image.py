# Generated by Django 2.1.5 on 2019-03-13 01:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lead', '0014_auto_20190312_0115'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='post_thumbnail_image',
            field=models.ImageField(blank=True, null=True, upload_to=''),
        ),
    ]
