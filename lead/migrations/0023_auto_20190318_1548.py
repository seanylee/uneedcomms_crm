# Generated by Django 2.1.5 on 2019-03-18 15:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lead', '0022_auto_20190318_1452'),
    ]

    operations = [
        migrations.AddField(
            model_name='lead',
            name='lead_demo_request',
            field=models.BooleanField(default=False, verbose_name='데모를 요청합니다.'),
        ),
        migrations.AddField(
            model_name='lead',
            name='lead_marketing_agree',
            field=models.BooleanField(default=False, verbose_name='제품, 서비스 및 이벤트 관련 마케팅 정보를 수신하고 싶습니다.'),
        ),
    ]
