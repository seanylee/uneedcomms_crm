from pyhive import presto
from slackclient import SlackClient
import requests
import json
import re
#import sqlite3

# ===| DB config |===

# 메인 쿼리서버 (SYSTEM 체크시에만 사용하고 그 외 모든 메인 쿼리는 DS 서버에서 처리)
class Presto():
    def __init__(self):
        self.presto_cursor = presto.connect(
            host='ims.presto.uneedcomms.net',
            port='9090',
            username='Slack Bot_Q',
            catalog='hive'
        ).cursor()

# DS 전용 쿼리서버 (MODEL의 하위 레이어에서 사용하는 쿼리는 아래 설정값을 이용! 인스턴스 생성!)
dummy = '''
class Presto():
    def __init__(self):
        self.presto_cursor = presto.connect(
            host='ds-presto.smartdw.private',
            port=8080,
            username='Slack Bot_Q',
            catalog='hive'
        ).cursor()
'''

# Slacker 웹훅 상세정보! (웹훅에서는 텍스트만 전송가능하므로, 데이터 타입 == str 을 확인해준다.)
def slack_msg(input_text):
    if isinstance(input_text,str):
        msg_locate = input_text
        # webhook_url = 'https://hooks.slack.com/services/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'
        webhook_url = 'https://hooks.slack.com/services/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'
        slack_data = {'text': msg_locate}
        response = requests.post(
            webhook_url, data=json.dumps(slack_data),
            headers={'Content-Type': 'application/json'}
            )
        if response.status_code != 200:
            raise ValueError(
                'Request to slack returned an error %s, the response is:\n%s'
                % (response.status_code, response.text)
                )
        pass
    else:
        input_text = str(input_text)
        slack_msg(input_text)
        pass

def slack_msg_test(input_text):
    if isinstance(input_text,str):
        msg_locate = input_text
        webhook_url = 'https://hooks.slack.com/services/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'
        slack_data = {'text': msg_locate}
        response = requests.post(
            webhook_url, data=json.dumps(slack_data),
            headers={'Content-Type': 'application/json'}
            )
        if response.status_code != 200:
            raise ValueError(
                'Request to slack returned an error %s, the response is:\n%s'
                % (response.status_code, response.text)
                )
        pass
    else:
        input_text = str(input_text)
        slack_msg_test(input_text)
        pass

# Slacker JSON 웹훅! (위의경우 :: 텍스트만 전송)
def slack_json_msg(input_text):
    webhook_url = 'https://hooks.slack.com/services/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'
    response = requests.post(
    webhook_url, data=json.dumps(input_text),
    headers={'Content-Type': 'application/json'}
    )
    if response.status_code != 200:
        raise ValueError(
            'Request to slack returned an error %s, the response is:\n%s'
            % (response.status_code, response.text)
            )

# 허브스팟 API 상세정보
def hubspot_api(api_type,associated_id):
    if api_type == "get_a_company":
        api_get = "https://api.hubapi.com/companies/v2/companies/{company_id}?hapikey=86900c82-939a-44a7-bada-b09e02d289fc".format(company_id = associated_id)
        return api_get
        pass
    elif api_type == "company_portal":
        company_portal_url = "https://app.hubspot.com/contacts/2800399/company/{company_id}".format(company_id = associated_id)
        return company_portal_url
        pass


# =================| package_list |==============

# import re
# import os
# import sys
# import urllib.request
# from urllib.request import urlopen
# import urllib.request, json 
# import requests
# import json
# from pprint import pprint
# from bs4 import BeautifulSoup
# from time import time
# import time
# from datetime import datetime
# import pytz
# import numpy as np
# import random
# from TCLIService.ttypes import TOperationState
# from pyhive import presto
# from slackclient import SlackClient
