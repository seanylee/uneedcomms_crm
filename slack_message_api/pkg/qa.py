from .configurations import *
from .queries import *
from .comments import *
from datetime import datetime, timedelta
import pytz


# 입력 단어 3개
def qa_f(*args):
    if len(args[0]) == 2:
        if args[0][1] == "job_fail":
            failed_job = job_failure()
            qa_comment_list = list()
            for job in failed_job:    
                # ims_app_id = job[0]
                # instance_job_idx = job[1]
                # job_idx = job[2]
                # data_table = job[3]
                # pipeline_stage_number = job[4]
                # pipeline_stage_name = job[5]
                # instance_reg_date = job[6]
                qa_comment_list.append(qa_comment("job_failure",job[0],job[1],job[2],job[3],job[4],job[5],job[6]))
            fail_cnt = len(qa_comment_list)
            
            tz = pytz.timezone("Asia/Seoul")
            yesterday = datetime.now(tz).date() - timedelta(days=1)

            header_comment = "*{} 일에 발생한 작지 오류 : {} 건 입니다.*\n파이프라인코드가 40번과 112번이 아닌 모든 경우 에러로 간주\n```ims_app_id || instance_job_idx || job_idx || data_table || pipeline_stage_name(Number)```".format(yesterday,fail_cnt)
            slack_msg(header_comment + '\n' + (",".join(qa_comment_list)))


        if args[0][1] == "data_fail":
            failed_job = job_failure()
            qa_comment_list = list()
            for job in failed_job:    
                # ims_app_id = job[0]
                # instance_job_idx = job[1]
                # job_idx = job[2]
                # data_table = job[3]
                # pipeline_stage_number = job[4]
                # pipeline_stage_name = job[5]
                # instance_reg_date = job[6]
                qa_comment_list.append(qa_comment("job_failure",job[0],job[1],job[2],job[3],job[4],job[5],job[6]))
            fail_cnt = len(qa_comment_list)
            
            tz = pytz.timezone("Asia/Seoul")
            yesterday = datetime.now(tz).date() - timedelta(days=1)

            header_comment = "*{} 일에 발생한 작지 오류 : {} 건 입니다.*\n파이프라인코드가 40번과 112번이 아닌 모든 경우 에러로 간주\n```ims_app_id || instance_job_idx || job_idx || data_table || pipeline_stage_name(Number)```".format(yesterday,fail_cnt)
            slack_msg(header_comment + '\n' + (",".join(qa_comment_list)))



    elif len(args[0]) == 3:
        if args[0][1] in Active_domain():
            if args[0][2] == "on":
                slack_msg("```업체 \"{}\" 의 샌더 URL을 빌드중입니다. (워크플로우 ON 조건)```".format(args[0][1]))
                slack_msg("쨔쟌")
                pass

            elif args[0][2] == "off":
                slack_msg("```업체 \"{}\" 의 샌더 URL을 빌드중입니다. (워크플로우 OFF 조건)```".format(args[0][1]))
                slack_msg("쨔쟌")
                pass

            else:
                not_get_comm = not_get_comment()
                slack_msg(not_get_comm)
                pass
            pass
        else:
            slack_msg("`존재하지 않는 ims_app_id 입니다. 다시 확인후 입력해 주세요.`")
            pass

    # 입력 단어 3
    elif len(args[0]) == 4:
        if args[0] == "who are you":
            slack_msg("hi")
            pass

    else:
        not_get_comm = not_get_comment()
        slack_msg(not_get_comm)
