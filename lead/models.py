#-*-coding: utf-8

from django.db import models
from ckeditor.fields import RichTextField
from django.conf import settings # import to call user object
from django.urls import reverse
from django.utils.text import slugify

from django.utils.safestring import mark_safe

# Validation Area
from django import forms
def clean_corporation(value):
    if any(form_input in value for form_input in ('(주)', '주식회사','Inc.','Ltd.','ltd.','Co.')):
        raise forms.ValidationError(" \"(주)\", \"주식회사\", \"Ltd\"등을 제외한 순수 법인명을 입력해 주세요")

def clean_company(value):
    # ORM => Validation 만약 컴퍼니가 있는지?
    if any(form_input in value for form_input in ('(주)', '주식회사','Inc.','Ltd.','ltd.','Co.')):
        raise forms.ValidationError(" \"(주)\", \"주식회사\", \"Ltd\"등을 제외한 순수 업체명을 입력해 주세요")

def clean_website_url(value):
    if 'http' not in value:
        raise forms.ValidationError("\"http://\" 혹은 \"https://\" 를 입력해 주세요.")

#def agreement(value):
#    if not value:
#        raise forms.ValidationError("수신")

class Lead(models.Model):
    class Meta:
        verbose_name			= 'Lead'
        verbose_name_plural		= 'Leads'
        ordering				= ['-id']

    lead_company_corporation = models.CharField('법인명', null=True, blank=True, max_length=50, validators=[clean_corporation])
    lead_company_name = models.CharField('업체명', null=True, blank=True, max_length=50, validators=[clean_company])
    lead_company_website_url = models.CharField('웹사이트 주소', null=True, blank=True, max_length=200, validators=[clean_website_url])

    lead_contact_department = models.CharField('부서명', null=True, blank=True, max_length=50,)
    lead_contact_name = models.CharField('이름', null=False, blank=False, max_length=50,)
    lead_contact_title = models.CharField('직책', null=True, blank=True, max_length=50,)
    lead_contact_email = models.EmailField('Email', null=False, blank=False,)
    lead_contact_phone = models.CharField('휴대전화', null=True, blank=True, max_length=50,)

    lead_event_interests = models.CharField('관심서비스', null=True, blank=True, max_length=100,)
    lead_event_inquiry = models.TextField('상세 문의사항', null=True, blank=True,)
    lead_event_referrer = models.CharField('리퍼러', null=True, blank=True, max_length=600,)
    lead_event_location = models.CharField('제출 위치', null=True, blank=True, max_length=600,)
    lead_event_location_title = models.CharField('제출 위치 제목', null=True, blank=True, max_length=100,)
    lead_event_submitted_at = models.DateTimeField('제출일', null=True, blank=True, auto_now_add=True,)
    lead_event_user_agent = models.CharField('User_Agent', null=False, blank=False, max_length=400,)
    # IP4 & IP6 모두 호환
    lead_event_ipaddress = models.GenericIPAddressField('IP Address', null=False, blank=False, protocol='both')
	
    lead_marketing_agree = models.BooleanField('마케팅 수신동의', default=False)
    lead_demo_request = models.BooleanField('데모 요청', default=False)
    def __str__(self):
        return f'{self.lead_company_name} - {self.lead_contact_name} {self.lead_contact_title}' 


class Subscription(models.Model):
    class Meta:
        verbose_name			= 'Subscription'
        verbose_name_plural		= 'Subscriptions'
        ordering				= ['-id']

    subs_corporation = models.CharField('법인명', null=True, blank=True, max_length=50,)
    subs_company = models.CharField('업체명', null=False, blank=False, max_length=50,)
    subs_department = models.CharField('부서명', null=True, blank=True, max_length=50,)
    subs_name = models.CharField('이름', null=False, blank=False, max_length=50,)
    subs_title = models.CharField('직책', null=True, blank=True, max_length=50,)
    subs_email = models.EmailField('Email', null=False, blank=False,)
    subs_phone = models.CharField('휴대전화', null=True, blank=True, max_length=50,)
    subs_interests = models.CharField('관심서비스', null=True, blank=True, max_length=100,)
    subs_inquiry = models.TextField('상세 문의사항', null=True, blank=True,)
    subs_referrer = models.CharField('리퍼러', null=True, blank=True, max_length=300,)
    subs_location = models.CharField('제출 위치', null=True, blank=True, max_length=300,)
    subs_location_title = models.CharField('제출 위치 제목', null=True, blank=True, max_length=100,)
    subs_submitted_at = models.DateTimeField('제출일', null=True, blank=True, auto_now_add=True,)
    # IP4 & IP6 모두 호환
    subs_ipaddress = models.GenericIPAddressField('IP Address', null=False, blank=False, protocol='both')

    def __str__(self):
        return f'{self.subs_company} - {self.subs_name} {self.subs_title}'

class Post(models.Model):
    class Meta:
        verbose_name			= 'Post'
        verbose_name_plural		= 'Posts'
        ordering				= ['-id']

    post_title = models.CharField('제목', null=False, blank=False, max_length=100, help_text='제목 (100자 이내)')
    post_body = RichTextField('본문', null=False, blank=False, help_text='본문 입력')
    post_keywords = models.CharField('키워드', null=True, blank=True, max_length=100, 
										help_text='키워드 입력 (콤마로 구분), 내부 키워드 검색시 사용')
    post_publication = models.BooleanField('게시 여부', default=False, help_text='게시 활성 여부')
    post_registered_by = models.ForeignKey(settings.AUTH_USER_MODEL,verbose_name='게시물 등록자',
										null=False, blank=False, on_delete=models.DO_NOTHING,default=4, help_text='게시물 등록자 선택',)
    post_thumbnail = models.ImageField('썸네일 이미지',null=True,blank=True, help_text='썸네일 이미지 등록') 
    post_preview = models.CharField('미리보기', null=False, blank=False, max_length=300, 
										help_text='게시물 미리보기 작성(최대 200자)')
    post_type = models.CharField('게시물 유형', null=False, blank=False, max_length=50,)
    post_exposed_date = models.DateField('게시일자', null=False, blank=False,)
    post_created_at = models.DateField('최초 생성일', null=True, blank=True, auto_now_add=True,)
    post_updated_at = models.DateField('최근 수정일', null=True, blank=True, auto_now=True,)
#    def save(self):
#        self.slug = slugify(self.post_title)
#        super(Post, self).save()

    def __str__(self):
        return self.post_title

#    def get_absolute_url(self):
#        return reverse('lead:post_detail', args=[self.pk])

class Scripting(models.Model):
    class Meta:
        verbose_name			= 'Script'
        verbose_name_plural		= 'Scripts'
        ordering				= ['-id']
	
    script_title = models.CharField('스크립트 이름', null=False, blank=False, max_length=50, 
										help_text='식별 가능한 스크립트 이름 입력(주석처리 후 소스에 삽입됩니다.)')
    script_body = models.TextField('스크립트', null=False, blank=False, 
									help_text=mark_safe("스크립트 입력 => 반드시 \'script\' 태그를 입력하십시오."),)
    script_expose = models.BooleanField('활성여부', null=False, blank=False, default=False, 
										help_text='KeepGrow 페이지 내 공통 스크립트 활성화 여부')
    script_created_at = models.DateField('최초 생성일', null=True, blank=True, auto_now_add=True,)
    script_updated_at = models.DateField('최근 수정일', null=True, blank=True, auto_now=True,)

    def __str__(self):
        return self.script_title
