# Generated by Django 2.1.5 on 2019-03-20 05:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0017_auto_20190320_0504'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ticketobject',
            name='ticket_number',
            field=models.CharField(default='RiVUXFHTZ4M79TMfp1oP', max_length=22, verbose_name='티켓번호'),
        ),
    ]
