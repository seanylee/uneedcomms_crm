from .configurations import *
from .queries import *
from .comments import *
from .column_matching import *
import time
from datetime import datetime
import pytz
import numpy as np

def data_f(*args):
    if len(args[0]) == 2:
# ===========================[2] 입력 단어 "" ==============================
        if args[0][1] == "goal":
            d_comment = data_comment("goal")
            slack_msg(d_comment)
            pass
        elif args[0][1] == "model":
            d_comment = data_comment("model")
            slack_msg(d_comment)
            pass
        elif args[0][1] == "dict":
            d_comment = data_comment("dict")
            slack_msg(d_comment)
            pass
        else:
            not_get_comm = not_get_comment(2, *args)
            return not_get_comm
            pass 
# ===========================[3] 입력 단어 "" ==============================

    elif len(args[0]) == 3:
        if args[0][1] == "dict":
        	if args[0][2] == "user_table":
        		d_comment = data_comment("user_table_dict")
        		return d_comment
        		pass


        elif args[0][1] == "convert":
            if args[0][2] == "user":
                d_comment = data_comment(*args)
                slack_msg(d_comment)
                pass

            elif args[0][2] == "visit":
                d_comment = data_comment(*args)
                slack_msg(d_comment)
                pass
            elif args[0][2] == "buy":
                d_comment = data_comment(*args)
                slack_msg(d_comment)
                pass
            else:
                not_get_comm = not_get_comment(*args)
                return not_get_comm
                pass 


        elif args[0][1] in Whole_domain():
            if args[0][2] == "user_report":
                table_header_json = data_analysis("user_table_header",args[0][1])
                json_data = str(json.loads(table_header_json)).replace("[","").replace("]","").replace("\\ufeff","")
                json_data = json_data.split(",")
                header_list = list()
                for data_ in json_data:
                    if (("아이디" in data_) or ("수신" in data_) or ("가입" in data_) or ("로그인" in data_) or ("접속일" in data_) or ("회원등급" in data_) or ("그룹" in data_)) and (("CRM" not in data_) or ("파워앱" not in data_) or ("IP" not in data_)):
                        header_list.append(data_)
                headers = "\n".join(header_list)
                data = "*user_all*"+"```"+headers+"```"
                comment = "\n\n위의 리스트에서\n|| 유저아이디: *fd_#* || 회원가입일: *fd_#* || 최근로그인: *fd_#* || SMS 수신여부: *fd_#* || 이메일 수신여부: *fd_#* || 회원그룹 및 회원 등급 *fd_#*\n을 추출하여 순서대로 입력해주세요.\n\n{}의 유저 종합 분석 리포트는 `data {} user_report fd_# fd_# fd_# fd_# fd_# fd_#` 입력을 통해 확인 가능합니다!".format(args[0][1],args[0][1])
                return data+comment

            elif args[0][2] == "user_table":
                try:
                    user_table_head = data_analysis("user_table_header",args[0][1])
                    json_data = str(json.loads(user_table_head)).replace("[","").replace("]","").replace("\\ufeff","")
                    json_data = json_data.replace("{\'","").replace("\': \'"," AS \"").replace("\'},","\",\n").replace("\'}","\"")
                    data = "*user_all*"+"```"+json_data+"```"
                    return data	
                except:
                    return ("*{}* 의 회원 테이블을 찾을 수 없습니다.".format(args[0][1]))


            # ================================= 주문 관련 ======================================

            elif args[0][2] == "order_report":
                order_table_head = data_analysis("order_table_header",args[0][1])
                json_data = str(json.loads(order_table_head)).replace("[","").replace("]","").replace("\\ufeff","")
                json_data = json_data.split(",")
                header_list = list()
                for data_ in json_data:
                    if ("주문번호" in data_) or ("등급" in data_) or ("그룹" in data_) or ("ID" in data_) or ("아이디" in data_) or ("수령인 주소" in data_) or ("입금확인일" in data_) or ("주문상태" in data_) or ("결제상태" in data_) or ("가입일" in data_) or ("연계주문" in data_) or ("주문경로" in data_) or ("주문상태" in data_) or ("Email" in data_) or ("이메일" in data_) or ("총금액" in data_) or ("결제금액" in data_):
                        header_list.append(data_)
                headers = "\n".join(header_list)
                data = "*user_all*"+"```"+headers+"```"
                comment = "\n\n위의 리스트에서\n|| 주문번호: *fd_#* || 회원등급: *fd_#* || 회원아이디: *fd_#* || 수령인 주소: *fd_#* || 입금 확인일 : *fd_#* || 주문상태 및 결제상태 *fd_#* || 가입일 *fd_#* || 주문경로 *fd_#* || 주문자 이메일 *fd_#* || 총 결제금액 *fd_#*\n을 추출하여 순서대로 입력해주세요.\n\n{ims_app_id}의 구매 종합 분석 리포트는 `data {ims_app_id} order_report fd_# fd_# fd_# fd_# fd_# fd_#` 입력을 통해 확인 가능합니다!".format(ims_app_id = args[0][1])
                return (data+comment)


            elif args[0][2] == "order_table":
                try:
                    order_table_head = data_analysis("order_table_header", args[0][1])
                    json_data = str(json.loads(order_table_head)).replace("[","").replace("]","").replace("\\ufeff","")
                    json_data = json_data.replace("{\'","").replace("\': \'"," AS \"").replace("\'},","\",\n").replace("\'}","\"")
                    data = "*order_all*"+"\n"+json_data
                    return data
                except:
                    return ("*{}* 의 주문 테이블을 찾을 수 없습니다.".format(args[0][1]))

            # ================================= 상품 관련 ======================================
            elif args[0][2] == "product_table":
                try:
                    product_table_head = data_analysis("product_table_header", args[0][1])
                    json_data = str(json.loads(product_table_head))
                    json_data = json_data.replace("},","},\n")
                    data = "*product_all*"+"\n```"+json_data+"```"
                    return data
                except:
                    return ("*{}* 의 상품 테이블을 찾을 수 없습니다.".format(args[0][1]))

            elif args[0][2] == "page_view":
                # try:
                slack_msg("페이지뷰 분석 결과는 프레스토 클러스터가 \"쾌적\"인 상태 기준 약 2 ~ 3 분이 소요됩니다.\n회원수에 따라 소요시간이 차이날 수 있습니다.")
                pv_result = data_analysis("page_view",args[0][1])
                user_pv_result = data_analysis("user_page_view",args[0][1])

                user_pv_1 = int(user_pv_result[0])
                user_pv_2 = int(user_pv_result[1])
                user_pv_3 = int(user_pv_result[2])
                user_pv_4 = int(user_pv_result[3])
                user_pv_5 = int(user_pv_result[4])
                user_pv_6 = int(user_pv_result[5])
                user_pv_7 = int(user_pv_result[6])
                user_pv_8 = int(user_pv_result[7])
                user_pv_9 = int(user_pv_result[8])
                user_pv_10 = int(user_pv_result[9])
                user_pv_11 = int(user_pv_result[10])
                user_pv_12 = int(user_pv_result[11])
                user_pv_13 = int(user_pv_result[12])
                user_pv_14 = int(user_pv_result[13])
                u = np.array([user_pv_1,user_pv_2,user_pv_3,user_pv_4,user_pv_5,user_pv_6,user_pv_7,user_pv_8,user_pv_9,user_pv_10,user_pv_11,user_pv_12,user_pv_13,user_pv_14])
                user_pv_mean = '{0:,.2f}'.format(np.mean(u))

                user_pv_1 = '{0:,d}'.format(user_pv_1)
                user_pv_2 = '{0:,d}'.format(user_pv_2)
                user_pv_3 = '{0:,d}'.format(user_pv_3)
                user_pv_4 = '{0:,d}'.format(user_pv_4)
                user_pv_5 = '{0:,d}'.format(user_pv_5)
                user_pv_6 = '{0:,d}'.format(user_pv_6)
                user_pv_7 = '{0:,d}'.format(user_pv_7)
                user_pv_8 = '{0:,d}'.format(user_pv_8)
                user_pv_9 = '{0:,d}'.format(user_pv_9)
                user_pv_10 = '{0:,d}'.format(user_pv_10)
                user_pv_11 = '{0:,d}'.format(user_pv_11)
                user_pv_12 = '{0:,d}'.format(user_pv_12)
                user_pv_13 = '{0:,d}'.format(user_pv_13)
                user_pv_14 = '{0:,d}'.format(user_pv_14)

                pv_1 = int(pv_result[0])
                pv_2 = int(pv_result[3])
                pv_3 = int(pv_result[6])
                pv_4 = int(pv_result[9])
                pv_5 = int(pv_result[12])
                pv_6 = int(pv_result[15])
                pv_7 = int(pv_result[18])
                pv_8 = int(pv_result[21])
                pv_9 = int(pv_result[24])
                pv_10 = int(pv_result[27])
                pv_11 = int(pv_result[30])
                pv_12 = int(pv_result[33])
                pv_13 = int(pv_result[36])
                pv_14 = int(pv_result[39])
                a = np.array([pv_1,pv_2,pv_3,pv_4,pv_5,pv_6,pv_7,pv_8,pv_9,pv_10,pv_11,pv_12,pv_13,pv_14])
                pv_mean = '{0:,.2f}'.format(np.mean(a))

                uv_1 = int(pv_result[1])
                uv_2 = int(pv_result[4])
                uv_3 = int(pv_result[7])
                uv_4 = int(pv_result[10])
                uv_5 = int(pv_result[13])
                uv_6 = int(pv_result[16])
                uv_7 = int(pv_result[19])
                uv_8 = int(pv_result[22])
                uv_9 = int(pv_result[25])
                uv_10 = int(pv_result[28])
                uv_11 = int(pv_result[31])
                uv_12 = int(pv_result[34])
                uv_13 = int(pv_result[37])
                uv_14 = int(pv_result[40])
                b = np.array([uv_1,uv_2,uv_3,uv_4,uv_5,uv_6,uv_7,uv_8,uv_9,uv_10,uv_11,uv_12,uv_13,uv_14])
                uv_mean = '{0:,.2f}'.format(np.mean(b))

                uni_pv_1 = float(pv_result[2])
                uni_pv_2 = float(pv_result[5])
                uni_pv_3 = float(pv_result[8])
                uni_pv_4 = float(pv_result[11])
                uni_pv_5 = float(pv_result[14])
                uni_pv_6 = float(pv_result[17])
                uni_pv_7 = float(pv_result[20])
                uni_pv_8 = float(pv_result[23])
                uni_pv_9 = float(pv_result[26])
                uni_pv_10 = float(pv_result[29])
                uni_pv_11 = float(pv_result[32])
                uni_pv_12 = float(pv_result[35])
                uni_pv_13 = float(pv_result[38])
                uni_pv_14 = float(pv_result[41])
                c = np.array([uni_pv_1,uni_pv_2,uni_pv_3,uni_pv_4,uni_pv_5,uni_pv_6,uni_pv_7,uni_pv_8,uni_pv_9,uni_pv_10,uni_pv_11,uni_pv_12,uni_pv_13,uni_pv_14])
                unique_pv_mean = '{0:,.2f}'.format(np.mean(c))

                pv_1 = integer_formatter(pv_result[0])
                pv_2 = integer_formatter(pv_result[3])
                pv_3 = integer_formatter(pv_result[6])
                pv_4 = integer_formatter(pv_result[9])
                pv_5 = integer_formatter(pv_result[12])
                pv_6 = integer_formatter(pv_result[15])
                pv_7 = integer_formatter(pv_result[18])
                pv_8 = integer_formatter(pv_result[21])
                pv_9 = integer_formatter(pv_result[24])
                pv_10 = integer_formatter(pv_result[27])
                pv_11 = integer_formatter(pv_result[30])
                pv_12 = integer_formatter(pv_result[33])
                pv_13 = integer_formatter(pv_result[36])
                pv_14 = integer_formatter(pv_result[39])

                uv_1 = integer_formatter(pv_result[1])
                uv_2 = integer_formatter(pv_result[4])
                uv_3 = integer_formatter(pv_result[7])
                uv_4 = integer_formatter(pv_result[10])
                uv_5 = integer_formatter(pv_result[13])
                uv_6 = integer_formatter(pv_result[16])
                uv_7 = integer_formatter(pv_result[19])
                uv_8 = integer_formatter(pv_result[22])
                uv_9 = integer_formatter(pv_result[25])
                uv_10 = integer_formatter(pv_result[28])
                uv_11 = integer_formatter(pv_result[31])
                uv_12 = integer_formatter(pv_result[34])
                uv_13 = integer_formatter(pv_result[37])
                uv_14 = integer_formatter(pv_result[40])

                uni_pv_1 = double_formatter(pv_result[2])
                uni_pv_2 = double_formatter(pv_result[5])
                uni_pv_3 = double_formatter(pv_result[8])
                uni_pv_4 = double_formatter(pv_result[11])
                uni_pv_5 = double_formatter(pv_result[14])
                uni_pv_6 = double_formatter(pv_result[17])
                uni_pv_7 = double_formatter(pv_result[20])
                uni_pv_8 = double_formatter(pv_result[23])
                uni_pv_9 = double_formatter(pv_result[26])
                uni_pv_10 = double_formatter(pv_result[29])
                uni_pv_11 = double_formatter(pv_result[32])
                uni_pv_12 = double_formatter(pv_result[35])
                uni_pv_13 = double_formatter(pv_result[38])
                uni_pv_14 = double_formatter(pv_result[41])

                today = datetime.now(pytz.timezone('Asia/Seoul')).strftime("%Y-%m-%d %H:%M:%S %z")
                comment = "*{app_id}* 의 지난 14일간 페이지뷰 결과입니다.\n14일간 평균 페이지 뷰는 *{mean} 회* 입니다.\n14일간 평균 순 유입자 수는 *{unique_mean} 명* 입니다.\n14일간 방문당 페이지뷰 평균은 *{uni_pv_mean} 회* 입니다.\n14일간 평균 회원 유입자 수는 *{user_pv_mean_} 명* 입니다.\n\n오늘 ({date}) 기준\n```1일 전 페이지뷰 : {_1} 회\t순 유입자 수 : {uv_1_} 명\t방문당 페이지 뷰 : {uni_pv_1_} 회\t회원 순유입 : {user_pv_1_}\n2일 전 페이지뷰 : {_2} 회\t순 유입자 수 : {uv_2_} 명\t방문당 페이지 뷰 : {uni_pv_2_} 회\t회원 순유입 : {user_pv_2_}\n3일 전 페이지뷰 : {_3} 회\t순 유입자 수 : {uv_3_} 명\t방문당 페이지 뷰 : {uni_pv_3_} 회\t회원 순유입 : {user_pv_3_}\n4일 전 페이지뷰 : {_4} 회\t순 유입자 수 : {uv_4_} 명\t방문당 페이지 뷰 : {uni_pv_4_} 회\t회원 순유입 : {user_pv_4_}\n5일 전 페이지뷰 : {_5} 회\t순 유입자 수 : {uv_5_} 명\t방문당 페이지 뷰 : {uni_pv_5_} 회\t회원 순유입 : {user_pv_5_}\n6일 전 페이지뷰 : {_6} 회\t순 유입자 수 : {uv_6_} 명\t방문당 페이지 뷰 : {uni_pv_6_} 회\t회원 순유입 : {user_pv_6_}\n7일 전 페이지뷰 : {_7} 회\t순 유입자 수 : {uv_7_} 명\t방문당 페이지 뷰 : {uni_pv_7_} 회\t회원 순유입 : {user_pv_8_}\n8일 전 페이지뷰 : {_8} 회\t순 유입자 수 : {uv_8_} 명\t방문당 페이지 뷰 : {uni_pv_8_} 회\t회원 순유입 : {user_pv_8_}\n9일 전 페이지뷰 : {_9} 회\t순 유입자 수 : {uv_9_} 명\t방문당 페이지 뷰 : {uni_pv_9_} 회\t회원 순유입 : {user_pv_9_}\n10일 전 페이지뷰 : {_10} 회\t순 유입자 수 : {uv_10_} 명\t방문당 페이지 뷰 : {uni_pv_10_} 회\t회원 순유입 : {user_pv_10_}\n11일 전 페이지뷰 : {_11} 회\t순 유입자 수 : {uv_11_} 명\t방문당 페이지 뷰 : {uni_pv_11_} 회\t회원 순유입 : {user_pv_11_}\n12일 전 페이지뷰 : {_12} 회\t순 유입자 수 : {uv_12_} 명\t방문당 페이지 뷰 : {uni_pv_12_} 회\t회원 순유입 : {user_pv_12_}\n13일 전 페이지뷰 : {_13} 회\t순 유입자 수 : {uv_13_} 명\t방문당 페이지 뷰 : {uni_pv_13_} 회\t회원 순유입 : {user_pv_13_}\n14일 전 페이지뷰 : {_14} 회\t순 유입자 수 : {uv_14_} 명\t방문당 페이지 뷰 : {uni_pv_14_} 회\t회원 순유입 : {user_pv_14_}```\n_#WEB테이블 기준이며, 순 유입자 수는 페이지 뷰 테이블의 고유 SID 기준이며, 회원 순 유입자 수는 USER_MAPPING 테이블에서 회원별 SIDs 기준입니다._".format(app_id = args[0][1], date = today, mean = pv_mean, user_pv_mean_ = user_pv_mean, unique_mean = uv_mean, uni_pv_mean= unique_pv_mean, _1 = pv_1, _2 = pv_2, _3 = pv_3, _4 = pv_4, _5 = pv_5, _6 = pv_6, _7 = pv_7, _8 = pv_8, _9 = pv_9, _10 = pv_10, _11 = pv_11, _12 = pv_12, _13 = pv_13, _14 = pv_14, uv_1_ = uv_1, uv_2_ = uv_2, uv_3_ = uv_3, uv_4_ = uv_4, uv_5_ = uv_5, uv_6_ = uv_6, uv_7_ = uv_7, uv_8_ = uv_8, uv_9_ = uv_9, uv_10_ = uv_10, uv_11_ = uv_11, uv_12_ = uv_12, uv_13_ = uv_13, uv_14_ = uv_14, uni_pv_1_ = uni_pv_1, uni_pv_2_ = uni_pv_2, uni_pv_3_ = uni_pv_3, uni_pv_4_ = uni_pv_4, uni_pv_5_ = uni_pv_5, uni_pv_6_ = uni_pv_6, uni_pv_7_ = uni_pv_7, uni_pv_8_ = uni_pv_8, uni_pv_9_ = uni_pv_9, uni_pv_10_ = uni_pv_10, uni_pv_11_ = uni_pv_11, uni_pv_12_ = uni_pv_12, uni_pv_13_ = uni_pv_13, uni_pv_14_ = uni_pv_14, user_pv_1_ = user_pv_1, user_pv_2_ = user_pv_2, user_pv_3_ = user_pv_3, user_pv_4_ = user_pv_4, user_pv_5_ = user_pv_5, user_pv_6_ = user_pv_6, user_pv_7_ = user_pv_7, user_pv_8_ = user_pv_8, user_pv_9_ = user_pv_9, user_pv_10_ = user_pv_10, user_pv_11_ = user_pv_11, user_pv_12_ = user_pv_12, user_pv_13_ = user_pv_13, user_pv_14_ = user_pv_14)
                # comment = "*{app_id}* 의 지난 14일간 페이지뷰 결과입니다.\n14일간 평균 페이지 뷰는 *{mean} 회* 입니다.\n14일간 평균 순 방문자 수는 *{unique_mean} 명* 입니다.\n14일간 방문당 페이지뷰 평균은 *{uni_pv_mean} 회* 입니다.\n\n오늘 ({date}) 기준\n```1일 전 페이지뷰 : {_1} 회\n\t순 유입자 수 : {uv_1_} 명\n\t방문당 페이지 뷰 : {uni_pv_1_} 회\n2일 전 페이지뷰 : {_2} 회\n\t순 유입자 수 : {uv_2_} 명\n\t방문당 페이지 뷰 : {uni_pv_2_} 회\n3일 전 페이지뷰 : {_3} 회\n\t순 유입자 수 : {uv_3_} 명\n\t방문당 페이지 뷰 : {uni_pv_3_} 회\n4일 전 페이지뷰 : {_4} 회\n\t순 유입자 수 : {uv_4_} 명\n\t방문당 페이지 뷰 : {uni_pv_4_} 회\n5일 전 페이지뷰 : {_5} 회\n\t순 유입자 수 : {uv_5_} 명\n\t방문당 페이지 뷰 : {uni_pv_5_} 회\n6일 전 페이지뷰 : {_6} 회\n\t순 유입자 수 : {uv_6_} 명\n\t방문당 페이지 뷰 : {uni_pv_6_} 회\n7일 전 페이지뷰 : {_7} 회\n\t순 유입자 수 : {uv_7_} 명\n\t방문당 페이지 뷰 : {uni_pv_7_} 회\n8일 전 페이지뷰 : {_8} 회\n\t순 유입자 수 : {uv_8_} 명\n\t방문당 페이지 뷰 : {uni_pv_8_} 회\n9일 전 페이지뷰 : {_9} 회\n\t순 유입자 수 : {uv_9_} 명\n\t방문당 페이지 뷰 : {uni_pv_9_} 회\n10일 전 페이지뷰 : {_10} 회\n\t순 유입자 수 : {uv_10_} 명\n\t방문당 페이지 뷰 : {uni_pv_10_} 회\n11일 전 페이지뷰 : {_11} 회\n\t순 유입자 수 : {uv_11_} 명\n\t방문당 페이지 뷰 : {uni_pv_11_} 회\n12일 전 페이지뷰 : {_12} 회\n\t순 유입자 수 : {uv_12_} 명\n\t방문당 페이지 뷰 : {uni_pv_12_} 회\n13일 전 페이지뷰 : {_13} 회\n\t순 유입자 수 : {uv_13_} 명\n\t방문당 페이지 뷰 : {uni_pv_13_} 회\n14일 전 페이지뷰 : {_14} 회\n\t순 유입자 수 : {uv_14_} 명\n\t방문당 페이지 뷰 : {uni_pv_14_} 회```".format(app_id = args[0][1], date = today, mean = pv_mean, unique_mean = uv_mean, uni_pv_mean= unique_pv_mean, _1 = pv_1, _2 = pv_2, _3 = pv_3, _4 = pv_4, _5 = pv_5, _6 = pv_6, _7 = pv_7, _8 = pv_8, _9 = pv_9, _10 = pv_10, _11 = pv_11, _12 = pv_12, _13 = pv_13, _14 = pv_14, uv_1_ = uv_1, uv_2_ = uv_2, uv_3_ = uv_3, uv_4_ = uv_4, uv_5_ = uv_5, uv_6_ = uv_6, uv_7_ = uv_7, uv_8_ = uv_8, uv_9_ = uv_9, uv_10_ = uv_10, uv_11_ = uv_11, uv_12_ = uv_12, uv_13_ = uv_13, uv_14_ = uv_14, uni_pv_1_ = uni_pv_1, uni_pv_2_ = uni_pv_2, uni_pv_3_ = uni_pv_3, uni_pv_4_ = uni_pv_4, uni_pv_5_ = uni_pv_5, uni_pv_6_ = uni_pv_6, uni_pv_7_ = uni_pv_7, uni_pv_8_ = uni_pv_8, uni_pv_9_ = uni_pv_9, uni_pv_10_ = uni_pv_10, uni_pv_11_ = uni_pv_11, uni_pv_12_ = uni_pv_12, uni_pv_13_ = uni_pv_13, uni_pv_14_ = uni_pv_14)
                return (comment)
                pass

            else:
                not_get_comm = not_get_comment(2, *args)
                return not_get_comm
                pass

        else:
            return ("입력하신 *\"{}\"* 는 존재하지 않는 ims_app_id 입니다. \nims_app_id 확인을 위해서는 `ims active`를 입력해 주세요.".format(args[0][1]))

# ===========================[4] 입력 단어 "" ==============================

    elif len(args[0]) == 4:
        if args[0][1] in Whole_domain():
            if args[0][2] == "user_table":
                table_header_json = data_analysis("user_table_header",args[0][1])
                json_data = str(json.loads(table_header_json)).replace("[","").replace("]","").replace("\\ufeff","")
                json_data = json_data.split(",")
                header_list = list()
                input_text = str(args[0][3])

                for data_ in json_data:
                    if input_text in data_:
                        header_list.append(data_)
                headers = "\n".join(header_list)
                if headers != "":
                    data = "{}의 *user_all* 테이블에서 입력하신 값인 \"{}\" 을/를 조회한 결과에요.".format(args[0][1],input_text)+"```"+headers+"```"
                    return (data)
                else:
                    return ("입력하신 값 *\"{}\"* 이/가 컬럼 헤더에 없을 가능성이 있습니다. 다시한번 확인해 주세요.".format(input_text))
                    pass

            elif args[0][2] == "order_table":
                order_table_head = data_analysis("order_table_header",args[0][1])
                json_data = str(json.loads(order_table_head)).replace("[","").replace("]","").replace("\\ufeff","")
                json_data = json_data.split(",")
                header_list = list()
                input_text = str(args[0][3])

                for data_ in json_data:
                    if input_text in data_:
                        header_list.append(data_)
                headers = "\n".join(header_list)
                if headers != "":
                    data = "{}의 *order_all* 테이블에서 입력하신 값인 \"{}\" 을/를 조회한 결과에요.".format(args[0][1],input_text)+"```"+headers+"```"
                    return (data)
                else:
                    return ("입력하신 값 *\"{}\"* 이/가 컬럼 헤더에 없을 가능성이 있습니다. 다시한번 확인해 주세요.".format(input_text))
                    pass
            

# ------------------------------------------------------------------------------------------------------------------------------
            elif args[0][2] == "revisit_web":
                slack_msg("```{ims_app_id} 의 {date}월 한 달간의 웹 재방문 주기를 계산중입니다. 소요시간은 약 4~5분 입니다.\n-1 인 값은 기간내 재방문이 없는 고객 수 입니다.```".format(ims_app_id = args[0][1],date = args[0][3]))
                try:
                    revisit_data = data_analysis("revisit_web",args[0][1],args[0][3])

                    tot_customers = 0
                    visit_once = 0
                    for customers in list(revisit_data):
                        date_gap = customers[1:-1].split(",")[0]
                        user_count = customers[1:-1].split(",")[1]
                        if '-1' in date_gap:
                            visit_once = int(visit_once) + int(user_count)
                        user_count = int(user_count)
                        tot_customers += user_count
                    visit_many = (tot_customers - visit_once)
                    tot_agg = "```{ims_app_id} 의 {date}월\n월간 웹을 통한 방문 전체 고유 회원수는 {us_cnt}명\n월간 웹을 통한 재방문 전체 고유 회원수는 {vi_many}명 입니다.```".format(us_cnt = tot_customers, vi_many = visit_many, ims_app_id = args[0][1], date = args[0][3])
                    revisit_data = str(revisit_data).replace(")\",",")\"\n").replace("\"(","").replace(")\"","")
                    revisit_formatting = "```{ims_app_id} 의 {date}월 내 웹 재방문 주기 분석 결과입니다. (-1 은 기간내 단일 방문이 이루어진 고객 수 입니다.)\n컬럼1=재방문주기(일) | 컬럼2=회원수\n".format(ims_app_id = args[0][1], date = args[0][3])
                    revisit_data = tot_agg +"\n"+ revisit_formatting + revisit_data + "```"
                except:
                    revisit_data = "{ims_app_id} 의 {date}월에 해당하는 웹 재방문 데이터를 찾을 수 없습니다. 데이터 수집 기간을 다시한번 확인바랍니다.\n`data uneedcommscom revisit_web 2018-07` 형식에 맞추어 원하는 달의 정보를 찾을 수 있습니다.\n질문이 필요하신 경우, `help`를 입력해 주세요.".format(date = args[0][3],ims_app_id = args[0][1])
                return revisit_data
                pass

            elif args[0][2] == "repurchase_web":
                slack_msg("```{ims_app_id} 의 {date}월 한 달간의 웹 재구매 주기를 계산중입니다. 소요시간은 약 2~3분 입니다.\n-1 인 값은 기간내 재방문이 없는 고객 수 입니다.```".format(ims_app_id = args[0][1],date = args[0][3]))
                try:
                    repurchase_data = data_analysis("repurchase_web",args[0][1],args[0][3])
                    tot_customers = 0
                    visit_once = 0
                    for customers in list(repurchase_data):
                        date_gap = customers[1:-1].split(",")[0]
                        user_count = customers[1:-1].split(",")[1]
                        if '-1' in date_gap:
                            visit_once = int(visit_once) + int(user_count)
                        user_count = int(user_count)
                        tot_customers += user_count
                    visit_many = (tot_customers - visit_once)
                    tot_agg = "```{ims_app_id} 의 {date}월\n월간 웹을 통한 구매 전체 고유 회원수는 {us_cnt}명\n월간 웹을 통한 재구매 전체 고유 회원수는 {vi_many}명 입니다.```".format(us_cnt = tot_customers, vi_many = visit_many,ims_app_id = args[0][1], date = args[0][3])
                    repurchase_data = str(repurchase_data).replace(")\",",")\"\n").replace("\"(","").replace(")\"","")
                    repurchase_formatting = "```{ims_app_id} 의 {date}월 내 웹 재구매 주기 분석 결과입니다. (-1 은 기간내 단일 방문이 이루어진 고객 수 입니다.)\n컬럼1=재구매주기(일) | 컬럼2=회원수\n".format(ims_app_id = args[0][1], date = args[0][3])
                    repurchase_data = tot_agg + "\n" + repurchase_formatting + repurchase_data + "```"
                except:
                    repurchase_data = "{ims_app_id} 의 {date}월에 해당하는 웹 재구매 데이터를 찾을 수 없습니다. 데이터 수집 기간을 다시한번 확인바랍니다.\n`data uneedcommscom repurchase_web 2018-07` 형식에 맞추어 원하는 달의 정보를 찾을 수 있습니다.\n질문이 필요하신 경우, `help`를 입력해 주세요.".format(date = args[0][3],ims_app_id = args[0][1])
                return repurchase_data
                pass

            elif args[0][2] == "revisit_app":
                slack_msg("```{ims_app_id} 의 {date}월 한 달간의 앱 재방문 주기를 계산중입니다. 소요시간은 약 4~5분 입니다.\n-1 인 값은 기간내 재방문이 없는 고객 수 입니다.```".format(ims_app_id = args[0][1],date = args[0][3]))
                try:
                    revisit_data = data_analysis("revisit_app",args[0][1],args[0][3])   
                    tot_customers = 0
                    visit_once = 0
                    for customers in list(revisit_data):
                        date_gap = customers[1:-1].split(",")[0]
                        user_count = customers[1:-1].split(",")[1]
                        if '-1' in date_gap:
                            visit_once = int(visit_once) + int(user_count)
                        user_count = int(user_count)
                        tot_customers += user_count
                    visit_many = (tot_customers - visit_once)
                    tot_agg = "```{ims_app_id} 의 {date}월\n월간 앱을 통한 방문 전체 고유 회원수는 {us_cnt}명\n월간 앱을 통한 재방문 전체 고유 회원수는 {vi_many}명 입니다.```".format(us_cnt = tot_customers, vi_many = visit_many, ims_app_id = args[0][1], date = args[0][3])
                    revisit_data = str(revisit_data).replace(")\",",")\"\n").replace("\"(","").replace(")\"","")
                    revisit_formatting = "```{ims_app_id} 의 {date}월 내 앱 재방문 주기 분석 결과입니다. (-1 은 기간내 단일 방문이 이루어진 고객 수 입니다.)\n컬럼1=재방문주기(일) | 컬럼2=회원수\n".format(ims_app_id = args[0][1], date = args[0][3])
                    revisit_data = tot_agg + "\n" + revisit_formatting + revisit_data + "```"
                except:
                    revisit_data = "{ims_app_id} 의 {date}월에 해당하는 앱 재방문 데이터를 찾을 수 없습니다. 데이터 수집 기간을 다시한번 확인바랍니다.\n앱 관련 테이블이 존재하지 않을 가능성이 있습니다.\n`data uneedcommscom revisit_app 2018-07` 형식에 맞추어 원하는 달의 정보를 찾을 수 있습니다.\n질문이 필요하신 경우, `help`를 입력해 주세요.".format(date = args[0][3],ims_app_id = args[0][1])
                return revisit_data
                pass

            elif args[0][2] == "repurchase_app":
                slack_msg("```{ims_app_id} 의 {date}월 한 달간의 앱 재구매 주기를 계산중입니다. 소요시간은 약 2~3분 입니다.\n-1 인 값은 기간내 재방문이 없는 고객 수 입니다.```".format(ims_app_id = args[0][1],date = args[0][3]))
                try:
                    repurchase_data = data_analysis("repurchase_app",args[0][1],args[0][3])                    
                    tot_customers = 0
                    visit_once = 0
                    for customers in list(repurchase_data):
                        date_gap = customers[1:-1].split(",")[0]
                        user_count = customers[1:-1].split(",")[1]
                        if '-1' in date_gap:
                            visit_once = int(visit_once) + int(user_count)
                        user_count = int(user_count)
                        tot_customers += user_count
                    visit_many = (tot_customers - visit_once)
                    tot_agg = "```{ims_app_id} 의 {date}월\n월간 앱을 통한 구매 전체 고유 회원수는 {us_cnt}명\n월간 앱을 통한 재구매 전체 고유 회원수는 {vi_many}명 입니다.```".format(us_cnt = tot_customers, vi_many = visit_many, ims_app_id = args[0][1], date = args[0][3])
                    repurchase_data = str(repurchase_data).replace(")\",",")\"\n").replace("\"(","").replace(")\"","")
                    repurchase_formatting = "```{ims_app_id} 의 {date}월 내 앱 재구매 주기 분석 결과입니다. (-1 은 기간내 단일 방문이 이루어진 고객 수 입니다.)\n컬럼1=재구매주기(일) | 컬럼2=회원수\n".format(ims_app_id = args[0][1], date = args[0][3])
                    repurchase_data = tot_agg + "\n" + repurchase_formatting + repurchase_data + "```"
                except:
                    repurchase_data = "{ims_app_id} 의 {date}월에 해당하는 앱 재방문 데이터를 찾을 수 없습니다. 데이터 수집 기간을 다시한번 확인바랍니다.\n앱 관련 테이블이 존재하지 않을 가능성이 있습니다.\n`data uneedcommscom repurchase_app 2018-07` 형식에 맞추어 원하는 달의 정보를 찾을 수 있습니다.\n질문이 필요하신 경우, `help`를 입력해 주세요.".format(date = args[0][3],ims_app_id = args[0][1])
                return repurchase_data
                pass

# ================================================| iMs 메시지가 발송된 고객 대상 |================================================ 
            elif args[0][2] == "ims_revisit_app":
                slack_msg("```{ims_app_id} 의 {date}월 한 달간 iMs 메시지가 발송된 고객의 앱 재방문 정보 분석. 소요시간은 약 2~3분 입니다.\n-1 인 값은 기간내 재방문이 없는 고객 수 입니다.```".format(ims_app_id = args[0][1],date = args[0][3]))
                try:
                    ims_repurchase_data = data_analysis("ims_revisit_app",args[0][1],args[0][3])
                    tot_customers = 0
                    visit_once = 0
                    for customers in list(ims_repurchase_data):
                        date_gap = customers[1:-1].split(",")[0]
                        user_count = customers[1:-1].split(",")[1]
                        if '-1' in date_gap:
                            visit_once = int(visit_once) + int(user_count)
                        user_count = int(user_count)
                        tot_customers += user_count
                    visit_many = (tot_customers - visit_once)
                    tot_agg = "```{ims_app_id} 의 {date}월\n월간 iMs 메시지를 받은 고객 중 앱을 통해 방문이 이루어진 전체 고유 회원수는 {us_cnt}명\n월간 iMs 메시지를 받은 고객 중 앱을 통해 재방문이 이루어진 전체 고유 회원수는 {vi_many}명 입니다.```".format(us_cnt = tot_customers, vi_many = visit_many, ims_app_id = args[0][1], date = args[0][3])                    
                    ims_repurchase_data = str(ims_repurchase_data).replace(")\",",")\"\n").replace("\"(","").replace(")\"","")
                    ims_repurchase_formatting = "```{ims_app_id} 의 {date}월 내 iMs 메시지가 발송된 고객의 앱 재방문 정보 분석 결과입니다. (-1 은 기간내 단일 방문이 이루어진 고객 수 입니다.)\n컬럼1=재구매주기(일) | 컬럼2=회원수\n".format(ims_app_id = args[0][1], date = args[0][3])
                    ims_repurchase_data = tot_agg + "\n" + ims_repurchase_formatting + ims_repurchase_data + "```"
                except:
                    ims_repurchase_data = "{ims_app_id} 의 {date}월에 해당하는 iMs 메시지가 발송된 고객의 앱 재방문 데이터를 찾을 수 없습니다. 데이터 수집 기간을 다시한번 확인바랍니다.\n앱 관련 테이블이 존재하지 않을 가능성이 있습니다.\n`data uneedcommscom ims_revisit_app 2018-07` 형식에 맞추어 원하는 달의 정보를 찾을 수 있습니다.\n질문이 필요하신 경우, `help`를 입력해 주세요.".format(date = args[0][3],ims_app_id = args[0][1])
                return ims_repurchase_data
                pass

            elif args[0][2] == "ims_repurchase_app":
                slack_msg("```{ims_app_id} 의 {date}월 한 달간 iMs 메시지가 발송된 고객의 앱 재구매 정보 분석. 소요시간은 약 2~3분 입니다.\n-1 인 값은 기간내 재구매가 없는 고객 수 입니다.```".format(ims_app_id = args[0][1],date = args[0][3]))
                try:
                    ims_repurchase_data = data_analysis("ims_repurchase_app",args[0][1],args[0][3])
                    tot_customers = 0
                    visit_once = 0
                    for customers in list(ims_repurchase_data):
                        date_gap = customers[1:-1].split(",")[0]
                        user_count = customers[1:-1].split(",")[1]
                        if '-1' in date_gap:
                            visit_once = int(visit_once) + int(user_count)
                        user_count = int(user_count)
                        tot_customers += user_count
                    visit_many = (tot_customers - visit_once)
                    tot_agg = "```{ims_app_id} 의 {date}월\n월간 iMs 메시지를 받은 고객 중 앱을 통해 구매가 이루어진 전체 고유 회원수는 {us_cnt}명\n월간 iMs 메시지를 받은 고객 중 앱을 통해 재구매가 이루어진 전체 고유 회원수는 {vi_many}명 입니다.```".format(us_cnt = tot_customers, vi_many = visit_many, ims_app_id = args[0][1], date = args[0][3])                    
                    ims_repurchase_data = str(ims_repurchase_data).replace(")\",",")\"\n").replace("\"(","").replace(")\"","")
                    ims_repurchase_formatting = "```{ims_app_id} 의 {date}월 내 iMs 메시지가 발송된 고객의 앱 재구매 정보 분석 결과입니다. (-1 은 기간내 단일 방문이 이루어진 고객 수 입니다.)\n컬럼1=재구매주기(일) | 컬럼2=회원수\n".format(ims_app_id = args[0][1], date = args[0][3])
                    ims_repurchase_data = tot_agg + "\n" + ims_repurchase_formatting + ims_repurchase_data + "```"
                except:
                    ims_repurchase_data = "{ims_app_id} 의 {date}월에 해당하는 iMs 메시지가 발송된 고객의 앱 재방문 데이터를 찾을 수 없습니다. 데이터 수집 기간을 다시한번 확인바랍니다.\n앱 관련 테이블이 존재하지 않을 가능성이 있습니다.\n`data uneedcommscom ims_repurchase_app 2018-07` 형식에 맞추어 원하는 달의 정보를 찾을 수 있습니다.\n질문이 필요하신 경우, `help`를 입력해 주세요.".format(date = args[0][3],ims_app_id = args[0][1])
                return ims_repurchase_data
                pass

            elif args[0][2] == "ims_revisit_web":
                slack_msg("```{ims_app_id} 의 {date}월 한 달간 iMs 메시지가 발송된 고객의 웹 재방문 정보 분석. 소요시간은 약 2~3분 입니다.\n-1 인 값은 기간내 재방문이 없는 고객 수 입니다.```".format(ims_app_id = args[0][1],date = args[0][3]))
                try:
                    ims_repurchase_data = data_analysis("ims_revisit_web",args[0][1],args[0][3])
                    tot_customers = 0
                    visit_once = 0
                    for customers in list(ims_repurchase_data):
                        date_gap = customers[1:-1].split(",")[0]
                        user_count = customers[1:-1].split(",")[1]
                        if '-1' in date_gap:
                            visit_once = int(visit_once) + int(user_count)
                        user_count = int(user_count)
                        tot_customers += user_count
                    visit_many = (tot_customers - visit_once)
                    tot_agg = "```{ims_app_id} 의 {date}월\n월간 iMs 메시지를 받은 고객 중 웹을 통해 방문이 이루어진 전체 고유 회원수는 {us_cnt}명\n월간 iMs 메시지를 받은 고객 중 웹을 통해 재방문이 이루어진 전체 고유 회원수는 {vi_many}명 입니다.```".format(us_cnt = tot_customers, vi_many = visit_many, ims_app_id = args[0][1], date = args[0][3])                    
                    ims_repurchase_data = str(ims_repurchase_data).replace(")\",",")\"\n").replace("\"(","").replace(")\"","")
                    ims_repurchase_formatting = "```{ims_app_id} 의 {date}월 내 iMs 메시지가 발송된 고객의 웹 재방문 정보 분석 결과입니다. (-1 은 기간내 단일 방문이 이루어진 고객 수 입니다.)\n컬럼1=재구매주기(일) | 컬럼2=회원수\n".format(ims_app_id = args[0][1], date = args[0][3])
                    ims_repurchase_data = tot_agg + "\n" + ims_repurchase_formatting + ims_repurchase_data + "```"
                except:
                    ims_repurchase_data = "{ims_app_id} 의 {date}월에 해당하는 iMs 메시지가 발송된 고객의 웹 재방문 데이터를 찾을 수 없습니다. 데이터 수집 기간을 다시한번 확인바랍니다.\n앱 관련 테이블이 존재하지 않을 가능성이 있습니다.\n`data uneedcommscom ims_revisit_web 2018-07` 형식에 맞추어 원하는 달의 정보를 찾을 수 있습니다.\n질문이 필요하신 경우, `help`를 입력해 주세요.".format(date = args[0][3],ims_app_id = args[0][1])
                return ims_repurchase_data
                pass

            elif args[0][2] == "ims_repurchase_web":
                slack_msg("```{ims_app_id} 의 {date}월 한 달간 iMs 메시지가 발송된 고객의 웹 재구매 정보 분석. 소요시간은 약 2~3분 입니다.\n-1 인 값은 기간내 재방문이 없는 고객 수 입니다.```".format(ims_app_id = args[0][1],date = args[0][3]))
                try:
                    ims_repurchase_data = data_analysis("ims_repurchase_web",args[0][1],args[0][3])
                    tot_customers = 0
                    visit_once = 0
                    for customers in list(ims_repurchase_data):
                        date_gap = customers[1:-1].split(",")[0]
                        user_count = customers[1:-1].split(",")[1]
                        if '-1' in date_gap:
                            visit_once = int(visit_once) + int(user_count)
                        user_count = int(user_count)
                        tot_customers += user_count
                    visit_many = (tot_customers - visit_once)
                    tot_agg = "```{ims_app_id} 의 {date}월\n월간 iMs 메시지를 받은 고객 중 웹을 통해 구매가 이루어진 전체 고유 회원수는 {us_cnt}명\n월간 iMs 메시지를 받은 고객 중 웹을 통해 재구매가 이루어진 전체 고유 회원수는 {vi_many}명 입니다.```".format(us_cnt = tot_customers, vi_many = visit_many, ims_app_id = args[0][1], date = args[0][3])

                    ims_repurchase_data = str(ims_repurchase_data).replace(")\",",")\"\n").replace("\"(","").replace(")\"","")
                    ims_repurchase_formatting = "```{ims_app_id} 의 {date}월 내 iMs 메시지가 발송된 고객의 웹 재구매 정보 분석 결과입니다. (-1 은 기간내 단일 방문이 이루어진 고객 수 입니다.)\n컬럼1=재구매주기(일) | 컬럼2=회원수\n".format(ims_app_id = args[0][1], date = args[0][3])
                    ims_repurchase_data = tot_agg + "\n" + ims_repurchase_formatting + ims_repurchase_data + "```"
                except:
                    ims_repurchase_data = "{ims_app_id} 의 {date}월에 해당하는 iMs 메시지가 발송된 고객의 웹 재구매 데이터를 찾을 수 없습니다. 데이터 수집 기간을 다시한번 확인바랍니다.\n앱 관련 테이블이 존재하지 않을 가능성이 있습니다.\n`data uneedcommscom ims_repurchase_web 2018-07` 형식에 맞추어 원하는 달의 정보를 찾을 수 있습니다.\n질문이 필요하신 경우, `help`를 입력해 주세요.".format(date = args[0][3],ims_app_id = args[0][1])
                return ims_repurchase_data
                pass
# ------------------------------------------------------------------------------------------------------------------------------

            else:
            	return ("어떠한 테이블 정보를 얻고자 하시나요? 현재 조회 가능한 테이블은 *user_table*, *order_table*, *product_table* 들 이에요!")
            	pass

        else:
            return ("입력하신 *\"{}\"* 는 존재하지 않는 ims_app_id 입니다. \nims_app_id 확인을 위해서는 `ims active`를 입력해 주세요.".format(args[0][2]))
            pass

# ===========================[>5] 입력 단어 "" ==============================
    elif len(args[0]) > 5:
        if args[0][1] in Whole_domain():
            if args[0][2] == "user_report":
                try:
                    slack_msg("회원데이터 분석 결과는 프레스토 클러스터가 \"쾌적\"인 상태 기준 약 3 ~ 4 분이 소요됩니다.\n회원수에 따라 소요시간이 차이날 수 있습니다.")
                    user_insight = data_analysis("user_report", args[0][1], args[0][3:8])
                    user_grade = data_analysis("user_grade", args[0][1], args[0][3:])
                    user_grade = str(sorted(user_grade, reverse=True)).replace("),",")\n")
                    try:
                        ims_send = send_coverage("send_view", args[0][1])
                        sent_cnt_total = int(ims_send[0])
                        view_cnt_total = int(ims_send[1])
                        ims_send = str(ims_send).replace("),","),\n")
                    except:
                        ims_send = "Empty"
                        sent_cnt_total = 0
                        view_cnt_total = 0
                    insight_user_cnt = int(user_insight[0])
                    insight_user_sms = int(user_insight[1])
                    insight_user_email = int(user_insight[2])
                    insight_user_revisit = int(user_insight[3])
                    insight_user_join = int(user_insight[4])
                    insight_user_email_sms = int(user_insight[5])

                    sent_cnt_ratio = double_formatter(math_calc("percentage",sent_cnt_total,insight_user_cnt))
                    view_cnt_ratio = double_formatter(math_calc("percentage",view_cnt_total,insight_user_cnt))

                    sent_cnt_total = integer_formatter(sent_cnt_total)
                    view_cnt_total = integer_formatter(view_cnt_total)
                    
                    insight_user_sms_ratio = double_formatter(math_calc("percentage",insight_user_sms,insight_user_cnt))
                    insight_user_email_ratio = double_formatter(math_calc("percentage",insight_user_email,insight_user_cnt))
                    insight_user_revisit_ratio = double_formatter(math_calc("percentage",insight_user_revisit,insight_user_cnt))
                    insight_user_join_ratio = double_formatter(math_calc("percentage",insight_user_join,insight_user_cnt))
                    insight_user_email_sms_ratio = double_formatter(math_calc("percentage",insight_user_email_sms,insight_user_cnt))

                    user_visit_total = (insight_user_join + insight_user_revisit)
                    user_visit_total_ratio = double_formatter(math_calc("percentage",user_visit_total,insight_user_cnt))
                    user_visit_total = integer_formatter(user_visit_total)

                    insight_user_cnt = integer_formatter(insight_user_cnt)
                    insight_user_sms = integer_formatter(insight_user_sms)
                    insight_user_email = integer_formatter(insight_user_email)
                    insight_user_revisit = integer_formatter(insight_user_revisit)
                    insight_user_join = integer_formatter(insight_user_join)
                    insight_user_email_sms = integer_formatter(insight_user_email_sms)

                    insight_message = "*{user_id}* 의 회원 데이터 분석 결과입니다.\n>>>전체 회원수 : *{user_cnt} 명*\nSMS & 이메일 동시 수신동의 회원수 : *{sms_email_user} 명 ({sms_email_ratio} %)*\nSMS 수신동의 회원수 : *{sms_user} 명 ({sms_ratio} %)*\n이메일 수신동의 회원수 : *{email_user} 명 ({email_ratio} %)*\niMs 메시지가 발송된 고유 회원수 : *{ims_msg_sent} 명 ({sent_ratio} %)*\niMs 메시지를 열어본 고유 회원수 : *{ims_msg_view} 명 ({view_ratio} %)*\n\n지난 28일 동안 방문한 총 고유 고객 수 : *{visit_total} 명 ({visit_total_ratio} %)*\n지난 28일 동안 재방문한 기존 (고유)고객 수 : *{revisit} 명 ({revisit_ratio} %)*\n지난 28일 동안 신규가입한 (고유)고객 수 : *{join} 명 ({join_ratio} %)*\n\n```등급별 회원수\n{grade_user}```".format(sent_ratio = sent_cnt_ratio, view_ratio = view_cnt_ratio,ims_msg_sent = sent_cnt_total, ims_msg_view = view_cnt_total, user_id = args[0][1], user_cnt = insight_user_cnt, sms_user = insight_user_sms, email_user = insight_user_email, revisit = insight_user_revisit, sms_ratio = insight_user_sms_ratio, email_ratio = insight_user_email_ratio, revisit_ratio = insight_user_revisit_ratio, join = insight_user_join, join_ratio = insight_user_join_ratio, visit_total = user_visit_total, visit_total_ratio = user_visit_total_ratio, grade_user = user_grade, sms_email_user = insight_user_email_sms,sms_email_ratio = insight_user_email_sms_ratio)
                    return insight_message
                    pass
                except:
                    return "fd_# 값과 순서를 다시한번 확인해 주시기 바랍니다.\n확인 후에도 결과가 나오지 않는 경우, \n```1. 쿼리의 결과값이 None일 가능성이 있습니다.\n2. 기존에 동일한 질문으로 진행중인 프로세스가 이미 존재할 가능성이 있습니다.\n3. 지난 2일 동안 user_all 테이블에 데이터가 수집되지 않았을 가능성이 있습니다.\n4. iMs 서비스 비활성 계정일 가능성이 있습니다.```\n\n5분 후에도 질문에 대한 응답 결과가 나오지 않을 경우 질문을 한번 더 해주세요^^"
                    pass

def integer_formatter(integer_input):
    if isinstance(integer_input, int):
        int_formatted_input = '{0:,d}'.format(integer_input)
        return int_formatted_input
    else:
        return None

def double_formatter(float_input):
    if isinstance(float_input, float):
        float_formatted_input = '{0:,.2f}'.format(float_input)
        return float_formatted_input
    elif isinstance(float_input, int):
        float_input = float(float_input)
        float_formatted_input = '{0:,.2f}'.format(float_input)
        return float_formatted_input
    else:
        return None

def math_calc(calc_type, calc_input1, calc_input2):
    if (isinstance(calc_input1, int) or isinstance(calc_input1, float)) and (isinstance(calc_input2, int) or isinstance(calc_input2, float)):
        if calc_type == "sum":
            calc_result = calc_input1 + calc_input2
            pass
        elif calc_type == "substraction":
            calc_result = calc_input1 - calc_input2
            pass
        elif calc_type == "division":
            numerator = calc_input1
            denominator = calc_input2
            try:
                calc_result = (numerator/denominator)
            except ZeroDivisionError:
                calc_result = 0
            pass
        elif calc_type == "percentage":
            numerator = calc_input1
            denominator = calc_input2
            try:
                calc_result = (numerator/denominator)*100
            except ZeroDivisionError:
                calc_result = 0
            pass
        else:
            calc_result = None
    else:
        calc_result = None
    return calc_result



