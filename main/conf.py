import requests
import json

def slack_msg(input_text, status='real'):
    # 발송 상태값 종류 : test, real
    # 테스트발송과 실발송
    
    if isinstance(input_text,str):
        msg_locate = input_text
        # --------------------------------------
        domain = 'https://hooks.slack.com/services/'
        if status == 'real':
            # Slack | real => Data manager_bot
            path = 'T050KJB1W/BGRUD4MEC/sk1sTx0PCBClBhssz4pq4W4P'
        else:
            # Slack | test => ims_operation_lab
            path = 'T050KJB1W/B95NXR075/OZkTQRyzrS6kmZhhTi3UXuEX'
        webhook_url = domain + path
        # --------------------------------------
        slack_data = {'text': msg_locate}
        response = requests.post(
            webhook_url, data=json.dumps(slack_data),
            headers={'Content-Type': 'application/json'}
            )
        if response.status_code != 200:
            raise ValueError(
                'Request to slack returned an error %s, the response is:\n%s'
                % (response.status_code, response.text)
                )
        pass
    else:
        input_text = str(input_text)
        slack_msg(input_text, 'real')
        pass
