from tastypie.authorization import Authorization
from tastypie.authentication import ApiKeyAuthentication
from tastypie.resources import ModelResource
from tastypie.serializers import Serializer
#from django.contrib.auth.models import User

from .models import Lead



class LeadResource(ModelResource):
    class Meta:
        always_return_data = False
        queryset = Lead.objects.all()
        resource_name = 'lead'
        authentication = ApiKeyAuthentication()
        authorization = Authorization()
        allowed_method = ['post',]
        serializer = Serializer(formats=['json'])
