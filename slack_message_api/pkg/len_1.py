from .configurations import *
from .comments import *
from .queries import *
from datetime import datetime
import requests
import pytz
import json
import time
import re
import os

# len_1 :: hi / help / feature / whoru / ims / qa / data

def len_1(*args):
    if args[0][0] == "hi":
        # datetime.now(pytz.timezone('Asia/Seoul')).strftime("%Y-%m-%d %H:%M:%S %z")
        current_hour = int(datetime.now(pytz.timezone('Asia/Seoul')).strftime("%H"))
        if current_hour > 6 and current_hour < 11 :
            msg_morning_hi = len_1_comment("morning") + len_1_comment("welcome")
            return msg_morning_hi
            pass
        elif current_hour == 11:
            msg_lunch_hi = len_1_comment("lunch") + len_1_comment("welcome")
            return msg_lunch_hi
            pass
        elif current_hour > 12 and current_hour < 20:
            msg_afternoon_hi = len_1_comment("afternoon") + len_1_comment("welcome")
            return msg_afternoon_hi
            pass
        elif current_hour > 19:
            msg_evening_hi = len_1_comment("evening") + len_1_comment("welcome")
            return msg_evening_hi
            pass
        else:
            say_hi = "반갑습니다! "+ len_1_comment("welcome")
            return say_hi
            pass

# =================================[1] 입력 단어 "help"=======================================
    elif args[0][0] == "help":
        msg_help = len_1_comment("help")
        return msg_help
        pass

# =================================[1] 입력 단어 "feature"=======================================
    # elif args[0][0] == "feature":
    #     msg_feature = len_1_comment("feature")
    #     return msg_feature
    #     pass

# =================================[1] 입력 단어 "whoru"=======================================
    elif args[0][0] == "whoru":
        msg_whoru = len_1_comment("whoru")
        return msg_whoru
        pass

# =================================[1] 입력 단어 "ims" / "qa" / "data" =======================================
    elif args[0][0] == "ims":
        msg_ims = len_1_comment("ims")
        return msg_ims
        pass

    elif args[0][0] == "qa":
        msg_qa = len_1_comment("qa")
        return msg_qa
        pass

    elif args[0][0] == "data":
        msg_data = len_1_comment("data")
        return msg_data
        pass

    elif args[0][0] == "ranker":
        msg_ranker = rank_yesterday()
        slack_msg_test(msg_ranker)
        rank_all = str(msg_ranker).replace("]","]\n")
        slack_msg_test(rank_all)
        pass

    elif args[0][0] == "system":
        msg_api_server = server_check()

        # main_database = main_db_check()[0]
        # main_running_query = main_running_queries()[0]
        # main_query_runtime = main_runtime_check()
        # main_node = main_node_cnt()[0]

        ds_database = ds_db_check()[0]
        ds_running_query = ds_running_queries()[0]
        ds_query_runtime = ds_runtime_check()
        ds_node = ds_node_cnt()[0]

        # if (main_database == "OK") and (ds_database == "OK") and (msg_api_server == 200):
        if (ds_database == "OK") and (msg_api_server == 200):
            # running_system_check 파라미터 변경하기!!!
            return running_system_check(msg_api_server, main_database, main_running_query, main_query_runtime, main_node, ds_database, ds_running_query, ds_query_runtime, ds_node)
            pass
        else:
            return running_system_status(4) + running_system_check(msg_api_server,main_database,main_running_query,main_query_runtime,ds_database,ds_running_query,ds_query_runtime)
            pass

    elif args[0][0] == "map":
        dictionary_table = len_1_comment("map")
        return dictionary_table
        pass

    elif args[0][0].startswith("map_"):
        ims_app = (args[0][0])[4:]
        dictionary_table = len_1_comment("map", ims_app_id = ims_app)
        return dictionary_table
        pass



    else:
        return not_get_comment(1,*args) 
        pass
    pass
