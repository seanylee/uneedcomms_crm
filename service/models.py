from django.db import models
from crm.models import ContactObject, CompanyObject
from sales.models import DealObject, DealMeta
from django.utils.crypto import get_random_string
from django.conf import settings

class TicketUserDefine(models.Model):
    class Meta:
        verbose_name            = 'Ticket Custom Field'
        verbose_name_plural     = 'Ticket Custom Fields'
        ordering                = ['-id']
        
    field_name = models.CharField('이름', null=False, blank=False, max_length=200)
    
    def __str__(self):
        return f'{self.field_name}'


class TicketObject(models.Model):
    class Meta:
        verbose_name            = 'Ticket'
        verbose_name_plural     = 'Tickets'
        ordering                = ['-id']

    TICKET_STATUS = (
            ('request','요청'),
            ('ongoing','처리중'),
            ('done','처리완료'),
            ('reject','반려'),
    )
    
    # ticket_number = models.CharField('티켓번호', null=False, blank=False, default=get_random_string(length=20), max_length=22,)
    ticket_company = models.ForeignKey(CompanyObject, null=True, blank=True, on_delete=models.SET_NULL, verbose_name='업체명',)
    ticket_contact = models.ForeignKey(ContactObject, null=True, blank=True, on_delete=models.SET_NULL, verbose_name='직원',)
    ticket_product = models.ForeignKey(DealMeta, null=True, blank=False, on_delete=models.SET_NULL, verbose_name='상품',)
    ticket_owner = models.ForeignKey(settings.AUTH_USER_MODEL,verbose_name='담당자',
	                    null=False, blank=False, on_delete=models.DO_NOTHING, help_text='티켓 담당자',)
    ticket_status = models.CharField(choices=TICKET_STATUS, max_length=10, default='request', verbose_name='진행 상태')
    ticket_category = models.ForeignKey(TicketUserDefine, on_delete=models.DO_NOTHING, verbose_name='타입', null=True,
						help_text='종류를 선택하세요')
    ticket_content = models.TextField('내용')
    ticket_created_at = models.DateField('제출일', null=True, blank=True, auto_now_add=True,)
    ticket_updated_at = models.DateField('수정일', null=True, blank=True, auto_now=True,)

    def __str__(self):
        return f'{self.id}'


#class TicketUserDefine(models.Model):
#    class Meta:
#        verbose_name			= 'Ticket Custom Field'
#        verbose_name_plural		= 'Ticket Custom Fields'
#        ordering				= ['-id']

#    field_name = models.CharField('이름', null=False, blank=False, max_length=200)

#    def __str__(self):
#        return f'{field_name}'
