from pygments import highlight
from pygments.lexers import JsonLexer
from pygments.formatters import HtmlFormatter

from django.contrib import admin
from django.utils.safestring import mark_safe
from .models import Challenges
import json

#@admin.register(Challenges)
class ChallengesObjectAdmin(admin.ModelAdmin):
    list_display = ['id','type','event_received_time','event_prettified']
    # list_display_links = ['id']
    
    #list_display = [field.name for field in Book._meta.fields if field.name != "id"]
    readonly_fields =['event_prettified'] + [c.name for c in Challenges._meta.fields]
    list_per_page=20
    
    def has_add_permission(self, request):
        return False
    def has_delete_permission(self, request, obj=None):
        return False

    def event_prettified(self, Challenges):
        response = json.dumps(Challenges.event, sort_keys=True, indent=2)
        response = response[:5000]
        formatter = HtmlFormatter(style='friendly')
        response = highlight(response, JsonLexer(), formatter)
        style = "<style>" + formatter.get_style_defs() + "</style><br>"
        return mark_safe(style + response)
    event_prettified.short_description = 'pretty_event'

#should not use decorator for 'prettifier'
admin.site.register(Challenges,ChallengesObjectAdmin)
