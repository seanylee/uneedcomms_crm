from .configurations import *
from .queries import *
from .comments import *
from .crawler import *
from datetime import datetime
import urllib.request, json 
import time
import pytz


def ims_f(*args):
    if len(args[0]) == 2:
# ===========================[2] 입력 단어 "ims active / ims inactive" ==============================
        if args[0][0] == "ims":
            if args[0][1] == "active":

                id_val = list()
                id_notpurchase = Active_notpurchase_domain()
                for domain in Active_domain():
                    if domain not in id_notpurchase:
                        id_val.append(domain)
                    else:
                        pass

                id_ = str(id_val)
                id_notpurchase_str = str(id_notpurchase)
                id_cnt = len(id_val)
                id_notpurchase_cnt = len(id_notpurchase)
                total_cnt = id_cnt + id_notpurchase_cnt
                ims_active_comment = ims_comment("active",id_cnt,id_,id_notpurchase_cnt,id_notpurchase_str,total_cnt)
                return ims_active_comment
                pass

            elif args[0][1] == "inactive":
                id_val = Inactive_domain()
                id_ = str(id_val)
                id_cnt = len(id_val)
                ims_inactive_comment = ims_comment("inactive",id_cnt,id_)
                return ims_inactive_comment
                pass


#                                                리포팅 관련!!
# -------------------------------------------- 엄청 불필요한 코드 반복! 수정이 필요!! ------------------------------------------------
            elif args[0][1] == "report0":
                slack_msg("활성화된 워크플로우의 발송 이상을 체크중입니다.\n영업일 -1일 (예외 - 토요일(금),일요일(금),월요일(금))에 워크플로우 발송 건수가 0건인 업체의 앱 아이디와 발송량을 확인중입니다.")
                id_val = ims_workflow_active()
                wf_active_companies = len(id_val)
                
                setting_list = list()
                sent_none = 0
                total_wf_active_cnt = 0
                
                for domain in id_val:
                    # 일요일 (-2 days)
                    if datetime.now(pytz.timezone('Asia/Seoul')).strftime("%w") == "0":
                        try:
                            check = workflow_sent_check(domain,0,0)
                            total_wf_active_cnt += int(len(check))
                            if len(check) != 0:
                                for one in check:
                                    workflow_idx = (one[0])
                                    workflow_status = (one[1])[:6]
                                    workflow_name = (one[2])
                                    wfw_sent_cnt = (one[3])
                                    command = (one[4])
                                    if int(one[3]) == 0:
                                        sent_none += 1
                                    comment_agg = "```{domain_} | workflow_idx : {wf_idx} | 워크플로우 상태 : {wf_status}\n워크플로우 이름 : {wf_name}\n메시지 발송량 : {sent_cnt} 건 (8일간의 발송량 확인 : {command_})```".format(domain_ = domain, wf_idx = workflow_idx, wf_status = workflow_status, wf_name = workflow_name, sent_cnt = wfw_sent_cnt, command_ = command)
                                    setting_list.append(comment_agg)
                                    pass
                        except:
                            pass
                    
                    # 월요일 (-3 days)
                    elif datetime.now(pytz.timezone('Asia/Seoul')).strftime("%w") == "1":
                        try:
                            check = workflow_sent_check(domain,1,0)
                            total_wf_active_cnt += int(len(check))
                            if len(check) != 0:
                                for one in check:
                                    workflow_idx = one[0]
                                    workflow_status = (one[1])[:6]
                                    workflow_name = one[2]
                                    wfw_sent_cnt = one[3]
                                    command = (one[4])
                                    if int(one[3]) == 0:
                                        sent_none += 1
                                    comment_agg = "```{domain_} | workflow_idx : {wf_idx} | 워크플로우 상태 : {wf_status}\n워크플로우 이름 : {wf_name}\n메시지 발송량 : {sent_cnt} 건 (8일간 발송량 확인 : {command_})```".format(domain_ = domain, wf_idx = workflow_idx, wf_status = workflow_status, wf_name = workflow_name, sent_cnt = wfw_sent_cnt, command_ = command)
                                    setting_list.append(comment_agg)
                                    pass
                        except:
                            pass
                    # 화요일 ~ 토요일 (-1 day)
                    else:
                        try:
                            check = workflow_sent_check(domain,6,0)
                            total_wf_active_cnt += int(len(check))
                            if len(check) != 0:
                                for one in check:
                                    workflow_idx = one[0]
                                    workflow_status = (one[1])[:6]
                                    workflow_name = one[2]
                                    wfw_sent_cnt = one[3]
                                    command = one[4]
                                    if int(one[3]) == 0:
                                        sent_none += 1
                                    comment_agg = "```{domain_} | workflow_idx : {wf_idx} | 워크플로우 상태 : {wf_status}\n워크플로우 이름 : {wf_name}\n메시지 발송량 : {sent_cnt}건 (8일간 발송량 확인 : {command_})```".format(domain_ = domain, wf_idx = workflow_idx, wf_status = workflow_status, wf_name = workflow_name, sent_cnt = wfw_sent_cnt, command_ = command)
                                    setting_list.append(comment_agg)
                        except:
                            pass
                setting_cnt = len(setting_list)
                setting_init_comment = "영업일 -1일 기준, `{company_cnt}개` 의 업체에서 운영중인 워크플로우 중\n발송건수가 0 인 활성 워크플로우의 수는 `{sent_none_}` 개 입니다.\n".format(sent_none_ = sent_none, company_cnt = wf_active_companies)
                setting_comment = str(",".join(setting_list)).replace(",","\n\n")
                setting_comment = setting_init_comment + setting_comment + "\n발송 체크가 종료되었습니다."
                return setting_comment
                pass

            elif args[0][1] == "report100":
                slack_msg("활성화된 워크플로우의 발송 이상을 체크중입니다.\n영업일 -1일 (예외 - 토요일(금),일요일(금),월요일(금))에 워크플로우 발송 건수가 100건 이하인 업체의 앱 아이디와 발송량을 확인중입니다.")
                id_val = ims_workflow_active()
                wf_active_companies = len(id_val)
                
                setting_list = list()
                sent_none = 0
                total_wf_active_cnt = 0
                
                for domain in id_val:
                    # 일요일 (-2 days)
                    if datetime.now(pytz.timezone('Asia/Seoul')).strftime("%w") == "0":
                        try:
                            check = workflow_sent_check(domain,0,100)
                            total_wf_active_cnt += int(len(check))
                            if len(check) != 0:
                                for one in check:
                                    workflow_idx = (one[0])
                                    workflow_status = (one[1])[:6]
                                    workflow_name = (one[2])
                                    wfw_sent_cnt = (one[3])
                                    command = (one[4])
                                    if int(one[3]) == 0:
                                        sent_none += 1
                                    comment_agg = "```{domain_} | workflow_idx : {wf_idx} | 워크플로우 상태 : {wf_status}\n워크플로우 이름 : {wf_name}\n메시지 발송량 : {sent_cnt} 건 (8일간의 발송량 확인 : {command_})```".format(domain_ = domain, wf_idx = workflow_idx, wf_status = workflow_status, wf_name = workflow_name, sent_cnt = wfw_sent_cnt, command_ = command)
                                    setting_list.append(comment_agg)
                                    pass
                        except:
                            pass
                    
                    # 월요일 (-3 days)
                    elif datetime.now(pytz.timezone('Asia/Seoul')).strftime("%w") == "1":
                        try:
                            check = workflow_sent_check(domain,1,100)
                            total_wf_active_cnt += int(len(check))
                            if len(check) != 0:
                                for one in check:
                                    workflow_idx = one[0]
                                    workflow_status = (one[1])[:6]
                                    workflow_name = one[2]
                                    wfw_sent_cnt = one[3]
                                    command = (one[4])
                                    if int(one[3]) == 0:
                                        sent_none += 1
                                    comment_agg = "```{domain_} | workflow_idx : {wf_idx} | 워크플로우 상태 : {wf_status}\n워크플로우 이름 : {wf_name}\n메시지 발송량 : {sent_cnt} 건 (8일간 발송량 확인 : {command_})```".format(domain_ = domain, wf_idx = workflow_idx, wf_status = workflow_status, wf_name = workflow_name, sent_cnt = wfw_sent_cnt, command_ = command)
                                    setting_list.append(comment_agg)
                                    pass
                        except:
                            pass
                    # 화요일 ~ 토요일 (-1 day)
                    else:
                        try:
                            check = workflow_sent_check(domain,6,100)
                            total_wf_active_cnt += int(len(check))
                            if len(check) != 0:
                                for one in check:
                                    workflow_idx = one[0]
                                    workflow_status = (one[1])[:6]
                                    workflow_name = one[2]
                                    wfw_sent_cnt = one[3]
                                    command = one[4]
                                    if int(one[3]) == 0:
                                        sent_none += 1
                                    comment_agg = "```{domain_} | workflow_idx : {wf_idx} | 워크플로우 상태 : {wf_status}\n워크플로우 이름 : {wf_name}\n메시지 발송량 : {sent_cnt}건 (8일간 발송량 확인 : {command_})```".format(domain_ = domain, wf_idx = workflow_idx, wf_status = workflow_status, wf_name = workflow_name, sent_cnt = wfw_sent_cnt, command_ = command)
                                    setting_list.append(comment_agg)
                        except:
                            pass
                setting_cnt = len(setting_list)
                setting_init_comment = "영업일 -1일 기준, `{company_cnt}개` 의 업체에서 운영중인 워크플로우 중\n총 `{active_wfs}개` 중에 `{cnt_}개` 의 워크플로우에서 발송 건수 100회 이하의 수치가 감지되었습니다.\n발송건수가 0 인 활성 워크플로우의 수는 `{sent_none_}` 개 입니다.\n".format(cnt_=setting_cnt, sent_none_ = sent_none, company_cnt = wf_active_companies, active_wfs = total_wf_active_cnt)
                setting_comment = str(",".join(setting_list)).replace(",","\n\n")
                setting_comment = setting_init_comment + setting_comment + "\n발송 체크가 종료되었습니다."
                return setting_comment
                pass

# -------------------------------------------- 엄청 불필요한 코드 반복! 수정이 필요!! ------------------------------------------------





# ===========================| 입력 단어 ims active_detail & ims_inactive_detail |==============================
# 
            elif args[0][1] == "active_detail":
                active_detail = ims_in_active_detail("active")
                slack_msg("iMs 서비스를 이용중인 전 업체의 서비스 종료일과 구독 상태 정보를 전달드립니다. (종료일 기준 오름차순)")
                detail_comment = ""
                for detail in active_detail:
                    shop_name, domain, ims_app_id, domain_id, end_date, subscription_status = detail
                    ims_app_id = str(ims_app_id).strip()
                    detail_comment += "*{sn}({iai})* 의 종료일 : `{ed}` | 월 구독 상태 : `{ss}`\n".format(sn = str(shop_name), dm = str(domain), iai = ims_app_id, ed = str(end_date), ss = str(subscription_status))
                
                    payment_history = str(show_payment(ims_app_id, "monthly"))
                    detail_comment += "\n*가장 최근에 일어난 결제 이력 3건*\n" + "```" + "  결제일시  |  결제성공여부  |  결제상태  |  금액  |  결제방식  |  상품상세  |  통화 정보  \n\n" + payment_history.replace("),","),\n") + "```" + "\n\n"

                return detail_comment
                pass
            
            elif args[0][1] == "inactive_detail":
                inactive_detail = ims_in_active_detail("inactive")
                slack_msg("iMs 서비스를 이탈한 업체의 과거의 최종 서비스 종료일 정보를 전달드립니다. (종료일 기준 내림차순)")
                detail_comment = ""
                for detail in inactive_detail:
                    shop_name, domain, ims_app_id, domain_id, end_date, subscription_status = detail
                    ims_app_id = str(ims_app_id).strip()
                    detail_comment += "*{sn}({iai})* 의 종료일 : `{ed}`\n".format(sn = str(shop_name), dm = str(domain), iai = str(ims_app_id), ed = str(end_date))
                return detail_comment
                pass
            
# ===========================| 입력 단어 "ims workflow" / "ims uneedcomms workflow_active" / "ims uneedcomms workflow_inactive" / ims uneedcomms |==============================
# 도메인 액티브 상태 -> 워크플로우 액티브 & 액티브 된 워크플로우 정보 호출

            elif args[0][1] == "workflow":
                wf_message = workflow_comment()
                return wf_message
                pass

            elif args[0][1] == "workflow_active":
                wf_active = ims_workflow_active()
                wf_count = len(wf_active)
                wf_active_comment = ims_comment("workflow_active",wf_count,wf_active)
                return wf_active_comment
                pass

            elif args[0][1] == "workflow_inactive":
                domains = Active_domain()
                wf_active = ims_workflow_active()
                wf_inactive = list()
                for inact in domains:
                    if inact not in wf_active:
                        wf_inactive.append(inact)
                wf_inact_count = len(wf_inactive)
                wf_inactive_comment = ims_comment("workflow_inactive",wf_inact_count,wf_inactive)
                return wf_inactive_comment
                pass

            elif args[0][1] == "hosting":
                mk_hosting = list()
                cf_hosting = list()
                gd_hosting = list()
                other_hosting = list()
                matching_result = app_host_matching()
                for mat in matching_result:
                    if mat[1] == "mk":
                        mk_hosting.append(mat[0].strip())
                    elif mat[1] == "cf":
                        cf_hosting.append(mat[0].strip())
                    elif mat[1] == "gd":
                        gd_hosting.append(mat[0].strip())
                    else:
                        other_hosting.append(mat[0].strip())

                mk_len = len(mk_hosting)
                cf_len = len(cf_hosting)
                gd_len = len(gd_hosting)
                ot_len = len(other_hosting)
                hosting_comment = ims_comment("hosting", cf_hosting, cf_len, mk_hosting, mk_len, gd_hosting, gd_len, other_hosting, ot_len)
                return hosting_comment
                pass

# ------------------------------------------기능 추가구현 필요 시작-------------------------------------
            # elif args[0][1] == "owner":
            #     app_id = app_host_matching()
            #     for app in app_id:


            #         associatedcompanyid = app[2]
            #         hubs_api = hubspot_api("get_a_company",associatedcompanyid)
            #         company_portal_url = hubspot_api("company_portal",associatedcompanyid)
            #         with urllib.request.urlopen(hubs_api) as url:
            #             data = json.loads(url.read().decode())
            #             print(str(data)+"\n\n\n")
            #             ims_owner = data['properties']['hubspot_owner_id']['sourceId'].split('@',1)[0]
            #             if ims_owner == None:
            #                 comment = "업체명 : "+app[0]+"\n담당자 : 없음"+"\n<허브스팟 포털 바로가기|"+company_portal_url+">"
            #                 slack_msg (comment)
            #             elif ims_owner != None:
            #                 comment = "업체명 : "+app[0]+"\n담당자 : "+ims_owner+"\n<"+company_portal_url+"|{}의 허브스팟 포털 바로가기>".format(app[0])
            #                 slack_msg (comment)
            #                 pass
            #             else:
            #                 slack_msg("1")
# -------------------------------------------------기능 추가구현 필요 끝-------------------------------------

            else:
                if args[0][1] in Whole_domain():
                    show_ = show_ims(args[0][1])
        
                    # =====| 허브스팟 오너와 포털 정보 추출 |=====
                    # 위의 "show_ims" 에서 associatedcompany_id (show_[7]) 추출
                    hubs_comment = '준비중입니다.'
                    #if show_[7] != None:
                    try:
                        associatedcompanyid = show_[7]
                        hubs_api = hubspot_api("get_a_company",associatedcompanyid)
                        company_portal_url = str(hubspot_api("company_portal",associatedcompanyid)).strip()
                        try:
                            with urllib.request.urlopen(hubs_api) as url:
                                data = json.loads(url.read().decode())
                                # print(str(data)+"\n\n\n")
                                hubspot_owner = data['properties']['hubspot_owner_id']['sourceId'].split('@',1)[0]
                                
                                if data['properties']['tier']['value'] != None:
                                    hubspot_tier = str(data['properties']['tier']['value']) 
                                else: 
                                    hubspot_tier = '정의된 티어 없음'

                                if data['properties']['address']['value'] != None:
                                    hubspot_address = str(data['properties']['address']['value']) 
                                else: 
                                    hubspot_address = '정의된 주소 없음'

                                if hubspot_owner == None:
                                    hubs_comment = "업체명 : "+show_[0]+"\n담당자 : 없음"+"\n<허브스팟 포털 바로가기|"+company_portal_url+">"
                                    # print (comment)
                                elif hubspot_owner != None:
                                    hubs_comment = "*허브스팟 관련 정보*\n```허브스팟 오너 : " + hubspot_owner + "\n내부등급 : " + hubspot_tier + "\n회사 위치 : " + hubspot_address + "```\n<" + company_portal_url + "|{}의 허브스팟 포털 바로가기>".format(args[0][1])
                                    # print (comment)
                                    
                                else:
                                    slack_msg("{app_id}의 허브스팟 정보가 올바르게 입력되지 않았습니다.\n허브스팟에서 업체등급과 주소정보 그리고 허브스팟 오너가 지정되어 있는지 확인 바랍니다.".format(app_id = args[0][1]))
                                    pass
                        except:
                            no_asscom = show_[7]
                            hubs_comment = "*허브스팟 관련 정보*\n```{app_id}의 허브스팟 'Associated_company_id' 값, {ass_com}를 찾을 수 없습니다.```".format(app_id = args[0][1], ass_com = show_[7])
                            pass
                    except:
                        #hubs_comment = "허브스팟 Associated_company_id 가 존재하지 않습니다."
                        pass

                    # =====| 카카오 플러스친구 수 호출 |=====
                    try:
                        kakaoplus = kakaoplusdict[args[0][1]]
                        member_cnt = getUsers(kakaoplus)
                        member_comment = "*카카오플러스친구*\n```카카오 플러스 친구 수 : {member_num} 명```".format(member_num = member_cnt)
                    except:
                        member_comment = "*카카오플러스친구*\n```카카오 플러스 계정을 찾을 수 없습니다.```"
                    # =====| 결제 이력 호출 |=====
                    # 7개 컬럼으로 구성된 튜플반환 (paid_time, success, status, amount, pay_method, name, currency)
                    payment_history = str(show_payment(args[0][1], "limit3"))
                    payment_history = "*가장 최근에 일어난 결제 이력 3건*\n" + "```" + "  결제일시  |  결제성공여부  |  결제상태  |  금액  |  결제방식  |  상품상세  |  통화 정보  \n\n" + payment_history.replace("),","),\n") + "```"
                    
                    # ==== | 업체 상세정보 호출 |====
                    # 상단의 show_ims 에서 리턴받은 값을 조합하여 코멘트를 조합 및 추출
                    ims_app_detail_comment = ims_comment("ims_app_detail", args[0][1], show_[6], show_[0], show_[1], show_[2], show_[3], show_[4], show_[5], show_[7], show_[8], show_[9], show_[10])
                    ims_command = str(ims_comment("ims_app_detail_command", args[0][1]))
                    ims_app_domain_detail = ims_app_detail_comment + '\n' + payment_history + '\n\n' + member_comment + '\n\n' + hubs_comment + '\n\n' + ims_command
                    return ims_app_domain_detail
                    pass

                else:
                    try:
                        domain_list = Whole_domain()
                        for li in domain_list:
                            if args[0][1] in li:
                                comment = "입력하신 \"{input_id}\"은/는 존재하지 않는 앱 아이디 입니다.\n찾으시는 iMs 앱 아이디가 혹시 `{ims_app_id}` 인가요?".format(ims_app_id = li, input_id = args[0][1])
                                return comment
                                pass
                    except:
                        not_get_comm = not_get_comment(2, *args)
                        return not_get_comm
                        pass

                    not_get_comm = not_get_comment(2, *args)
                    return not_get_comm
                    pass

    elif len(args[0]) == 3:
        if args[0][0] == "ims":
            domains = Active_domain()
            if args[0][1] in domains:
                if args[0][2] == "workflow":
                    wf_whole = workflow_whole(args[0][1])
                    wf_whole_cnt = len(wf_whole)
                    wf_whole_summary = ims_comment("workflow_app_whole",args[0][1],wf_whole_cnt)
                    wf_whole_comment = ",".join(wf_whole)
                    wf_whole_comment_agg = wf_whole_summary + '\n' + '```' + wf_whole_comment + '```'
                    return wf_whole_comment_agg
                    pass

                elif args[0][2] == "workflow_active":
                    wf_active = workflow_active(args[0][1])
                    wf_active_cnt = len(wf_active)
                    wf_active_summary = ims_comment("workflow_app_active",args[0][1],wf_active_cnt)
                    wf_active_comment = ",".join(wf_active)
                    wf_active_comment_agg = wf_active_summary + '\n' + '```' + wf_active_comment + '```'
                    return wf_active_comment_agg
                    pass

                elif args[0][2] == "workflow_inactive":
                    wf_inactive = workflow_inactive(args[0][1])
                    wf_inactive_cnt = len(wf_inactive)
                    wf_inactive_summary = ims_comment("workflow_app_inactive",args[0][1],wf_inactive_cnt)
                    wf_inactive_comment = ",".join(wf_inactive)
                    wf_inactive_comment_agg = wf_inactive_summary + '\n' + '```' + wf_inactive_comment + '```'
                    return wf_inactive_comment_agg
                    pass
                else:
                    not_get_comm = not_get_comment(3,*args)
                    return not_get_comm
                    pass
            else:
                invalid_id_comment = ims_comment("invalid_app_id",args[0][1])
                return invalid_id_comment
                pass
        else:
            not_get_comm = not_get_comment(3,*args)
            return not_get_comm
            pass

    # 특정업체 지난 7일간 전체 워크플로우 발송 정보 확인하기
    elif len(args[0]) == 4:
        if (args[0][1] in Active_domain()) and (args[0][2] == "workflow") and (args[0][3] == "sent"):
            slack_msg("지난 8일 동안의 일 단위 전체 워크플로우 발송정보를 준비중입니다.")
            wf_send_log = workflow_sent_cnt(args[0][1],0)
            comment_list = list()
            for row in wf_send_log:
                date = row[0]
                sent = row[1]
                view = row[2]
                view_ratio = row[3]
                comment_agg = "날짜 : {date_} | 발송량 : {sent_}건 | 오픈수 : {view_}건 | 오픈률 : {view_ratio_}%".format(date_ = date, sent_ = sent, view_ = view, view_ratio_ = view_ratio)
                comment_list.append(comment_agg)
            comment_list = str(",".join(comment_list)).replace(",","\n")
            head_comment = "{ims_app_id}의 일 단위 워크플로우 발송 및 열람 수 입니다.\n".format(ims_app_id = args[0][1])
            return head_comment+"```"+comment_list+"```"
        else:
            not_get_comm = not_get_comment(3,*args)
            return not_get_comm
            pass



    # 워크플로우 샌더컨텐츠 정보 확인!! (등록 샌더 전부!)
    elif len(args[0]) == 5:
        if (args[0][1] in Active_domain()) and (args[0][2] == "workflow") and (args[0][4] == "sender"):
            workflow_idx = args[0][3]
            seneder_comment = list()
            for se in workflow_sender(args[0][1],workflow_idx):
                content_idx = se[0]
                template_type = se[1]
                send_type = se[2]
                workflow_name_ = se[3]
                content_title = se[4]
                url = se[5]
                image_url = se[6]
                msg_agg = 'content_idx : ' + content_idx + ' | ' + '템플릿 타입 : ' + template_type+' | ' + '발송 타입 : ' + send_type+' | ' + '컨텐츠 타이틀(링크 바로가기) : ' + '<' + url + '|' +content_title+'>' + '\n'
                seneder_comment.append(msg_agg)
            return '```'+workflow_name_+'```'+'\n'+'전체 이미지 보기 : '+image_url+'\n'+",".join(seneder_comment).replace(',','')

        # 특정 업체의 특정 워크플로우 메시지 발송량 확인
        elif (args[0][1] in Active_domain()) and (args[0][2] == "workflow") and (args[0][4] == "sent"):
            workflow_idx = args[0][3]
            msg = "지난 8일 동안의 {ims_app_id}의 {workflow_idx_}번 워크플로우\n일 단위 전체 워크플로우 발송정보를 준비중입니다.".format(ims_app_id = args[0][1], workflow_idx_ = workflow_idx)
            slack_msg(msg)
            seneder_comment = list()
            for wf_send_log in workflow_sent_cnt(args[0][1],workflow_idx):
                date = wf_send_log[0]
                sent = wf_send_log[1]
                view = wf_send_log[2]
                view_ratio = wf_send_log[3]
                comment_agg = "날짜 : {date_} | 발송량 : {sent_}건 | 오픈수 : {view_}건 | 오픈률 : {view_ratio_}%".format(date_ = date, sent_ = sent, view_ = view, view_ratio_ = view_ratio)
                seneder_comment.append(comment_agg)
            seneder_comment = str(",".join(seneder_comment)).replace(",","\n")
            head_comment = "{ims_app_id}의 {workflow_idx_}번 워크플로우 일 단위 워크플로우 발송 및 열람 수 입니다.\n".format(ims_app_id = args[0][1], workflow_idx_ = workflow_idx)
            return head_comment+"```"+seneder_comment+"```"
        
        else:
            not_get_comm = not_get_comment(3,*args)
            return not_get_comm
            pass
    else:
            not_get_comm = not_get_comment(3,*args)
            return not_get_comm
            pass




