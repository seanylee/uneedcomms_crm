import random
import time


# ================| comment for "system" |=====================

def running_system_check(msg_api_server, main_database, main_running_query, main_query_runtime, main_node, ds_database, ds_running_query, ds_query_runtime, ds_node):
    check_result = "시스템 체크 결과를 전달드려요!\n*API 서버 : {api_server}*\n\n<http://hive.samantha.world:9090/|*메인 쿼리 서버*>\n```서버 상태 : {main_query_server}\n활성 노드/워커 수 : {main_act_node}개\n실행중인 전체 쿼리 수 : '{main_total_query}'개\n30분 이상 실행중인 쿼리 수 : '{main_running_query_}'개 입니다.```\n\n<http://ds-presto.samantha.world:8080/|*DS 쿼리 서버*>\n```서버 상태 : {ds_query_server}\n활성 노드/워커 수 : {ds_act_node}개\n실행중인 전체 쿼리 수 : '{ds_total_query}'개\n30분 이상 실행중인 쿼리 수 : '{ds_running_query_}'개 입니다.```\n".format(api_server = msg_api_server, main_query_server = main_database, main_total_query = main_running_query, main_running_query_ = main_query_runtime, main_act_node = main_node, ds_query_server = ds_database, ds_total_query = ds_running_query, ds_running_query_ = ds_query_runtime, ds_act_node = ds_node)
    return check_result

def running_system_status(status_code):
    if status_code == 1:
        return "*현재 매우 쾌적한 상태입니다. 원하시는 결과를 빠르게 받아보실 수 있습니다.*"
        pass
    elif status_code == 2:
        return "*결과가 다소 늦게 산출될 수 있습니다. 참고 부탁드립니다.*"
        pass
    elif status_code == 3:
        return "*이미 너무 많은 쿼리가 돌아가고 있습니다. 결과가 매우 늦게 산출될 것으로 예측됩니다.*"
        pass
    elif status_code == 4:
        return "`비정상이 감지되었습니다!`\n"
        pass
    else:
        pass

def not_get_comment(word_len,*input_):
    if word_len == 1:
        return "\n질문에 대한 의도를 잘 모르겠어요.\n입력하신 *\"{}\"* 는 등록되지 않은 명령어인 것으로 보여요!\n입력창에 `@Q help` 를 입력해서 자세한 가이드를 확인해 보세요.\n저에 대한 추가적인 문의는 운영팀 이완선 주임에게 전달 부탁드립니다.".format(input_[0][0])
    elif word_len == 2:
        if input_[0][0] == "ims":
            return "\n질문에 대한 의도를 잘 모르겠어요.\n입력하신 *\"{}\"* 는 유효한 ims_app_id가 아니거나, 등록되지 않은 명령어인 것으로 보여요!\n입력창에 `@Q help` 를 입력해서 자세한 가이드를 확인해 보세요.\n저에 대한 추가적인 문의는 운영팀 이완선 주임에게 전달 부탁드립니다.".format(input_[0][1])
    else:
        return "\n입력하신 *\"{}\"* 에 대한 의도를 잘 모르겠어요.\n입력창에 `@Q help` 를 입력해서 자세한 가이드를 확인해 보세요.\n저에 대한 추가적인 문의는 운영팀 이완선 주임에게 전달 부탁드립니다.".format(input_[0])


# ================| comment for "len_1" |=====================

def len_1_comment(len_comment_input, ims_app_id = "uneedcommscom"):
    Q_welcome = '\n        /\_/\ \n      ((@v@)) \n      ( ) : : : ( ) \n       VV-VV \n     /   __    \ \n   /   /  /   / \n/   /_/   / \n\_____\_\  \n'

    if len_comment_input == "morning":
        comment = [
        '좋은 아침입니다!', 
        '상쾌한 아침입니다!', 
        '저와함께 모닝커피 한잔 어떠신가요? 블랑드티에서 기다릴게요~', 
        '오늘 할 일을 정리해보는건 어떨까요?', 
        '굿모닝! 활기찬 하루 되세요^^', 
        '와! 아침부터 절 찾아주셔서 너무 고마워요 >_<', 
        '좋은 아침이에요! 항상 열심히 하는 모습이 너무 멋있어요!',
        '오늘 하루도 화이티이이잉!!'
        ]
        random_comment = random.choice(comment)
        return Q_welcome+"\n"+"반갑습니다! \n"+random_comment
        pass

    elif len_comment_input == "lunch":
        comment = [
            '점심 메뉴는 무엇으로 고르셨나요?', 
            '오늘 저는 한식이 땡기는데 한식 어떠세요?', 
            '저는 오늘 중식이 땡기는데... 중식 어떠세요?', 
            '저는 오늘 일식이 땡겨요! 일식 어떠세요?', 
            '빨리요! 점심 메뉴를 골라야해요!', 
            '하루 중 가장 심각하게 고민하는 대상! 바로 점심메뉴!',
            '저 지금 매우 심각하게 점심메뉴 생각하는 중이에요...'
            ]
        random_comment = random.choice(comment)
        return Q_welcome+"\n"+"반갑습니다! \n"+random_comment
        pass

    elif len_comment_input == "afternoon":
        comment = [
            '기지개 한번 켜고 계속 하는건 어떠신가요?',
            '스트레칭이 필요한 시간이에요! :)',
            '항상 멋지게 일하는 모습에 반했어요!'
            ]
        random_comment = random.choice(comment)
        return Q_welcome+"\n"+"안녕하세요! 반갑습니다! \n"+random_comment
        pass

    elif len_comment_input == "evening":
        comment = [
            '아니, 아직도 근무중이신가요? 몸 생각해야지요!', 
            '이 늦은 밤에도 일을 놓치 못하는 당신! 지금 바로 떠나세요! 1588-1588', 
            '너무 늦게까지 일하면 몸 상해요ㅠㅠ',
            '달리는것도 좋지만 쉴땐 쉬어야지요!',
            '저녁은 먹고 일하고 계신가요ㅠㅠ?'
            ]
        random_comment = random.choice(comment)
        return Q_welcome+"\n"+"반갑습니다! \n"+random_comment
        pass

    elif len_comment_input == "welcome":
        return "\n\n# 제가 누구인지 알고 싶다면 제게 `@Q whoru` 를 입력해 질문을 해주세요.\n# 제가 도와드릴 수 있는 일이 무엇인지 알고 싶다면 제게 `@Q help`를 입력해 주세요."
        pass
    elif len_comment_input == "help":
        return "안녕하세요. iMs 운영 챗봇 *\'Q\'* 에 대해 소개드리겠습니다.\nQ봇은 iMs의 원활한 운영을 위하여 작동되는 Real-Time Interactive 봇입니다.\n제가 여러분의 질문에 자신있게 답변드릴 수 있는 주제는 크게 3가지 입니다.```\n1. iMs 운영관련 문의\n2. QA 관련 문의\n3. Data 분석 관련 문의```\n\n# 위 주제에 대한 설명은 `@Q ims`, `@Q qa`, `@Q data`를 통해 가능해요! *원하는 데이터를 찾을 수 있도록 제가 옆에서 가이드를 드릴게요!*\n# 찾고자 하는 *목적 데이터가 명확* 하거나, 제가 가진 *기능 전체를* 확인하고 싶으시면, `@Q map` 을 입력해서 명령어 지도를 호출해 주세요!\n## 데이터베이스 및 API 서버상태, 현재 돌아가고 있는 쿼리 수 확인 등은 `@Q system` 을 입력해 주세요.\n# 혹시.. 제가 누구인지 궁금하시다면, `@Q whoru` 로 제게 물어봐 주세요. *_*"
        pass
    elif len_comment_input == "whoru":
        return "안녕하세요! 제 이름은 \'Q(큐)\'' 에요!\nQ 모양이 마치 무엇인가를 찾는 돋보기 모양 같지요? \n\n제게 물어볼 것이 있다면 언제든 `\'@Q\'` 와 함께 문의 사항을 입력해 주세요. \n여러분을 위해 저는 항상 깨어 있어요!\n\n저는 데이터에 대한 내부적 수요가 빠르게 증가함에 따라 \n지난 3월, iMs 운영팀과 RE팀의 도움으로 태어나게 됐어요!\n\n전 이제 갓 태어났어요.. 그래서 여러분들의 많은 도움이 필요하답니다^^\n# 그래도 벌써 할 줄 아는게 꽤 있어요! `@Q map`를 입력하면 제가 뭘 할 수 있는지 알려드릴게요!"
        pass
    elif len_comment_input == "ims":
        return "어서오세요^__^ iMs운영과 관련해서 궁금한것이 무엇인가요?\n## iMs 이용 업체의 *쇼핑몰 호스팅 정보* 확인은 `@Q ims hosting` 입력을 통해 가능해요!\n## iMs의 *활성 혹은 비활성 계정* 확인은 `@Q ims active` | `@Q ims inactive` 입력을 통해 가능해요!\n# iMs *워크플로우 운영정보* 확인은 `@Q ims workflow` 통해 가능하답니다."
        pass
    elif len_comment_input == "qa":
        return "우와우! 반갑습니다! iMs서비스의 QA를 할 수 있는 다양한 데이터를 제공해 드려요!\n아직 QA관련 기능이 많지 않아요ㅠㅠ 차근차근 채워나갈 예정이니 기대해주세요!\n\n## 어제자 작업지시서 오류 현황 파악을 위해서는 `@Q qa job_fail`을 입력해 주세요!"
        pass
    elif len_comment_input == "data":
        return "*데이터 섹션* 에 오신것을 환영합니다! 데이터 분석에 관련된 질문을 하실경우, \n\n## 사전에 `@Q system` 입력을 통해 *내부 리소스 상태* 를 한번 확인하는것도 좋아요^^!\n### 업체별 *회원 종합 분석* 정보를 받아보고 싶은 경우엔 `@Q data ### report_report` 를 입력해 주세요.(### = ims_app_id)\n## iMs 를 이용중인 업체의 *앱 아이디(ims_app_id)* 확인을 위해서는 `@Q ims active`를 입력해 주세요."
        pass
    elif len_comment_input == "map":
        return """운영챗봇 Q의 명령어 지도 입니다.
"Q"
 ├─ `@Q hi` : 시간대별 랜덤 인사 및 챗봇안내
 ├─ `@Q help` : 도움말 호출
 ├─ `@Q whoru` : Q에 대한 소개
 ├─ `@Q map` / `@Q map_{{ims_app_id}}` : 명령어 지도 호출
 ├─ `@Q ims` : 운영 기능관련 명령어 목록
 │        ├ `@Q ims hosting` : iMs 전 업체의 쇼핑몰 CMS 정보 확인 (카페/메이크샵/고도몰/그 외)
 │        ├ `@Q ims active` : iMs 계정이 활성 상태인 업체 확인
 │        │      └ (준비중)`@Q ims active_detail` : iMs 계정 활성 업체의 상세정보(서비스 완료일, 과금정보)
 │        ├ `@Q ims inactive` : iMs 계정이 비활성 상태인 업체 확인
 │        │      └ (준비중)`@Q ims inactive_detail` : iMs 계정 비활성 업체의 상세정보(서비스 완료일)   
 │        ├ `@Q ims report0` : 전 업체 iMs 메시지 발송량이 0인 업체 모니터링
 │        ├ `@Q ims report100` : 전 업체 iMs 메시지 발송량이 100 이하인 업체 모니터링
 │        ├ `@Q ims {id}` : iMs 이용업체의 상세정보
 │        └ `@Q ims workflow` : 워크플로우 관련 명령어 목록
 │                      ├ `@Q ims workflow_active` : *워크플로우를 운영중인* 업체 목록 및 업체 수
 │                      ├ `@Q ims workflow_inactive` : *워크플로우를 운영하지 않는* 업체 목록 및 업체 수
 │                      └ `@Q ims {id} workflow` : 특정업체의 *활성 & 비활성 워크플로우* 정보
 │                                    ├ `@Q ims {id} workflow_active` : 특정업체의 *활성 워크플로우* 정보
 │                                    ├ `@Q ims {id} workflow_inactive` : 특정업체의 *비활성 워크플로우* 정보
 │                                    ├ `@Q ims {id} workflow ### sender` : 특정업체 ###번 워크플로우의 *샌더컨텐츠* 정보
 │                                    ├ `@Q ims {id} workflow sent` : 지난 7일간 특정업체 *워크플로우 전체* 메시지 발송량 확인
 │                                    └ `@Q ims {id} workflow ### sent` : 특정업체 *###번 워크플로우의* 메시지 발송량 확인 
 ├─ `qa` : QA 기능 관련 명령어 호출
 │         ├ `qa job_fail` : 어제자 *작업지시서 오류* 현황 파악 (파이프라인코드 40, 112 가 아닌 경우)
 │         └ (준비중) `qa data_fail` : 어제자 *이빨빠진 데이터가* 있는 업체 파악 (유저, 주문 데이터) 
 └─ `@Q data` : 데이터 관련
           ├ `@Q data {id} user_report` : {id} *유저* 관련 *데이터 분석* 도출
           │             └ `@Q data {id} user_report fd_##### fd_##### fd_##### fd_##### fd_#####` : 회원 종합 분석
           ├ (준비중) `@Q data {id} order_report` : uneedcommscom의 *주문* 관련 *데이터 분석* 도출
           │             └ `@Q data {id} order_report fd_##### fd_##### fd_##### fd_##### fd_#####` : 주문 종합 분석
           ├ (준비중) `@Q data {id} product_report` : uneedcommscom의 *상품* 관련 *데이터 분석* 도출
           │             └ `@Q data {id} product_report fd_##### fd_##### fd_##### fd_##### fd_#####` : 상품 종합 분석
           │
           ├ `@Q data {id} page_view` : {id} 의 페이지 뷰 관련 데이터 분석 (오늘로부터 지난 14일간의 데이터)
           ├ `@Q data {id} revisit_web 2018-##`: 2018년 ##달 {id} 의 웹 재방문 정보 분석
           ├ `@Q data {id} revisit_app 2018-##`: 2018년 ##달 {id} 의 앱 재방문 정보 분석
           ├ `@Q data {id} repurchase_web 2018-##`: 2018년 ##달 {id} 의 웹 재구매 정보 분석
           ├ `@Q data {id} repurchase_app 2018-##`: 2018년 ##달 {id} 의 앱 재구매 정보 분석
           │
           ├ `@Q data {id} ims_revisit_web 2018-##`: 2018년 ##달 iMs 메시지 발송된 고객의 웹 재방문 정보 분석
           ├ `@Q data {id} ims_revisit_app 2018-##`: 2018년 ##달 iMs 메시지 발송된 고객의 앱 재방문 정보 분석
           ├ `@Q data {id} ims_repurchase_web 2018-##`: 2018년 ##달 iMs 메시지 발송된 고객의 웹 재구매 정보 분석
           ├ `@Q data {id} ims_repurchase_app 2018-##`: 2018년 ##달 iMs 메시지 발송된 고객의 앱 재구매 정보 분석
           │
           ├ `@Q data {id} user_table` : {id} 의 *회원 테이블* 전체 헤더값 정의 ('fd_######' : '값')
           │             ├ `@Q data {id} user_table fd_######` : {id} 의 회원테이블에서 *fd_##### 의 값 검색*
           │             └ `@Q data {id} user_table 회원` : {id} 회원테이블에서 "회원" *문자열이 포함된* 모든 헤더값 검색 
           ├ `@Q data {id} order_table` : {id} 의 *주문 테이블* 전체 헤더값 정의 ('fd_######' : '값')
           │             ├ `@Q data {id} order_table fd_######` : {id} 의 주문테이블에서 *fd_##### 의 값 검색*
           │             └ `@Q data {id} order_table 주문` : {id} 회원테이블에서 "주문" *문자열이 포함된* 모든 헤더값 검색 
           └ `@Q data {id} product_table` : {id} 의 *상품 테이블* 전체 헤더값 정의 ('fd_######' : '값')
                         ├ `@Q data {id} product_table fd_######` : {id} 의 상품테이블에서 *fd_##### 의 값 검색*
                         └ `@Q data {id} product_table 번호` : {id} 의 회원테이블에서 "번호" *문자열이 포함된* 모든 헤더값 검색 """.format(id = ims_app_id, ims_app_id = "ims_app_id")


           # │
           # ├ (준비중) `data dict` : 컬럼명 동의어 사전 설명
           # │              ├ (준비중) `data dict user_table` : 회원 테이블의 컬럼명 동의어 사전
           # │              ├ (준비중) `data dict order_table` : 주문 테이블의 컬럼명 동의어 사전
           # │              └ (준비중) `data dict product_table` : 상품 테이블의 컬럼명 동의어 사전

 #           ├─ `qa` : QA 기능 관련 명령어 호출
 # │         ├ `qa job_fail` : 어제자 *작업지시서 오류* 현황 파악 (파이프라인코드 40, 112 가 아닌 경우)
 # │         └ (준비중) `qa data_fail` : 어제자 *이빨빠진 데이터가* 있는 업체 파악 (유저, 주문 데이터) 
 # │ 
# =======================| comment for "ims" |============================

def ims_comment(*ims_comment_input):

    if ims_comment_input[0] == "active":
        return "\n`총 {} 개` 의 업체가 iMs를 이용중에 있습니다.\n\n*{}* 개의 업체가 iMs 서비스 활성 날짜가 등록되어 있는 상태입니다.\n```{}```\n*{}* 개의 업체가 iMs 서비스 활성 날짜가 등록되어 있지 않은 상태입니다.\n```{}```\n## 앱 아이디를 통해 *업체 상세 정보* 를 확인할 수 있습니다. 가령, `@Q ims secretlabel` \n## *특정 업체의 워크플로우 전체* 목록 확인을 위해서는 `@Q ims secretlabel workflow`을 입력해 주세요.\n## *특정 업체의 활성 워크플로우* 목록 확인을 위해서는 `@Q ims secretlabel workflow_active`를 입력해 주세요.\n## *특정 업체의 비활성 워크플로우* 목록 확인을 위해서는 `@Q ims secretlabel workflow_inactive`를 입력해 주세요.".format(ims_comment_input[5],ims_comment_input[1],ims_comment_input[2],ims_comment_input[3],ims_comment_input[4])
        pass

    elif ims_comment_input[0] == "inactive":
        return "\n총 *{}* 개 업체의 iMs 서비스가 중지 상태입니다.\n```{}```\niMs를 이용중인 업체의 앱 아이디는 `ims active`를 통해 확인 가능합니다.".format(ims_comment_input[1],ims_comment_input[2])
        pass

    elif ims_comment_input[0] == "ims_app_detail":
        return "\n*{company_name_ko}* ({ims_app_id}) 의 정보를 전달드립니다.\n\n*업체 상세 정보*\n```ims_domain_idx : {domain_idx}\ndomain_id : {domain_id}\n\n호스팅사 : {hosting}\n이용 서비스 : {product_type}\n서비스 상태 : {status}\n세팅 완료 여부 : {setting_status}\n서비스 액티브 날짜 : {service_active_date}\n서비스 종료날짜 : {service_end_date}```\n".format(ims_app_id = ims_comment_input[1], hosting = ims_comment_input[2], status = ims_comment_input[3], domain_idx = ims_comment_input[4], domain_id = ims_comment_input[5],service_active_date = ims_comment_input[6],service_end_date = ims_comment_input[7],setting_status = ims_comment_input[10],product_type = ims_comment_input[11], company_name_ko = ims_comment_input[12])
        pass

    elif ims_comment_input[0] == "ims_app_detail_command":
        return "*{ims_app_id}의 워크플로우 상세 정보* 확인을 위해서 \n## 활성화된 워크플로우 : `@Q ims {ims_app_id} workflow_active`,\n## 비활성화된 워크플로우 : `@Q ims {ims_app_id} workflow_inactive`\n## 활성 비활성 워크플로우 전체 : `@Q ims {ims_app_id} workflow`를 입력해 주세요.".format(ims_app_id = ims_comment_input[1])
# -----

    elif ims_comment_input[0] == "workflow_active":
        return "\n총 *{}* 개의 업체가 워크플로우를 이용중에 있습니다. (\"uneedcommscom\" 포함)\n```{}```\n## 현재 *워크플로우를 이용하지 않는 전 업체* 확인을 위해서는 `@Q ims workflow_inactive`를 입력해 주세요.\n## 업체별 *워크플로우 전체 정보를* 확인하기 위해서는 `@Q ims secretlabel workflow` 를 입력해 주세요.\n## 특정 업체의 *활성 워크플로우* 목록 확인을 위해서는 `@Q ims secretlabel workflow_active`를 입력해 주세요.".format(ims_comment_input[1],ims_comment_input[2])
        pass

    elif ims_comment_input[0] == "workflow_inactive":
        return "\n총 *{}* 개의 업체가 워크플로우를 이용하지 않고 있습니다.\n```{}```\n## 현재 *워크플로우를 이용하고 있는 전 업체* 확인을 위해서는 `@Q ims workflow_active`를 입력해 주세요.\n## 업체별 *워크플로우 전체 정보를* 확인하기 위해서는 `@Q ims secretlabel workflow` 를 입력해 주세요.\n## 특정 업체의 *활성 워크플로우* 목록 확인을 위해서는 `@Q ims secretlabel workflow_active`를 입력해 주세요.".format(ims_comment_input[1],ims_comment_input[2])
        pass

# -----

    elif ims_comment_input[0] == "workflow_app_whole":
        return "\n*{ims_app_id}* 계정에 총 `{count_}`개의 워크플로우가 셋팅되어 있습니다. (Active / Inactive 모두포함)\n## Active된 워크플로우 확인을 위해서는 `@Q ims {ims_app_id} workflow_active` 를 입력해 주세요.\n## Inactive된 워크플로우 확인을 위해서는 `@Q ims {ims_app_id} workflow_inactive` 를 입력해 주세요\n## 워크플로우의 샌더 컨텐츠 확인을 위해서는 `@Q ims {ims_app_id} workflow @@@ sender` 를 입력해 주세요.(@@@ = workflow_idx)\n## 특정 워크플로우의 지난 8일간 메시지 발송량을 확인하기 위해서 `@Q ims {ims_app_id} workflow @@@ sent` 를 입력해 주세요. (@@@ = workflow_idx)\n".format(ims_app_id = ims_comment_input[1], count_ = ims_comment_input[2])
        pass

    elif ims_comment_input[0] == "workflow_app_active":
        return "\n*{ims_app_id}* 계정에 총 `{count_}`개의 워크플로우가 Active 상태입니다.\n## 워크플로우의 샌더 컨텐츠 확인을 위해서는 `@Q ims {ims_app_id} workflow @@@ sender` 를 입력해 주세요.(@@@ = workflow_idx)\n## 특정 워크플로우의 지난 8일간 메시지 발송량을 확인하기 위해서 `@Q ims {ims_app_id} workflow @@@ sent` 를 입력해 주세요. (@@@ = workflow_idx)\n".format(ims_app_id = ims_comment_input[1] ,count_ = ims_comment_input[2])
        pass

    elif ims_comment_input[0] == "workflow_app_inactive":
        return "\n*{}* 계정에 총 `{}`개의 워크플로우가 Inactive 상태입니다.\n".format(ims_comment_input[1],ims_comment_input[2])
        pass

# -----

    elif ims_comment_input[0] == "invalid_app_id":
        return "입력하신 *\"{}\"* 는 존재하지 않는 ims_app_id 입니다. 확인 후 다시 입력 바랍니다. \n## iMs를 이용중인 고객의 ims_app_id는 `@Q ims active` 입력을 통해 확인 가능합니다.".format(ims_comment_input[1])
        pass

    elif ims_comment_input[0] == "hosting":
        return "*iMs계정이 활성* 상태이며, *워크플로우 결제완료* 상태인 업체의 쇼핑몰 호스팅 정보입니다.\n\n*카페24* 를 이용중인 고객사는 총 `{}`개 업체입니다.\n```{}```\n*메이크샵* 을 이용중인 고객사는 총 `{}`개 업체입니다.\n```{}```\n*고도몰* 을 이용중인 고객사는 총 `{}` 개 업체입니다.\n```{}```\n그 외는 총 `{}` 개 업체입니다.\n```{}```\n## 앱 아이디를 통해 *업체 상세 정보* 를 확인할 수 있습니다. 가령, `@Q ims secretlabel`\n## *특정 업체의 워크플로우 전체* 목록 확인을 위해서는 `@Q ims secretlabel workflow`을 입력해 주세요.".format(ims_comment_input[2],ims_comment_input[1],ims_comment_input[4],ims_comment_input[3],ims_comment_input[6],ims_comment_input[5],ims_comment_input[8],ims_comment_input[7])

def workflow_comment():
    wf_comment = """워크플로우 운영 관련 명령어 입니다.

## *워크플로우가 가동되고 있는* 전 업체의 확인은 `@Q ims workflow_active`
## *워크플로우가 가동되지 않는* iMs 전 업체의 확인은 `@Q ims workflow_inactive`
## 특정 *업체의 워크플로우* 목록 전체 확인은 `@Q ims uneedcomms workflow`
## 특정 *업체의 활성화 워크플로우* 상세 확인은 `@Q ims uneedcomms workflow_active`
## 특정 *업체의 비활성화 워크플로우* 상세 확인은 `@Q ims uneedcomms workflow_inactive`
"""
    return wf_comment
    pass


def qa_comment(*qa_comment_input):
    if qa_comment_input[0] == "job_failure":
        return "{} || `{} || {} || {} || {}({})` \n".format(qa_comment_input[1],qa_comment_input[2],qa_comment_input[3],qa_comment_input[4],qa_comment_input[5],qa_comment_input[6],qa_comment_input[7])
    elif qa_comment_input[0] == "data_fail":
        return "준비중입니다."


def data_comment(*data_comment_input):
    if data_comment_input[0] == "goal":
        return "현재 iMs 관련 데이터 분석은 크게 3가지의 목적을 갖습니다.(추후 변경될 가능성이 있습니다)\n```1. 비회원 -> 회원 전환 목적\n2. 회원 재방문 목적\n3. 회원 재구매 목적```\n# *회원전환 목적* 에 관련된 질문은 `@Q data convert user`에 모여있어요!\n# *재방문 목적* 에 관련된 질문은 `@Q data convert visit`에 모여있지요!\n# *재구매 목적* 에 관련된 질문은 `@Q data convert buy`에 모여있어요!"
        pass
    elif data_comment_input[0] == "model":
        return "데이터 모델입니다 ^.^\n\nhttp://uneedcomms.cdn.smart-img.com/marketing/ims/iMs_model.png"
        pass
    elif data_comment_input[0][1] == "convert":
        if data_comment_input[0][2] == "user":
            return "*비회원 -> 회원 전환 목적*\n### 지난 n일간 *전체 유입 대비 비회원 유입* 분석 은 `@Q data ### nonmember inflow -nday`를 입력해주세요.(### = ims_app_id)\n### 지난 n일간 *비회원 유입 소스* 분석은 `@Q data ### nonmember inflow_channel -nday`를 입력해 주세요.(### = ims_app_id)\n### 지난 n일간 *비회원 구매 비중* 분석은 `@Q data ### nonmember purchase -nday` (### = ims_app_id)\n# *데이터 분석의 목적* 확인을 위해서는 `@Q data goal`을 입력해주세요."
            pass
        elif data_comment_input[0][2] == "visit":
            return "data convert visit은 준비중입니다."
            pass
        elif data_comment_input[0][2] == "buy":
            return "data convert buy는 준비중입니다."
            pass
    elif data_comment_input[0] == "dict":
        return "데이터 *컬럼명 동의어 사전* 에 대한 설명입니다.\n*같은 의미* 를 갖는 여러 컬럼명을 모아서 *하나의 이름* 으로 규칙을 정해 사전을 만드는 곳입니다.\n\n회원 테이블의 컬럼명 동의어 사전확인을 위해서는 `@Q data dict user_table`을 입력해 주세요.\n주문 테이블의 컬럼명 동의어 사전확인을 위해서는 `@Q data dict order_table`을 입력해 주세요.\n상품 테이블의 컬럼명 동의어 사전확인을 위해서는 `@Q data dict product_table`을 입력해 주세요."
        pass
    elif data_comment_input[0] == "user_table_dict":
        return "회원 테이블에 정의된 컬럼의 종류는 총 n 개 입니다.\n```회원아이디(user_id)\n회원가입일(user_join_date)\nSMS수신여부(sms_agree)\n이메일수신여부(email_agree)\n회원등급(user_grade)```\n"






