# Generated by Django 2.1.5 on 2019-03-12 10:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0022_auto_20190312_1024'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dealobject',
            name='deal_renew_period',
            field=models.CharField(choices=[('0', '0'), ('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'), ('6', '6'), ('7', '7'), ('8', '8'), ('9', '9'), ('10', '10'), ('11', '11'), ('12', '12')], default=0, max_length=13, verbose_name='정기결제 기간(개월수)'),
        ),
    ]
