# ============================| 모듈 리스트 설명 |=================================
# 1. configurations :: 각종 민감 정보 [DB 접속 정보, 슬랙 웹훅 정보, 허브스팟 API 정보] 
# 2. comments :: 슬랙에서 보여지는 모든 문구들 관장하는 모델
# 3. len_1 :: 입력 요소 값이 1개에 해당하는 모든 경우를 관장하는 모델
# 4. ims :: 입력 요소 값이 2개 이상이며, iMs 서비스 운영과 관련된 정보열람 모델
# 5. data :: 입력 요소 값이 2개 이상이며, iMs 서비스와 관련된 데이터 열람 모델 
# 6. qa :: 입력 요소 값이 2개 이상이며, iMs의 Qaulity Assurance 체크 모델 

import sys
from pkg.configurations import *
from pkg.comments import *
from pkg.len_1 import *
from pkg.data import *
from pkg.hey import *
from pkg.ims import *
from pkg.qa import *

#from pkg.command import *


# @@ MVC => CONTROLLER! 최초 데이터 요청시 클래스로 처리!! 
#               -> 매 요청시 인스턴스 생성하여 독립적으로 처리

def ims_init(msg_to,*args):
	if len(args[0]) == 1:
		len_1_msg_to = "<@"+msg_to+"> 님께서 요청하신 결과입니다!\n"
		len_1_result = str(len_1(*args))
		slack_msg(len_1_msg_to+len_1_result)
		pass

	elif args[0][0] == "ims":
		ims_msg_to = "<@"+msg_to+"> 님께서 요청하신 결과입니다!\n"
		ims_result = str(ims_f(*args))
		slack_msg(ims_msg_to+ims_result)
		pass

	elif args[0][0] == "qa":
		qa_msg_to = "<@"+msg_to+"> 님께서 요청하신 결과입니다!\n"
		qa_result = str(qa_f(*args))
		# slack_msg(qa_msg_to+qa_result)
		pass

	elif args[0][0] == "data":
		data_msg_to = "<@"+msg_to+"> 님께서 요청하신 결과입니다!\n"
		data_result = str(data_f(*args))
		slack_msg(data_msg_to+data_result)
		pass

	elif args[0][0] == "command":
		data_msg_to = "<@"+msg_to+"> 님께서 요청하신 결과입니다!\n"
		data_result = str(command_f(*args))
		slack_msg(data_msg_to+data_result)
		pass

	elif args[0][0] == "please":
		data_msg_to = "<@"+msg_to+"> 님께서 요청하신 결과입니다!\n"
		data_result = hey_f(*args)
		pass

	elif len(args) == None:
		data_msg_to = "<@"+msg_to+"> 님께서 요청하신 결과입니다!\n"
		none_result = "아무런 값을 입력하지 않으셨습니다. 저에게 `hi`라고 물어보는건 어떠신가요?"
		slack_msg(none_result)
		pass

	else:
		ims_msg_to = "<@"+msg_to+"> 님께서 요청하신 결과입니다!\n"
		not_get_comm = not_get_comment(9999,*args)
		slack_msg(ims_msg_to+not_get_comm)
		pass

class chat_controller():
	def __init__(self):
		pass

	def chat_init(self, user, message):
		ims_init(user,message)
		pass

def slack_user_text(user, text):
    controller = chat_controller()
    controller.chat_init(user, text)
#init = chat_controller()
#init.ims_init(sys.argv[1], sys.argv[2])

user = sys.argv[1]
text = sys.argv[2:]
slack_user_text(user, text)

# if __name__ == '__main__':

#     while True:
#         input_val = list(input('값을 입력해주세요 : ').lower().split())
#         ims_qa_init(input_val)

