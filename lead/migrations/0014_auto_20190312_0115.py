# Generated by Django 2.1.5 on 2019-03-12 01:15

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lead', '0013_remove_post_post_slug'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='leadform',
            options={'ordering': ['-id'], 'verbose_name': 'LeadForm', 'verbose_name_plural': 'LeadForms'},
        ),
    ]
