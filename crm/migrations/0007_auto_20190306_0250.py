# Generated by Django 2.1.5 on 2019-03-06 02:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0006_auto_20190305_1325'),
    ]

    operations = [
        migrations.AddField(
            model_name='eventlog',
            name='event_hierarchy',
            field=models.CharField(choices=[('company', 'company'), ('contact', 'contact'), ('deal', 'deal')], default='contact', max_length=10),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='eventlog',
            name='related_company',
            field=models.ForeignKey(default=2350, on_delete=django.db.models.deletion.CASCADE, to='crm.CompanyObject', verbose_name='관련 company'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='eventlog',
            name='related_contact',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='crm.ContactObject', verbose_name='관련 Contact'),
        ),
    ]
