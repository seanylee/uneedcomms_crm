# Generated by Django 2.1.5 on 2019-03-19 15:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0009_auto_20190319_1518'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ticketobject',
            name='ticket_number',
            field=models.CharField(default='ldHN8bwkSSYPr2gKAhp9', max_length=22, verbose_name='티켓번호'),
        ),
    ]
