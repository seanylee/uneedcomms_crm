# This is column_name_matching_table


# USER_TABLE! :: 현재 메이크샵, 카페24만 지원
user_all = {

	"user_id" : [
					['fd_13949380', '아이디', 'cf'],
					['fd_13949380', '아이디', 'mk']
				],

	"join_date" : [
					['fd_69752900', '회원 가입일', 'cf'],
					['fd_27208238', '가입일', 'mk']
				],

	"최종접속(로그인)일" : [
					['fd_99447900', '최종접속일','cf'],
					['fd_92702844', '최근로그인','mk']
				],

	"SMS수신여부" : [
					['fd_29359602', 'SMS 수신여부', 'cf'],
					['fd_81352308', 'SMS수신','mk']
				],

	"이메일수신여부" : [ 
					['fd_3013597', '이메일 수신여부', 'cf'],
					['fd_49387840', '메일수신', 'mk']
				],

	"회원그룹및등급" : [
					['fd_48136569', '회원등급', 'cf'],
					['fd_22072679', '회원그룹', 'mk']
				]
	}





# ORDER_TABLE :: 현재 메이크샵, 카페24만 지원
order_logical_dict = {

	"주문번호" : [
					['fd_80895864','주문번호','mk'],
					['fd_80895864','주문번호','cf']
				],


	"주문자 회원등급" : [
					['fd_10816143','주문자 그룹','mk'], 
					['fd_48136569','회원등급','cf']
				],


	"회원 아이디" : [
					['fd_50953545','주문자 ID','mk'], 
					['fd_23332694','주문자ID','cf']
				],


	"수령인 주소" : [
					['fd_89457940','수령인 주소1','mk'], 
					['fd_41043520','수령인 주소','cf']
				],


	"결제일시(입금확인일)" : [
					['fd_82144906','입금확인일(결제일)','mk'],
					['fd_69768358','결제일시(입금확인일)','cf']
				],


	"주문및결제상태" : [
					['fd_30293643','결제상태','mk'],
					['fd_69472518','주문상태정보','cf']
				],


	"주문자 가입일" : [
					['fd_21718919','회원가입일','mk'],
					['fd_42196037','주문자 가입일','cf']
				],


	"유입상세" : [
					['fd_53624396','연계주문',['mk']],
					['fd_97568251','매출경로',['cf']]
				],


	"유입종류(PC/Mobile)" : [
					['fd_17981404','유입경로',['mk']],
					['fd_31811549','주문경로 (PC/모바일)',['cf']]
				],


	"주문자 이메일" : [
					['fd_23612668','주문자 Email','mk'],
					['fd_14911687','주문자 이메일','cf']
				],


	"총 결제금액" : [
					['fd_55645747','결제금액(주문전체)','mk'],
					['fd_58034203','총 결제금액(KRW)','cf']
				],


	"결제수단" : [
					['fd_1269532','결제방법','mk'],
					['fd_18629349','결제수단','cf']
				]		

}

order_pay_date_d_1 = {
	'fd_80895864' : ['주문번호','주문번호',['mk','cf']],	

	'fd_10816143' : ['주문자 회원등급','주문자 그룹',['mk']], 
	'fd_48136569' : ['주문자 회원등급','회원등급',['cf']],

	'fd_50953545' : ['회원 아이디','주문자 ID',['mk']],
	'fd_23332694' : ['회원 아이디','주문자ID',['cf']],

	'fd_89457940' : ['수령인 주소', '수령인 주소1',['mk']],
	'fd_41043520' : ['수령인 주소', '수령인 주소',['cf']],

	'fd_82144906' : ['결제일시(입금확인일)','입금확인일(결제일)',['mk']],
	'fd_69768358' : ['결제일시(입금확인일)','결제일시(입금확인일)',['cf']],

	'fd_30293643' : ['주문및결제상태','결제상태',['mk']],
	'fd_69472518' : ['주문및결제상태','주문상태정보',['cf']],

	'fd_21718919' : ['주문자 가입일','회원가입일',['mk']],
	'fd_42196037' : ['주문자 가입일','주문자 가입일',['cf']],

	'fd_53624396' : ['유입상세','연계주문',['mk']],
	'fd_97568251' : ['유입상세','매출경로',['cf']],

	'fd_17981404' : ['유입종류(PC/Mobile)','유입경로',['mk']],
	'fd_31811549' : ['유입종류(PC/Mobile)','주문경로 (PC/모바일)',['cf']],

	'fd_23612668' : ['주문자 이메일','주문자 Email',['mk']],
	'fd_14911687' : ['주문자 이메일','주문자 이메일',['cf']],

	'fd_55645747' : ['총 결제금액','결제금액(주문전체)',['mk']],
	'fd_58034203' : ['총 결제금액','총 결제금액(KRW)',['cf']],

	'fd_1269532' : ['결제수단','결제방법',['mk']],
	'fd_18629349' : ['결제수단','결제수단',['cf']]
	
}

	
	

