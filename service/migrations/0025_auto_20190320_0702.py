# Generated by Django 2.1.5 on 2019-03-20 07:02

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0024_auto_20190320_0603'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ticketobject',
            name='ticket_owner',
            field=models.ForeignKey(help_text='티켓 담당자', on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL, verbose_name='담당자'),
        ),
    ]
