from django import forms
from .models import Lead

class LeadForm(forms.ModelForm):
    lead_company_corporation		= forms.CharField(label='', widget=forms.TextInput(attrs={"placeholder":"법인명"}))
    lead_company_name				= forms.CharField(label='', widget=forms.TextInput(attrs={"placeholder":"업체명"}))
    lead_company_website_url		= forms.CharField(label='', widget=forms.TextInput(attrs={"placeholder":"웹사이트"}))
    lead_contact_email				= forms.CharField(label='', widget=forms.TextInput(attrs={"placeholder":"이메일"}))
    lead_contact_department			= forms.CharField(label='', widget=forms.TextInput(attrs={"placeholder":"부서명"}))
    lead_contact_name				= forms.CharField(label='', widget=forms.TextInput(attrs={"placeholder":"이름"}))
    lead_contact_phone				= forms.CharField(label='', widget=forms.TextInput(attrs={"placeholder":"휴대전화"}))
    lead_contact_title				= forms.CharField(label='', widget=forms.TextInput(attrs={"placeholder":"직책"}))
    lead_event_inquiry				= forms.CharField(label='', widget=forms.Textarea(attrs={"placeholder":"상세 문의"}))
    lead_event_interests			= forms.CharField(label='', widget=forms.TextInput(attrs={"placeholder":"관심 서비스"}))
    lead_marketing_agree			= forms.BooleanField(widget=forms.CheckboxInput, label='제품, 서비스 및 이벤트 관련 마케팅 정보를 수신하고 싶습니다.')
    lead_demo_request				= forms.BooleanField(widget=forms.CheckboxInput, label='데모를 요청합니다.')

    class Meta:
        model = Lead
        fields = ('lead_company_corporation','lead_company_name','lead_company_website_url',
                  'lead_contact_department','lead_contact_email','lead_contact_name',
                  'lead_contact_phone','lead_contact_title','lead_event_inquiry','lead_event_interests',
				  'lead_marketing_agree','lead_demo_request',
				  )

