from django.apps import AppConfig


class KeepgrowConfig(AppConfig):
    name = 'keepgrow'
