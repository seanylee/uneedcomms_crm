# Generated by Django 2.1.5 on 2019-02-28 02:21

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0004_remove_contactobject_mannual_validation'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='companyobject',
            name='mannual_validation',
        ),
    ]
