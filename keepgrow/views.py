from django.shortcuts import render

def coming_soon_render(request):
    return render(request, 'keepgrow/index.html')
