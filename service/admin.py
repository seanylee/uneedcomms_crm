from django.contrib import admin
from .models import TicketObject, TicketUserDefine 

class TicketSuccess(TicketObject):
    class Meta:
        proxy = True

class TicketFail(TicketObject):
    class Meta:
        proxy = True

@admin.register(TicketObject)
class TicketObjectAdmin(admin.ModelAdmin):
    class Meta:
        model = TicketObject
  
    raw_id_fields       = ('ticket_contact',)
    list_display		= [c.name for c in TicketObject._meta.fields]


@admin.register(TicketUserDefine)
class TicketUserDefineAdmin(admin.ModelAdmin):
    class Meta:
        model = TicketUserDefine

