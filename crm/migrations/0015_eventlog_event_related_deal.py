# Generated by Django 2.1.5 on 2019-03-07 02:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sales', '0018_auto_20190305_0859'),
        ('crm', '0014_auto_20190306_0959'),
    ]

    operations = [
        migrations.AddField(
            model_name='eventlog',
            name='event_related_deal',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='sales.DealObject', verbose_name='관련 Deal'),
        ),
    ]
