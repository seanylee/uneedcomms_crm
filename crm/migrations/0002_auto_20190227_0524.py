# Generated by Django 2.1.5 on 2019-02-27 05:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contactobject',
            name='mobile',
            field=models.CharField(blank=True, help_text='휴대전화 번호 입력("-" 미포함)', max_length=50, null=True, unique=True, verbose_name='휴대전화'),
        ),
    ]
