from urllib.request import urlopen
from urllib.error import HTTPError
from bs4 import BeautifulSoup
import requests
import time

kakaoplusdict = {
	'gaenso':'gaenso',
	'danilovecokr':'danilove',
	'drjstorycom':'닥터제이스토리',
	'dorosiwacom':'도로시와',
	'diguecom':'디그스타일',
	'roompacker':'룸페커',
	'makmaks':'마크막스',
	'miamasvin':'miamasvin',
	'byslicom':'바이슬림',
	'mpaknamaecom':'pnmshop',
	'sappuncokr':'사뿐',
	'soim':'소임',
	'stylemankr':'스타일맨',
	'secretlabel':'secretlabel',
	'chicfox':'시크폭스',
	'chicheracokr':'시크헤라',
	'ssomuchcom':'쏘머치',
	'anaiscokr':'anais',
	'annpionacokr':'앤피오나',
	'sistershu':'sistershu',
	'awab':'에이와비',
	'odecokr':'오드',
	'wingblingcokr':'윙블링',
	'crushjcom':'크러시제이',
	'f4xtylecom':'폭스타일',
	'pinksisly':'핑크시슬리',
	'hotpingcokr':'핫핑',
	'hellosweetycokr':'hellosweety',
	'yuricacokr':'yurica'
}


def getUsers(kakao_id):
	"""
	Describe : Get a number of members from Kakao Plus Friends
	Input : (str)Kakao Plus Friend id (aaaa)
	Output : (str)result number as a form of string
	"""
	try:
		url_build = "https://goto.kakao.com/@{}".format(kakao_id)
		r = requests.get(url_build)
	except HTTPError as e:
		return None
	try:
		soup = BeautifulSoup(r.content, 'html.parser')
		member_cnt = soup.find('span', class_ = "num_count")
		member_cnt = member_cnt.text
		final_agg = "{member_num}".format(member_num = member_cnt)
		return final_agg
	except AttributeError as e:
		return None

	
	
#each = kakaoplusdict['sappuncokr']
#kakao = getUsers(each)
#print(kakao)
