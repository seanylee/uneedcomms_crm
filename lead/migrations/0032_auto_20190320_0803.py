# Generated by Django 2.1.5 on 2019-03-20 08:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lead', '0031_auto_20190320_0800'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scripting',
            name='script_body',
            field=models.TextField(help_text="스크립트 입력 => 반드시 스크립트 태그'<script>'를 입력하십시오.", verbose_name='스크립트'),
        ),
    ]
