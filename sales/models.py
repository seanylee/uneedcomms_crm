#-*-coding: utf-8
from django.conf import settings
from django.db import models
from django.utils import timezone
from django.core.validators import MaxValueValidator, MinValueValidator
from crm.models import ContactObject, CompanyObject
from tinymce.models import HTMLField
from decimal import Decimal

from django.db.models.signals import post_save
from main.conf import slack_msg

class DealObject(models.Model):
    DEAL_STAGES = (
					('meeting','미팅'),
					('communication','커뮤니케이션'),
					('waitforpayment','결제대기'),
					)
    DEAL_STATUS = (
					('ongoing','결제 단계'),
					('success','결제 완료'),
					('fail','결제 실패')
					)
    DEAL_FAIL	= (
					('cost','비용 부담'), 
					('competitor','경쟁사 이전'), 
					('service_quality', '서비스 불만족'),
					('internal_management', '경영악화'),
					)
    CURRENCY_CHOICES = (
						('KRW','KRW(￦)'),
						('JPY','JPY(¥)'),
						('USD','USD($)'),
					)
    DEAL_TYPE	= (
					('new_deal','신규 결제'),
					('renew_deal_1', '재결제(비정기)'),
					('renew_deal_n', '재결제(정기)'),
				  )
    class Meta:
        verbose_name			= 'Deal'
        verbose_name_plural		= 'Deals'
        ordering				= ['-id']

    deal_pipeline = models.ForeignKey('DealMeta', on_delete=models.SET_NULL,
	    null=True, blank=False, verbose_name='상품', help_text='판매중인 서비스 선택',)

    deal_stage = models.CharField(choices=DEAL_STAGES, max_length=15, verbose_name='거래 단계', default='meeting',)

    amount = models.DecimalField('서비스 금액', max_digits=12, decimal_places=2,)
    commission_rate = models.IntegerField('수수료율(%)', default=0, null=False, blank=False, 
			help_text='정수를 입력해 주십시오. 수수료가 없는 상품의 경우 0 을 유지하십시오.')

    commission_amount = models.DecimalField('수수료', max_digits=12, decimal_places=2, 
			help_text='수수료율이 반영된 수수료 금액입니다.')

    before_tax_amount = models.DecimalField('부가세 이전 금액', max_digits=12, decimal_places=2, 
			help_text='부가세 이전 금액입니다. (서비스 금액 + 수수료)')

    after_tax_amount = models.DecimalField('부가세 포함 최종 금액', max_digits=12, decimal_places=2, 
			help_text='부가세 포함 금액입니다. (부가세 이전 금액 + 부가세 10%)')

    currency = models.CharField(choices=CURRENCY_CHOICES, max_length=10, verbose_name='통화', default='KRW(￦)',)
   
    deal_company = models.ForeignKey(CompanyObject, null=True, blank=False, on_delete=models.SET_NULL, verbose_name='업체명',)

    deal_customer = models.ForeignKey(ContactObject, null=True, blank=False, on_delete=models.SET_NULL, verbose_name='구매자',)

    deal_owner = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=False, on_delete=models.SET_NULL,
        verbose_name='내부 담당자', help_text='내부 담당자 선택',)

    created_at = models.DateField('최초 생성일', null=True, blank=True, auto_now_add=True,)

    updated_at = models.DateField('최근 수정일', null=True, blank=True, auto_now=True,)

    deal_type = models.CharField('딜 종류', choices=DEAL_TYPE, null=False, blank=False, default='new_deal', max_length=30)

    deal_renew_period = models.CharField('정기결제 기간(개월)', choices=[(str(x), str(x)) for x in range(0, 13)], default=0, 
			help_text='신규 및 비정기 결제의 경우 0을 유지하십시오.(스마트이미지 제외)', max_length=13)

    deal_status = models.CharField('결제상태', choices=DEAL_STATUS, null=False, blank=False, max_length=30, default='ongoing')

    fail_type = models.CharField('실패사유', choices=DEAL_FAIL, max_length=30,help_text='거래 실패사유 분류')

    fail_detail = models.TextField('실패 내용',)

    failed_at = models.DateField('실패일', null=True, blank=True)

    successed_at = models.DateField('결제 완료일', null=True, blank=True)
#-------- Fields from Hubspot ----------
#    hub_deal_id = models.IntegerField('hub_deal_id', null=True, blank=True,)
#    hub_deal_type = models.CharField('hub_deal_type', null=True, blank=True, max_length=100)
#    hub_associated_company_id = models.IntegerField('hub_assocated_company_id', null=True, blank=True,)
#    hub_associated_comapny = models.CharField('hub_associated_company', null=True, blank=True, max_length=200,)
#    hub_associated_contact_ids = models.CharField('hub_associated_contact_ids', null=True, blank=True, max_length=200,)
#    hub_associated_contacts = models.CharField('hub_associated_contacts', null=True, blank=True, max_length=200,)

    def __str__(self):
        return f'[{self.deal_pipeline}] {self.deal_customer}'

    def save(self, *args, **kwargs):
        if (self.amount) and (self.commission_rate != 0):
            self.commission_amount = Decimal(self.amount) * Decimal(self.commission_rate/100)
            self.before_tax_amount = self.amount + self.commission_amount
            self.after_tax_amount  = self.before_tax_amount + (self.before_tax_amount * Decimal('0.1'))
        elif (self.amount) and (self.commission_rate == 0):
            self.commission_amount = 0
            self.before_tax_amount = self.amount + self.commission_amount
            self.after_tax_amount = self.before_tax_amount + (self.before_tax_amount * Decimal('0.1'))
        super(DealObject, self).save(*args, **kwargs)

#    @staticmethod
#    def autocomplete_search_fields():
#        return 'id','deal_customer__id'
def save_dealobject(sender, instance, **kwargs):
#slack_msg(sender) # <class 'sales.models.DealObject'>
#slack_msg('------')
    slack_msg('거래' + str(instance) + '이/가 생성되었습니다.')
post_save.connect(save_dealobject, sender=DealObject)

class DealMeta(models.Model):

    order = models.PositiveIntegerField('order', default=0, blank=False, null=False)

    domestic_service_name = models.CharField('국내 서비스명', null=False, blank=False, max_length=100,
		help_text='국내 서비스명 입력 (예시 - "스마트스킨")',)

    international_service_name = models.CharField('해외 서비스명', null=False, blank=False, max_length=100,
        help_text='해외 서비스명 입력 (예시 - "smartskin")')

    service_version = models.DecimalField('버전', null=False, blank=False, max_digits=4, decimal_places=1, default = 0.1,
        help_text='서비스 버전 입력 (기본값: 0.1, 소수점 1자리까지 허용)',)
	
    service_code = models.CharField('식별코드', null=False, blank=False, max_length=100,
		help_text='서비스의 내부 식별코드 입력 (예시 - SMS)',)

#    deal_stage = models.ForeignKey('DealStage', on_delete=models.DO_NOTHING, verbose_name='거래 단계',)

    domestic_is_activate = models.BooleanField('상품 활성화(국내)', default=False,
		help_text='판매 가능여부 선택 (가능/불가능)',)

    international_is_activate = models.BooleanField('상품 활성화(해외)', default=False,
        help_text='판매 가능여부 선택 (가능/불가능)',)

    engineer = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=False, on_delete=models.CASCADE,
		verbose_name='기술 담당자', help_text='기술 담당자 선택',)

    released_at = models.DateField('공식 출시일', null=True, blank=True, help_text='공식 출시일 선택',)
    created_at = models.DateField('최초 생성일', null=True, blank=True, auto_now_add=True)
    updated_at = models.DateField('최근 수정일', null=True, blank=True, auto_now=True)

    class Meta(object):
        verbose_name			= 'Deal Meta'
        verbose_name_plural		= 'Deal Metas'
        ordering				= ['order']

    def __str__(self):
        return f'{self.domestic_service_name} ({self.service_code}:{self.service_version})'

class DealStage(models.Model):

    deal_meta = models.ForeignKey(DealMeta, null=True, blank=True, on_delete=models.CASCADE, verbose_name='서비스')

    deal_stage_name = models.CharField('거래 단계명', null=False, blank=False,max_length=200,
		help_text='거래 단계를 정의할 수 있는 정보입력',)

    deal_stage_policy = models.TextField('거래 단계별 정책 정의', help_text='거래 단계에 해당하는 정책사항 입력(옵션)',)

    expected_stage_duration = models.DurationField('거래 단계별 예상 소요 기간',
		help_text='다음 단계로 넘어가기까지 걸리는 기간정의')

    deal_probability = models.IntegerField('거래 진행률(%)', default=1,
        validators=[MaxValueValidator(100), MinValueValidator(1)], help_text='1 - 100 (%) 사이의 거래 진행률 정의')
    
    order = models.PositiveIntegerField('거래 진행 순서', default=0, null=False, blank=False,)

    class Meta(object):
        verbose_name			= 'Deal Stage'
        verbose_name_plural		= 'Deal Stages'
        ordering				= ['order']

    def __str__(self):
        return f'{self.deal_stage_name} ({self.deal_probability}%)'

class ResellerObject(models.Model):
    class Meta:
        verbose_name			= 'Reseller'
        verbose_name_plural		= 'Resellers'
        ordering				= ['-id']

    def __str__(self):
        return None

