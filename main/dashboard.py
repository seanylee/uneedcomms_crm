from django.utils.translation import ugettext_lazy as _
from jet.dashboard import modules
from jet.dashboard.dashboard import Dashboard, AppIndexDashboard, DashboardModule


class CustomIndexDashboard(Dashboard):
    columns = 1

    def init_with_context(self, context):
        self.available_children.append(modules.LinkList)
        self.children.append(modules.LinkList(
            _('Support'),
            children=[
                {
                    'title': _('Django documentations'),
                    'url': 'http://docs.djangoproject.com/',
                    'external': True,
                },
                {
                    'title': _('Django "django-users" mailing lists'),
                    'url': 'http://groups.google.com/group/django-users',
                    'external': True,
                },
                {
                    'title': _('Django irc channels'),
                    'url': 'irc://irc.freenode.net/django',
                    'external': True,
                },
            ],
            column=3,
            order=3
        ))



from plotly import graph_objs
from plotly.offline import plot

class Graph(DashboardModule):
    title = 'Graph'
    template = 'dashboard_modules/modules/graph1.html'

    def init_with_context(self, context):
        x = [-2, 0, 4, 6, 7]
        y = [q ** 2 - q + 3 for q in x]
        trace1 = graph_objs.Scatter(
            x=x,
            y=y,
            marker={
                'color': 'red',
                'symbol': 104,
                'size': "10"
            },
            mode="lines",
            name='1st Trace'
        )
        data = graph_objs.Data([trace1])
        layout = graph_objs.Layout(
            title="Meine Daten",
            xaxis={'title': 'x1'},
            yaxis={'title': 'x2'}
        )
        figure = graph_objs.Figure(data=data, layout=layout)
        div = plot(figure, auto_open=False, output_type='div')

        self.graph = div
#https://gitter.im/geex-arts/django-jet/archives/2017/03/20
