#from tastypie.http import Http
from tastypie.authorization import Authorization
from tastypie.authentication import ApiKeyAuthentication
from tastypie.resources import ModelResource
from tastypie.serializers import Serializer
from tastypie.api import Api
from tastypie import fields
import requests
import json
from .models import Challenges
from django.contrib.auth.models import User
from django.http import HttpResponse

import sys
from subprocess import Popen, call
import subprocess
import os


class ChallengesResource(ModelResource):
    class Meta:
        always_return_data = True
        queryset = Challenges.objects.all()
        resource_name = 'slango'
        authentication = ApiKeyAuthentication()
        authorization = Authorization()
        allowed_method = ['POST','GET']
        serializer = Serializer(formats=['json'])

    def hydrate(self, bundle):
        try:
            user = str(bundle.data['event']['user'])
            dirty_text = str(bundle.data['event']['text']).split()
            text = " ".join(dirty_text[1:])

            # chmod +x run.sh 필수!
            subprocess.Popen(['sh','/home/docker/code/app/slack_message_api/request.sh', 
				user, text])
        except:
            subprocess.Popen(['sh','/home/docker/code/app/slack_message_api/request.sh', 
				'U2U83B2JJ', 'Hydrate_error'])
        finally:
            return bundle

#class UserResource(ModelResource):
#    class Meta:
#        queryset = User.objects.all()
#        resource_name = 'user'
#        authentication = ApiKeyAuthentication()
#        authorization = Authorization()
#        fields = ['email','username','date_joined','id']
#        allowed_methods=['get']
