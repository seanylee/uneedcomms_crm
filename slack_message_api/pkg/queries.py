from .configurations import *
from TCLIService.ttypes import TOperationState
import requests
import time

import sqlite3
from datetime import *
import pytz
from datetime import datetime, time, timedelta
import urllib.request, json 

# =============================| Usage ranking base on history |================================

dummy = '''
def rank_yesterday():
    tz = pytz.timezone("Asia/Seoul")
    yesterday = ((datetime.now(tz) - timedelta(days=1)).strftime("%Y-%m-%d"))
    rank_for_yesterday = list()
    query = """
        SELECT count(*) as 'rank', user 
        FROM slack_message_api_event 
        GROUP BY user
        ORDER BY count(*) DESC 
        LIMIT 10;"""
    a = SQLite3()
    sqlite3 = a.sqlite
    for row in sqlite3.execute(query):
        input_ = str(row[0])
        rank_for_yesterday.append(input_)
    return rank_for_yesterday
'''

# =============================| QA queries for iMs Quality Assurance purpose! |================================
def job_failure():
    sql_task_failure_list = list()
    sql_task_failure = '''
        SELECT
        *
        FROM(
          SELECT
            ims_app_id,
            instance_job_idx,
            job_idx,
            data_table,
            pipeline_stage_name,
            pipeline_stage_number,
            split_part(cast(inst_kr_reg_date as varchar), ' ',1) as instance_reg_date
          FROM mysql.mmc.grabber_instance_job_monitoring_view
          WHERE
          (pipeline_stage_number != 40) AND
          (pipeline_stage_number != 112)
        )
        WHERE DATE(instance_reg_date) = DATE(now())
        ORDER BY pipeline_stage_number ASC
    '''
    a = Presto()
    presto = a.presto_cursor
    presto.execute(sql_task_failure)
    for i in presto.fetchall():
        sql_task_failure_list.append(i)
    return sql_task_failure_list


# =============================| iMs queries for iMs operation purpose! |================================


exception_list = [
    'bi_dummy',
    'default',
    'information_schema',
    'jay_testdb',
    'targetbook',
    'td',
    'tesilio',
    'test1',
    'uneedcomms',
    'supersetds',
    'smartdw2',
    'eriktesthadoop'
    ]
    # 'uneedcommscom',

def Whole_domain():
    schema_id = list()
    sql_id = '''SHOW SCHEMAS FROM hive'''
    a = Presto()
    presto = a.presto_cursor
    presto.execute(sql_id)
    for i in presto.fetchall():
        i = i[0]
        if i not in exception_list:
            schema_id.append(i)
    return schema_id

def Inactive_domain():
    inact_domain = list()
    sql__id = '''
    SELECT ims_app_id FROM mysql.mmc.ims_domains WHERE status LIKE \'inactive%\'
    '''
    a = Presto()
    presto = a.presto_cursor
    presto.execute(sql__id)
    for ii in presto.fetchall():
        ii = ii[0].strip()
        inact_domain.append(ii)
    return inact_domain

def Active_domain():
    ims_id = list()
    schema_id = list()
    inact_domain_ = Inactive_domain()
    sql_id = '''
            SELECT ims_app_id
            FROM mysql.mmc.ims_domains
            WHERE
            (status like 'active%')
            AND
              (
                ims_app_id not like 'tesilio%' AND 
                ims_app_id not like 'bi_dummy%' AND
                ims_app_id not like 'bi_dummy%' AND
                ims_app_id not like 'default%' AND
                ims_app_id not like 'jay_testdb%' AND
                ims_app_id not like 'targetbook%' AND
                ims_app_id not like 'td%' AND
                ims_app_id not like 'test1%' AND
                ims_app_id not like 'uneedcomms' AND
                --ims_app_id not like 'uneedcommscom%' AND
                ims_app_id not like 'supersetds%' AND
                ims_app_id not like 'smartdw2%' AND
                ims_app_id not like 'eriktesthadoop%'
              )
            -- AND
            -- (ims_product_idx IS NOT NULL)
                '''
    a = Presto()
    presto = a.presto_cursor
    presto.execute(sql_id)
    for i in presto.fetchall():
        i = i[0].strip()
        if i not in inact_domain_:
            ims_id.append(i)
    return ims_id


def Active_notpurchase_domain():
    ims_id = list()
    schema_id = list()
    inact_domain_ = Inactive_domain()
    sql_id = '''
            SELECT ims_app_id
            FROM mysql.mmc.ims_domains
            WHERE
            (status like 'active%')
            AND
              (
                ims_app_id not like 'tesilio%' AND 
                ims_app_id not like 'bi_dummy%' AND
                ims_app_id not like 'bi_dummy%' AND
                ims_app_id not like 'default%' AND
                ims_app_id not like 'jay_testdb%' AND
                ims_app_id not like 'targetbook%' AND
                ims_app_id not like 'td%' AND
                ims_app_id not like 'test1%' AND
                ims_app_id not like 'uneedcomms' AND
                --ims_app_id not like 'uneedcommscom%' AND
                ims_app_id not like 'supersetds%' AND
                ims_app_id not like 'smartdw2%' AND
                ims_app_id not like 'eriktesthadoop%'
              )
            AND
            (ims_product_idx IS NULL)
                '''
    a = Presto()
    presto = a.presto_cursor
    presto.execute(sql_id)
    for i in presto.fetchall():
        i = i[0].strip()
        if i not in inact_domain_:
            ims_id.append(i)
    return ims_id



def app_host_matching():
    matching_list = list()
    sql_app_host_matching = '''
    SELECT
    DISTINCT (ims_domains.ims_app_id), s.hosting, s.associatedcompanyid
    FROM mysql.mmc.ims_domains
    INNER JOIN (
    SELECT id,
    CASE
    WHEN shop_hosting = 1 THEN 'mk'
    WHEN shop_hosting = 2 THEN 'cf'
    WHEN shop_hosting = 3 THEN 'gd'
    ELSE 'el'
    END AS "hosting",
    associatedcompanyid
    from mysql.uneedcomms_super_admin.super_domain_info) s
    ON s.id = ims_domains.domain_id
    WHERE
    -- (ims_product_idx is not NULL) and
    (ims_domains.ims_app_id NOT LIKE 'tesilio%') and
    (ims_domains.ims_app_id NOT LIKE 'uneedcomms') and
    (ims_domains.ims_app_id NOT LIKE 'targetbook%') and
    (ims_domains.status like 'active%')
    '''
    a = Presto()
    presto = a.presto_cursor
    presto.execute(sql_app_host_matching)
    for s in presto.fetchall():
        if s == None:
            pass
        else:
            matching_list.append(s)
    return matching_list


def show_ims(ims_app_id):
# 9개 컬럼 반환
    sql_show = '''
    SELECT
        status, ims_domain_idx, ims_domains.domain_id,
        split_part(try_cast(active_date as VARCHAR),' ',1) as active_date,
        split_part(try_cast(end_date as varchar),' ',1) as end_date,
        remaining_months, s.hosting, s.associatedcompanyid,
        if (is_setting_completed = 1, '세팅 완료', '세팅 미완료') as "setting",
        prd.ims_product_name,
        s.shop_name
        FROM mysql.mmc.ims_domains
        INNER JOIN (SELECT id,
                    CASE
                    WHEN shop_hosting = 1 THEN '메이크샵'
                    WHEN shop_hosting = 2 THEN '카페24'
                    WHEN shop_hosting = 3 THEN '고도몰'
                    ELSE '독립몰+기타'
                    END AS "hosting",
                    associatedcompanyid,
                    shop_name
                    FROM mysql.uneedcomms_super_admin.super_domain_info) s
        ON s.id = ims_domains.domain_id
        LEFT JOIN (SELECT ims_product_name,ims_product_idx
                    FROM mysql.mmc.ims_products) prd
        ON prd.ims_product_idx = ims_domains.ims_product_idx
        WHERE ims_app_id LIKE '{}%' 
    '''.format(ims_app_id)
    a = Presto()
    presto = a.presto_cursor
    presto.execute(sql_show)
    for s in presto.fetchall():
        result = s
        return result


def show_hosting(ims_app_id):
    sql_hosting = '''
    SELECT
    DISTINCT (s.hosting) AS hosting
    FROM mysql.mmc.ims_domains
    INNER JOIN (SELECT id, CASE WHEN shop_hosting = 1 THEN 'mk' WHEN shop_hosting = 2 THEN 'cf' WHEN shop_hosting = 3 THEN 'gd' ELSE 'etc' END AS "hosting"
                FROM mysql.uneedcomms_super_admin.super_domain_info 
                WHERE id IS NOT NULL) s
    ON s.id = ims_domains.domain_id
    WHERE
    -- (ims_product_idx is not NULL) AND
    (ims_domains.ims_app_id like '{}%')
    '''.format(ims_app_id)
    a = Presto()
    presto = a.presto_cursor
    presto.execute(sql_hosting)
    hosting = presto.fetchone()[0]
    return hosting

def show_payment(ims_app_id, period):
    ims_payment = list()
    if period == "limit3":
        sql_payment = '''
        SELECT
          from_unixtime(bc_payment_list.paid_at) paid_time, bc_payment_list.success,
          bc_payment_list.status, bc_payment_list.amount, bc_payment_list.pay_method,
          bc_payment_list."name", bc_payment_list.currency
        FROM mysql.uneedcomms_billing_center.bc_payment_list
        WHERE
            ("name" LIKE '%iMs%') AND (status = 'paid') AND (success = 'true') AND 
            bc_payment_list.domain_id IN (SELECT domain_id FROM mysql.mmc.ims_domains WHERE ims_app_id LIKE '{app_id}%')
        ORDER BY from_unixtime(bc_payment_list.paid_at) DESC LIMIT 3
        '''.format(app_id = ims_app_id)

    if period == "monthly":
        sql_payment = '''
        SELECT
            from_unixtime(bc_payment_list.paid_at) paid_time, bc_payment_list.success,
            bc_payment_list.status, bc_payment_list.amount, bc_payment_list.pay_method,
            bc_payment_list."name", bc_payment_list.currency
        FROM mysql.uneedcomms_billing_center.bc_payment_list
        WHERE
            ("name" LIKE '%iMs%') AND (status = 'paid') AND (success = 'true') AND
            bc_payment_list.domain_id IN (SELECT domain_id FROM mysql.mmc.ims_domains WHERE ims_app_id LIKE 'miamasvin%')
            AND (MONTH(TRY_CAST(from_unixtime(bc_payment_list.paid_at) AS DATE)) = MONTH(NOW()))
        ORDER BY from_unixtime(bc_payment_list.paid_at)
        '''.format(app_id = ims_app_id)
    a = Presto()
    presto = a.presto_cursor
    presto.execute(sql_payment)
    for payment in presto.fetchall():
        ims_payment.append(payment)
    return ims_payment

def ims_workflow_active():
    ims_workflow_active_ = list()
    sql_ims_workflow_active = '''
        SELECT
          ims_domains.ims_app_id
        FROM mysql.mmc.ims_workflows
        INNER JOIN mysql.mmc.ims_domains
        ON ims_domains.ims_domain_idx = ims_workflows.ims_domain_idx
        WHERE
          (ims_workflows.status like 'active%') AND
          (ims_domains.ims_app_id not like 'tesilio%')
        GROUP BY ims_app_id
        '''
    a = Presto()
    presto = a.presto_cursor
    presto.execute(sql_ims_workflow_active)
    for im_ in presto.fetchall():
        im_ = im_[0].strip()
        ims_workflow_active_.append(im_)
    return ims_workflow_active_

def ims_workflow_inactive():
    ims_workflow_inactive_ = list()
    sql_ims_workflow_inactive = '''
        SELECT
          trim(ims_domains.ims_app_id)
        FROM mysql.mmc.ims_workflows
        INNER JOIN mysql.mmc.ims_domains
        ON ims_domains.ims_domain_idx = ims_workflows.ims_domain_idx
        WHERE
          (ims_workflows.status like 'inactive%')
        GROUP BY ims_app_id
        '''
    a = Presto()
    presto = a.presto_cursor
    presto.execute(sql_ims_workflow_inactive)
    for im_ in presto.fetchall():
        im_ = im_[0].strip()
        ims_workflow_inactive_.append(im_)
    return ims_workflow_inactive_

def workflow_whole(ims_app_id):
    workflow_comments = list()
    sql_workflow_whole = '''
        SELECT
          ims_workflows.workflow_idx,
          ims_workflows.workflow_name,
          ims_workflows.status
        FROM mysql.mmc.ims_workflows
        INNER JOIN mysql.mmc.ims_domains
        ON ims_domains.ims_domain_idx = ims_workflows.ims_domain_idx
        WHERE
          (ims_domains.ims_app_id like '{}%') AND
          (ims_workflows.status like '%active%')
        '''.format(ims_app_id)
    a = Presto()
    presto = a.presto_cursor
    presto.execute(sql_workflow_whole)
    for s in presto.fetchall():
        workflow_idx = s[0]
        workflow_name = s[1]
        status = s[2].strip()
        workflow_comment = "workflow_idx : {}\n워크플로우 이름 : {}\n워크플로우 상태 : {}\n--------------------\n\n".format(workflow_idx,workflow_name,status)
        workflow_comment = str(workflow_comment)
        workflow_comments.append(workflow_comment)
    return workflow_comments

def workflow_active(ims_app_id):
    workflow_comments = list()
    sql_workflow_active = (
        "SELECT "
          "ims_workflows.workflow_idx, "
          "ims_workflows.workflow_name, "
          "ims_workflows.status "
        "FROM mysql.mmc.ims_workflows "
        "INNER JOIN mysql.mmc.ims_domains "
        "ON ims_domains.ims_domain_idx = ims_workflows.ims_domain_idx "
        "WHERE "
          "(ims_domains.ims_app_id like '{}%') AND "
          "(ims_workflows.status like 'active%') "
        ).format(ims_app_id)
    a = Presto()
    presto = a.presto_cursor
    presto.execute(sql_workflow_active)
    for s in presto.fetchall():
        workflow_idx = s[0]
        workflow_name = s[1]
        status = s[2].strip()
        workflow_comment = "workflow_idx : {}\n워크플로우 이름 : {}\n워크플로우 상태 : {} \n==============\n\n".format(workflow_idx,workflow_name,status)
        workflow_comment = str(workflow_comment)
        workflow_comments.append(workflow_comment)
    return workflow_comments

# def workflow_inactive(ims_app_id):
#     workflow_comments = list()
#     sql_workflow_inactive = '''
#         SELECT
#           ims_workflows.workflow_idx,
#           ims_workflows.workflow_name,
#           ims_workflows.status
#         FROM mysql.mmc.ims_workflows
#         INNER JOIN mysql.mmc.ims_domains
#         ON ims_domains.ims_domain_idx = ims_workflows.ims_domain_idx
#         WHERE
#           (ims_domains.ims_app_id like '{}%') AND
#           (ims_workflows.status like 'inactive%')
#         '''.format(ims_app_id)
#     presto.execute(sql_workflow_inactive)
#     for s in presto.fetchall():
#         workflow_idx = s[0]
#         workflow_name = s[1]
#         status = s[2].strip()
#         workflow_comment = "workflow_idx : {}\n워크플로우 이름 : {}\n워크플로우 상태 : {} \n\n==============\n".format(workflow_idx,workflow_name,status)
#         workflow_comment = str(workflow_comment)
#         workflow_comments.append(workflow_comment)
#     return workflow_comments


def workflow_sender(input_ims_app_id,workflow_idx):
    sender_list = list()
    sql_wf_sender = '''    
    SELECT    
    content_idx, IF(template_type IS NULL,'None',template_type), send_type, workflow_name, content_title,
    CONCAT('https://view-dev.uneedcomms.com/ims/', ims_domain_idx, '?template_json_idx=', dc_template_idx, '&content_idx=', content_idx, '&landing_type=', send_type) AS "url",
    CONCAT('https://view-dev.uneedcomms.com/ims/', ims_domain_idx, '?template_json_idx=61', '&content_idx=', content_idx, '&landing_type=email&test=true') AS "image_url"
    FROM(
        SELECT
            s.content_idx AS content_idx, split_part(dce.title,' ',2) AS template_type,
            CASE WHEN s.touchpoint_code LIKE '%001%' THEN 'sms' WHEN s.touchpoint_code LIKE '%001%' THEN 'sms' WHEN s.touchpoint_code LIKE '%002%' THEN 'email' WHEN s.touchpoint_code LIKE '%003%' THEN 'kakao' WHEN s.touchpoint_code LIKE '%004%' THEN 'app' END AS send_type,
            w.workflow_name AS workflow_name, 
            CAST(s.content_title AS VARCHAR) AS content_title,
            CAST(d.ims_domain_idx AS VARCHAR) AS ims_domain_idx,
            CAST(dce.dc_template_idx AS VARCHAR) AS dc_template_idx
        FROM mysql.mmc.ims_workflows w
        INNER JOIN
                (SELECT ims_domain_idx, ims_app_id
                FROM mysql.mmc.ims_domains) d
                ON d.ims_domain_idx = w.ims_domain_idx
        INNER JOIN
                (SELECT template_idx  AS template_idx,
                    CAST(content_idx AS VARCHAR) AS content_idx, CAST(touchpoint_code AS VARCHAR) AS touchpoint_code,
                    workflow_idx, content_title, dc_contents
                FROM mysql.mmc.ims_sender_contents) s
                ON w.workflow_idx = s.workflow_idx
        INNER JOIN
                (SELECT "title","dc_template_idx" FROM mysql.dc.dc_templates) dce
                ON dce.dc_template_idx = s.template_idx
        WHERE
          -- pinksisly & active 를 변동값으로 받아오기
            (d.ims_app_id like '{ims_app_id}%') AND
            (w.workflow_idx = {wf_idx})
        ORDER BY send_type
    )'''.format(ims_app_id = input_ims_app_id, wf_idx = workflow_idx)
    a = Presto()
    presto = a.presto_cursor
    presto.execute(sql_wf_sender)
    for sender in presto.fetchall():
        sender_list.append(sender)
    return sender_list


# 워크플로우 발송 전체 모니터링!!
def workflow_sent_check(app_id, date_code,check_count=0):
    if date_code == 6:
        date_input = 1
    elif date_code == 0:
        date_input = 2
    elif date_code == 1:
        date_input = 3
    try:
        sent_check_sql = """
        SELECT
          ims_workflows.workflow_idx AS workflow_idx,
          ims_workflows.status AS status,
          ims_workflows.workflow_name AS workflow_name,
          CASE WHEN CAST(sent_summary.sent_cnt AS VARCHAR) IS NULL THEN '0' ELSE CAST(sent_summary.sent_cnt AS VARCHAR) END AS sent_cnt,
          CONCAT('ims ',ims_domain.ims_app_id,' workflow ',CAST(ims_workflows.workflow_idx AS VARCHAR),' sent') AS q_command1
        FROM mysql.mmc.ims_workflows
        INNER JOIN (SELECT ims_domain_idx, TRIM(CAST(ims_app_id AS VARCHAR)) AS "ims_app_id" FROM mysql.mmc.ims_domains) ims_domain
        ON ims_domain.ims_domain_idx = ims_workflows.ims_domain_idx
        LEFT JOIN (
                    SELECT
                        MAX(sdw__permission__ims_app_id) AS "ims_app_id",
                        COUNT(*) AS "sent_cnt",
                        try_cast(utm_campaign as int) as utm_campaign
                    FROM hive.{ims_app_id_}.sender_log
                    WHERE
                        (utm_medium LIKE 'sent%')
                        AND (DATE(datetime_val) = DATE(NOW()) + INTERVAL '-{date_gap}' DAY)
                    GROUP BY utm_campaign
                    ) sent_summary
        ON sent_summary.utm_campaign = ims_workflows.workflow_idx
        WHERE
          (ims_workflows.status like 'active%') AND
          (ims_domain.ims_app_id like '{ims_app_id_}%')""".format(ims_app_id_ = app_id, date_gap = date_input)
        if check_count == 0:
            check = workflow_sent_check_executor(sent_check_sql, 0)
        else:
            check = workflow_sent_check_executor(sent_check_sql, 100)
        return check
    except:
        return None


def workflow_sent_check_executor(query_input, check_count):
    check_list = list()
    try:
        a = Presto()
        presto = a.presto_cursor
        presto.execute(query_input)
        for fetc in presto.fetchall():
            if int(fetc[3]) <= check_count:
                check_list.append(fetc)
                pass
            else:
                pass
        return check_list
    except:
        return None


        # SELECT MAX(sdw__permission__ims_app_id) AS "ims_app_id", COUNT(*) AS "sent_cnt"
        # FROM hive.{ims_app_id_}.sender_log
        # WHERE (utm_medium LIKE 'sent%') AND (DATE(datetime_val) = DATE(NOW()) + INTERVAL '-{date_gap}' DAY)



# 특정 업체 전체 워크플로우의 발송량 및 워크플로우 idx에 따른 발송량
def workflow_sent_cnt(app_id,workflow_idx):
    sent_cnt_agg = list()
    if workflow_idx == 0:
        wf_idx_query_part = ""
    elif workflow_idx != 0:
        wf_idx_query_part = "AND (utm_campaign = '{wf_idx}')".format(wf_idx = workflow_idx) 
    workflow_sent_cnt_sql = """
    WITH
    wf_sent_0 AS (
    SELECT DATE(datetime_val) as "date", wf_sent, wf_view, ROUND((CAST(wf_view AS DOUBLE)/CAST(wf_sent AS DOUBLE) * 100),2) AS open_ratio
    FROM(SELECT COUNT_IF(utm_medium LIKE 'sent%') AS "wf_sent", COUNT_IF(utm_medium LIKE '%view%') AS "wf_view", MIN(datetime_val) as datetime_val FROM hive.{ims_app_id}.sender_log WHERE (DATE(datetime_val) = DATE(NOW())) {query_part})),
    wf_sent_1 AS (
    SELECT DATE(datetime_val) as "date", wf_sent, wf_view, ROUND((CAST(wf_view AS DOUBLE)/CAST(wf_sent AS DOUBLE) * 100),2) AS open_ratio
    FROM(SELECT COUNT_IF(utm_medium LIKE 'sent%') AS "wf_sent", COUNT_IF(utm_medium LIKE '%view%') AS "wf_view", MIN(datetime_val) as datetime_val FROM hive.{ims_app_id}.sender_log WHERE (DATE(datetime_val) = DATE(NOW()) + INTERVAL '-1' DAY) {query_part})),
    wf_sent_2 AS (
    SELECT DATE(datetime_val) as "date", wf_sent, wf_view, ROUND((CAST(wf_view AS DOUBLE)/CAST(wf_sent AS DOUBLE) * 100),2) AS open_ratio
    FROM(SELECT COUNT_IF(utm_medium LIKE 'sent%') AS "wf_sent", COUNT_IF(utm_medium LIKE '%view%') AS "wf_view", MIN(datetime_val) as datetime_val FROM hive.{ims_app_id}.sender_log WHERE (DATE(datetime_val) = DATE(NOW()) + INTERVAL '-2' DAY) {query_part})),
    wf_sent_3 AS (
    SELECT DATE(datetime_val) as "date", wf_sent, wf_view, ROUND((CAST(wf_view AS DOUBLE)/CAST(wf_sent AS DOUBLE) * 100),2) AS open_ratio
    FROM(SELECT COUNT_IF(utm_medium LIKE 'sent%') AS "wf_sent", COUNT_IF(utm_medium LIKE '%view%') AS "wf_view", MIN(datetime_val) as datetime_val FROM hive.{ims_app_id}.sender_log WHERE (DATE(datetime_val) = DATE(NOW()) + INTERVAL '-3' DAY) {query_part})),
    wf_sent_4 AS (
    SELECT DATE(datetime_val) as "date", wf_sent, wf_view, ROUND((CAST(wf_view AS DOUBLE)/CAST(wf_sent AS DOUBLE) * 100),2) AS open_ratio
    FROM(SELECT COUNT_IF(utm_medium LIKE 'sent%') AS "wf_sent", COUNT_IF(utm_medium LIKE '%view%') AS "wf_view", MIN(datetime_val) as datetime_val FROM hive.{ims_app_id}.sender_log WHERE (DATE(datetime_val) = DATE(NOW()) + INTERVAL '-4' DAY) {query_part})),
    wf_sent_5 AS (
    SELECT DATE(datetime_val) as "date", wf_sent, wf_view, ROUND((CAST(wf_view AS DOUBLE)/CAST(wf_sent AS DOUBLE) * 100),2) AS open_ratio
    FROM(SELECT COUNT_IF(utm_medium LIKE 'sent%') AS "wf_sent", COUNT_IF(utm_medium LIKE '%view%') AS "wf_view", MIN(datetime_val) as datetime_val FROM hive.{ims_app_id}.sender_log WHERE (DATE(datetime_val) = DATE(NOW()) + INTERVAL '-5' DAY) {query_part})),
    wf_sent_6 AS (
    SELECT DATE(datetime_val) as "date", wf_sent, wf_view, ROUND((CAST(wf_view AS DOUBLE)/CAST(wf_sent AS DOUBLE) * 100),2) AS open_ratio
    FROM(SELECT COUNT_IF(utm_medium LIKE 'sent%') AS "wf_sent", COUNT_IF(utm_medium LIKE '%view%') AS "wf_view", MIN(datetime_val) as datetime_val FROM hive.{ims_app_id}.sender_log WHERE (DATE(datetime_val) = DATE(NOW()) + INTERVAL '-6' DAY) {query_part})),
    wf_sent_7 AS (
    SELECT DATE(datetime_val) as "date", wf_sent, wf_view, ROUND((CAST(wf_view AS DOUBLE)/CAST(wf_sent AS DOUBLE) * 100),2) AS open_ratio
    FROM(SELECT COUNT_IF(utm_medium LIKE 'sent%') AS "wf_sent", COUNT_IF(utm_medium LIKE '%view%') AS "wf_view", MIN(datetime_val) as datetime_val FROM hive.{ims_app_id}.sender_log WHERE (DATE(datetime_val) = DATE(NOW()) + INTERVAL '-7' DAY) {query_part}))
    SELECT * FROM wf_sent_0 UNION ALL SELECT * FROM wf_sent_1 UNION ALL SELECT * FROM wf_sent_2 UNION ALL SELECT * FROM wf_sent_3 UNION ALL SELECT * FROM wf_sent_4 UNION ALL SELECT * FROM wf_sent_5 UNION ALL SELECT * FROM wf_sent_6 UNION ALL SELECT * FROM wf_sent_7 ORDER BY "date" desc
    """.format(ims_app_id = app_id, query_part = wf_idx_query_part)
    a = Presto()
    presto = a.presto_cursor
    presto.execute(workflow_sent_cnt_sql)
    for send_log in presto.fetchall():
        sent_cnt_agg.append(send_log)
    return sent_cnt_agg

# ==============================| system_queries for maintenance purpose! |===================================
def server_check():
    URL = 'http://superset.samantha.world:8283/event/'
    response = requests.get(URL)
    return (response.status_code)

def main_node_cnt():
    main_node_cnt = """
    SELECT count(*) FROM system.runtime.nodes where state = 'active'
    """
    a = Main_presto()
    presto = a.main_presto_cursor
    presto.execute(main_node_cnt)
    result = presto.fetchone()
    return result

def main_db_check():
    sql_db_check = """
    SELECT 'OK' AS "status" FROM mysql.mmc.ims_workflows
    """
    a = Main_presto()
    presto = a.main_presto_cursor
    presto.execute(sql_db_check)
    result = presto.fetchone()
    return result

def main_running_queries():
    sql_running_queries = """
    SELECT count(*) FROM system.runtime.queries
    WHERE state = 'RUNNING'
    """
    a = Main_presto()
    presto = a.main_presto_cursor
    presto.execute(sql_running_queries)
    result = presto.fetchone()
    return result

def main_runtime_check():
    sql_runtime_check = """
    SELECT COUNT(DATE_DIFF('minute',started,last_heartbeat)) as runtime
    FROM (SELECT started, last_heartbeat FROM system.runtime.queries WHERE state = 'RUNNING')
    WHERE date_diff('minute',started,last_heartbeat) > 30
    """
    a = Main_presto()
    presto = a.main_presto_cursor
    presto.execute(sql_runtime_check)
    runtime_cnt = presto.fetchall()[0][0]
    return runtime_cnt

def ds_db_check():
    sql_db_check = """
    SELECT 'OK' AS "status" FROM mysql.mmc.ims_workflows
    """
    a = Presto()
    presto = a.presto_cursor
    presto.execute(sql_db_check)
    result = presto.fetchone()
    return result

def ds_node_cnt():
    ds_node_cnt = """
    SELECT count(*) FROM system.runtime.nodes where state = 'active'
    """
    a = Presto()
    presto = a.presto_cursor
    presto.execute(ds_node_cnt)
    result = presto.fetchone()
    return result

def ds_running_queries():
    sql_running_queries = """
    SELECT count(*) FROM system.runtime.queries
    WHERE state = 'RUNNING'
    """
    a = Presto()
    presto = a.presto_cursor
    presto.execute(sql_running_queries)
    result = presto.fetchone()
    return result

def ds_runtime_check():
    sql_runtime_check = """
    SELECT COUNT(DATE_DIFF('minute',started,last_heartbeat)) as runtime
    FROM (SELECT started, last_heartbeat FROM system.runtime.queries WHERE state = 'RUNNING')
    WHERE date_diff('minute',started,last_heartbeat) > 30
    """
    a = Presto()
    presto = a.presto_cursor
    presto.execute(sql_runtime_check)
    runtime_cnt = presto.fetchall()[0][0]
    return runtime_cnt



# def sender_url_builder (ims_app_id,status):
#     sql_sender = """
#     SELECT
#     wf_status, workflow_name,content_title,dc_contents,send_type,
#     CONCAT('https://view-dev.uneedcomms.com/ims/',ims_domain_idx,'?template_json_idx=', s.template_idx,'&content_idx=',content_idx,'&landing_type=',send_type) AS url,
#     CONCAT('https://view-dev.uneedcomms.com/ims/',ims_domain_idx,'?template_json_idx=61','&content_idx=',content_idx,'&landing_type=',send_type,'&test=true') AS image_url
#     -- ARRAY_AGG(trim(url))
#     FROM (
#         SELECT
#             w.workflow_idx AS workflow_idx , w.workflow_name AS workflow_name, w.status AS wf_status, w.ims_domain_idx AS ims_domain_idx, s.template_idx AS template_idx, s.content_idx AS content_idx,
#             CASE WHEN s.touchpoint_code LIKE '%001%' THEN 'sms' WHEN s.touchpoint_code LIKE '%001%' THEN 'sms' WHEN s.touchpoint_code LIKE '%002%' THEN 'email' WHEN s.touchpoint_code LIKE '%003%' THEN 'kakao' WHEN s.touchpoint_code LIKE '%004%' THEN 'app' END AS send_type,
#             s.content_title AS content_title, s.dc_contents AS dc_contents
#         FROM mysql.mmc.ims_workflows w
#         INNER JOIN
#             (SELECT ims_domain_idx, ims_app_id 
#             FROM mysql.mmc.ims_domains) d 
#             ON d.ims_domain_idx = w.ims_domain_idx
#         INNER JOIN
#             (SELECT CAST(ims_domain_idx AS VARCHAR) AS ims_domain_idx, CAST(template_idx AS VARCHAR) AS template_idx,
#                 CAST(content_idx AS VARCHAR) AS content_idx, CAST(touchpoint_code AS VARCHAR) AS touchpoint_code,
#                 workflow_idx, content_title, dc_contents
#             FROM mysql.mmc.ims_sender_contents) s
#             ON w.workflow_idx = s.workflow_idx
#         WHERE
#           -- pinksisly & active 를 변동값으로 받아오기
#             (d.ims_app_id like '{}%') AND
#             (w.status like '%{}%')
#         )
#     -- WHERE
#     --   (url IS NOT NULL)
#       """

#     presto.execute(sql_sender)
#     for sen in presto.fetchall():
#         print(sen)




# =================================================| data analysis! |=================================================

def data_analysis(category,app_id,*text_input):
    host_info = show_hosting(app_id)
    # ------------| 회원 컬럼명(fd_12345) 가져오는 쿼리 |------------
    if category == "user_table_header":
        get_data_table_sql = '''
            SELECT data_table_idx
            FROM mysql.mmc.data_grabber_data_tables
            WHERE 
                (ims_app_id like '%{}%')
                and (data_set like '%user%')
                and (data_table like '{}_user_all')
        '''.format(app_id,host_info)
        a = Presto()
        presto = a.presto_cursor
        presto.execute(get_data_table_sql)
        data_table_idx_result = presto.fetchone()[0]

        get_table_header_sql = '''
            SELECT max_by(csv_header_data,"reg_date")
            FROM mysql.mmc.data_grabber_csv_headers
            WHERE data_table_idx={}
            '''.format(data_table_idx_result)
        a = Presto()
        presto = a.presto_cursor
        presto.execute(get_table_header_sql)
        headers = presto.fetchone()[0]
        return headers
        pass
    # ------------| 주문 컬럼명 (fd_12345) 가져오는 쿼리 |------------
    elif category == "order_table_header":
        get_data_table_sql = '''
            SELECT data_table_idx
            FROM mysql.mmc.data_grabber_data_tables
            WHERE 
                (ims_app_id like '%{}%')
                and (data_set like '%order%')
                and (data_table like '{}_order_pay_date_d_1')
        '''.format(app_id,host_info)
        a = Presto()
        presto = a.presto_cursor
        presto.execute(get_data_table_sql)
        data_table_idx_result = presto.fetchone()[0]

        get_table_header_sql = '''
            SELECT max_by(csv_header_data,"reg_date")
            FROM mysql.mmc.data_grabber_csv_headers
            WHERE data_table_idx={}
            '''.format(data_table_idx_result)
        a = Presto()
        presto = a.presto_cursor
        presto.execute(get_table_header_sql)
        headers = presto.fetchone()[0]
        return headers
        pass
    # ------------| 상품 컬럼명 (fd_12345) 가져오는 쿼리 |------------
    elif category == "product_table_header":
        get_data_table_sql = '''
            SELECT data_table_idx
            FROM mysql.mmc.data_grabber_data_tables
            WHERE 
                (ims_app_id like '%{}%')
                and (data_set like '%product%')
                and (data_table like '{}_product_all')
        '''.format(app_id,host_info)
        a = Presto()
        presto = a.presto_cursor
        presto.execute(get_data_table_sql)
        data_table_idx_result = presto.fetchone()[0]

        get_table_header_sql = '''
            SELECT max_by(csv_header_data,"reg_date")
            FROM mysql.mmc.data_grabber_csv_headers
            WHERE data_table_idx={}
            '''.format(data_table_idx_result)
        a = Presto()
        presto = a.presto_cursor
        presto.execute(get_table_header_sql)
        headers = presto.fetchone()[0]
        return headers
        pass

    # ------------| 회원 관련 종합 리포트 메이크샵 & 카페 24 |------------
    elif category == "user_report":
        data_user_id = text_input[0][0]
        data_user_join = text_input[0][1]
        data_user_login = text_input[0][2]
        data_user_sms = text_input[0][3]
        data_user_email = text_input[0][4]
        if host_info == "mk":
            user_insight_sql = '''
            WITH
            user_set AS (
            SELECT
              {user_id} as "유저아이디", LOWER(MAX_BY({user_sms},"time")) AS "SMS수신동의", LOWER(MAX_BY({user_email},"time")) AS "이메일수신동의",
              MAX_BY({login},"time") AS "로그인일자", MAX_BY({user_join},"time") AS "회원가입일자", MAX_BY(datetime_val, "time") AS datetime_val
            FROM
                (SELECT {user_id}, {user_sms}, {user_email}, {login}, {user_join}, datetime_val, "time" FROM hive.{ims_app_id}.user_{hosting}_user_all WHERE DATE(datetime_val) > DATE(now())+interval '-2' DAY)
            WHERE
              ({login} LIKE '%/%') AND ({user_join} LIKE '%/%') AND (DATE_PARSE(split_part({login},' ',1),'%Y/%m/%d') > (DATE(NOW())+ INTERVAL '-366' DAY)) 
            GROUP BY {user_id}
            ),
            user_cnt AS (SELECT COUNT("유저아이디") AS total_user_cnt FROM user_set),
            sms_cnt AS (SELECT COUNT("SMS수신동의") AS sms_user_cnt FROM user_set WHERE ("SMS수신동의" = 't' or "SMS수신동의" = 'y' or "SMS수신동의" = 'true' or "SMS수신동의" = 'yes')),
            email_cnt AS (SELECT COUNT("이메일수신동의") AS email_user_cnt FROM user_set WHERE ("이메일수신동의" = 't' or "이메일수신동의" = 'y' or "이메일수신동의" = 'true' or "이메일수신동의" = 'yes')),
            recent_visit AS (SELECT COUNT("유저아이디") AS recent_visit_cnt FROM user_set WHERE (DATE_PARSE(split_part("로그인일자",' ',1),'%Y/%m/%d') > (DATE(NOW())+ INTERVAL '-29' DAY)) AND (DATE_PARSE(split_part("회원가입일자",' ',1),'%Y/%m/%d') < (DATE(NOW())+ INTERVAL '-28' DAY))),
            recent_join AS (SELECT COUNT("유저아이디") AS recent_join_cnt FROM user_set WHERE (DATE_PARSE(split_part("회원가입일자",' ',1),'%Y/%m/%d') > (DATE(NOW())+ INTERVAL '-29' DAY))),
            sms_email_cnt AS (SELECT COUNT("SMS수신동의") AS sms_email_cnt FROM user_set WHERE ("SMS수신동의" = 't' or "SMS수신동의" = 'y' or "SMS수신동의" = 'true' or "SMS수신동의" = 'yes') AND ("이메일수신동의" = 't' or "이메일수신동의" = 'y' or "이메일수신동의" = 'true' or "이메일수신동의" = 'yes'))            
            SELECT * FROM user_cnt, sms_cnt, email_cnt, recent_visit, recent_join, sms_email_cnt
            '''.format(user_id = data_user_id, login = data_user_login, ims_app_id = app_id, hosting = host_info, user_sms = data_user_sms, user_email = data_user_email, user_join = data_user_join)
            a = Presto()
            presto = a.presto_cursor
            presto.execute(user_insight_sql)
            user_insight = (presto.fetchone())
            return user_insight
            pass

        elif host_info == "cf":
            user_insight_sql = '''
            WITH
            user_set AS (
            SELECT
              {user_id} as "유저아이디", LOWER(MAX_BY({user_sms},"time")) AS "SMS수신동의", LOWER(MAX_BY({user_email},"time")) AS "이메일수신동의",
              MAX_BY({login},"time") AS "로그인일자", MAX_BY({user_join},"time") AS "회원가입일자", MAX_BY(datetime_val, "time") AS datetime_val
            FROM (SELECT {user_id}, {user_sms}, {user_email}, {login}, {user_join}, datetime_val, "time" FROM hive.{ims_app_id}.user_{hosting}_user_all WHERE DATE(datetime_val) > DATE(now())+interval '-2' DAY)
            WHERE ({login} LIKE '%-%') AND ({user_join} LIKE '%-%') AND (DATE_PARSE(split_part({login},' ',1),'%Y-%m-%d') > (DATE(NOW())+ INTERVAL '-366' DAY))
            GROUP BY {user_id}),
            user_cnt AS (SELECT COUNT("유저아이디") AS total_user_cnt FROM user_set),
            sms_cnt AS (SELECT COUNT("SMS수신동의") AS sms_user_cnt FROM user_set WHERE ("SMS수신동의" = 't' or "SMS수신동의" = 'y' or "SMS수신동의" = 'true' or "SMS수신동의" = 'yes')),
            email_cnt AS (SELECT COUNT("이메일수신동의") AS email_user_cnt FROM user_set WHERE ("이메일수신동의" = 't' or "이메일수신동의" = 'y' or "이메일수신동의" = 'true' or "이메일수신동의" = 'yes')),
            recent_visit AS (SELECT COUNT("유저아이디") AS recent_visit_cnt FROM user_set WHERE (DATE_PARSE(split_part("로그인일자",' ',1),'%Y-%m-%d') > (DATE(NOW())+ INTERVAL '-29' DAY)) AND (DATE_PARSE(split_part("회원가입일자",' ',1),'%Y-%m-%d') < (DATE(NOW())+ INTERVAL '-28' DAY))),
            recent_join AS (SELECT COUNT("유저아이디") AS recent_join_cnt FROM user_set WHERE (DATE_PARSE(split_part("회원가입일자",' ',1),'%Y-%m-%d') > (DATE(NOW())+ INTERVAL '-29' DAY))),
            sms_email_cnt AS (SELECT COUNT("SMS수신동의") AS sms_email_cnt FROM user_set WHERE ("SMS수신동의" = 't' or "SMS수신동의" = 'y' or "SMS수신동의" = 'true' or "SMS수신동의" = 'yes') AND ("이메일수신동의" = 't' or "이메일수신동의" = 'y' or "이메일수신동의" = 'true' or "이메일수신동의" = 'yes'))
            SELECT * FROM user_cnt, sms_cnt, email_cnt, recent_visit, recent_join,sms_email_cnt
            '''.format(user_id = data_user_id, login = data_user_login, ims_app_id = app_id, hosting = host_info, user_sms = data_user_sms, user_email = data_user_email, user_join = data_user_join)
            a = Presto()
            presto = a.presto_cursor
            presto.execute(user_insight_sql)
            user_insight = (presto.fetchone())
            return user_insight
            pass
        else:
            return "카페24, 메이크샵을 제외한 호스팅은 현재 지원되지 않습니다."



    # ------------| 주문 관련 종합 리포트 메이크샵 & 카페 24 |------------
    elif category == "order_report":
        data_user_id = text_input[0][0]
        data_user_join = text_input[0][1]
        data_user_login = text_input[0][2]
        data_user_sms = text_input[0][3]
        data_user_email = text_input[0][4]

    # ------------| 일간 페이지 뷰 수 |------------
    elif category == "page_view":
        page_view_sql = '''
        WITH
        pv_1 as (SELECT *,(CAST(pv_1_day AS DOUBLE) / CAST(uv_1_day AS DOUBLE )) AS pv_per_person 
        FROM (SELECT count(datetime_val) as "pv_1_day", count(DISTINCT smart_dw_sid) as "uv_1_day" FROM hive.{ims_app_id}.behavioral_page_view_web
        WHERE date(datetime_val) = date(now()) + interval '-1' day)),

        pv_2 as (SELECT *,(CAST(pv_2_day AS DOUBLE) / CAST(uv_2_day AS DOUBLE )) AS pv_per_person 
        FROM (SELECT count(datetime_val) as "pv_2_day", count(DISTINCT smart_dw_sid) as "uv_2_day" FROM hive.{ims_app_id}.behavioral_page_view_web
        WHERE date(datetime_val) = date(now()) + interval '-2' day)),

        pv_3 as (SELECT *,(CAST(pv_3_day AS DOUBLE) / CAST(uv_3_day AS DOUBLE )) AS pv_per_person 
        FROM (SELECT count(datetime_val) as "pv_3_day", count(DISTINCT smart_dw_sid) as "uv_3_day" FROM hive.{ims_app_id}.behavioral_page_view_web
        WHERE date(datetime_val) = date(now()) + interval '-3' day)),

        pv_4 as (SELECT *,(CAST(pv_4_day AS DOUBLE) / CAST(uv_4_day AS DOUBLE )) AS pv_per_person 
        FROM (SELECT count(datetime_val) as "pv_4_day", count(DISTINCT smart_dw_sid) as "uv_4_day" FROM hive.{ims_app_id}.behavioral_page_view_web
        WHERE date(datetime_val) = date(now()) + interval '-4' day)),

        pv_5 as (SELECT *,(CAST(pv_5_day AS DOUBLE) / CAST(uv_5_day AS DOUBLE )) AS pv_per_person 
        FROM (SELECT count(datetime_val) as "pv_5_day", count(DISTINCT smart_dw_sid) as "uv_5_day" FROM hive.{ims_app_id}.behavioral_page_view_web
        WHERE date(datetime_val) = date(now()) + interval '-5' day)),

        pv_6 as (SELECT *,(CAST(pv_6_day AS DOUBLE) / CAST(uv_6_day AS DOUBLE )) AS pv_per_person 
        FROM (SELECT count(datetime_val) as "pv_6_day", count(DISTINCT smart_dw_sid) as "uv_6_day" FROM hive.{ims_app_id}.behavioral_page_view_web
        WHERE date(datetime_val) = date(now()) + interval '-6' day)),

        pv_7 as (SELECT *,(CAST(pv_7_day AS DOUBLE) / CAST(uv_7_day AS DOUBLE )) AS pv_per_person 
        FROM (SELECT count(datetime_val) as "pv_7_day", count(DISTINCT smart_dw_sid) as "uv_7_day" FROM hive.{ims_app_id}.behavioral_page_view_web
        WHERE date(datetime_val) = date(now()) + interval '-7' day)),

        pv_8 as (SELECT *,(CAST(pv_8_day AS DOUBLE) / CAST(uv_8_day AS DOUBLE )) AS pv_per_person 
        FROM (SELECT count(datetime_val) as "pv_8_day", count(DISTINCT smart_dw_sid) as "uv_8_day" FROM hive.{ims_app_id}.behavioral_page_view_web
        WHERE date(datetime_val) = date(now()) + interval '-8' day)),

        pv_9 as (SELECT *,(CAST(pv_9_day AS DOUBLE) / CAST(uv_9_day AS DOUBLE )) AS pv_per_person 
        FROM (SELECT count(datetime_val) as "pv_9_day", count(DISTINCT smart_dw_sid) as "uv_9_day" FROM hive.{ims_app_id}.behavioral_page_view_web
        WHERE date(datetime_val) = date(now()) + interval '-9' day)),

        pv_10 as (SELECT *,(CAST(pv_10_day AS DOUBLE) / CAST(uv_10_day AS DOUBLE )) AS pv_per_person 
        FROM (SELECT count(datetime_val) as "pv_10_day", count(DISTINCT smart_dw_sid) as "uv_10_day" FROM hive.{ims_app_id}.behavioral_page_view_web
        WHERE date(datetime_val) = date(now()) + interval '-10' day)),

        pv_11 as (SELECT *,(CAST(pv_11_day AS DOUBLE) / CAST(uv_11_day AS DOUBLE )) AS pv_per_person 
        FROM (SELECT count(datetime_val) as "pv_11_day", count(DISTINCT smart_dw_sid) as "uv_11_day" FROM hive.{ims_app_id}.behavioral_page_view_web
        WHERE date(datetime_val) = date(now()) + interval '-11' day)),

        pv_12 as (SELECT *,(CAST(pv_12_day AS DOUBLE) / CAST(uv_12_day AS DOUBLE )) AS pv_per_person 
        FROM (SELECT count(datetime_val) as "pv_12_day", count(DISTINCT smart_dw_sid) as "uv_12_day" FROM hive.{ims_app_id}.behavioral_page_view_web
        WHERE date(datetime_val) = date(now()) + interval '-12' day)),

        pv_13 as (SELECT *,(CAST(pv_13_day AS DOUBLE) / CAST(uv_13_day AS DOUBLE )) AS pv_per_person 
        FROM (SELECT count(datetime_val) as "pv_13_day", count(DISTINCT smart_dw_sid) as "uv_13_day" FROM hive.{ims_app_id}.behavioral_page_view_web
        WHERE date(datetime_val) = date(now()) + interval '-13' day)),

        pv_14 as (SELECT *,(CAST(pv_14_day AS DOUBLE) / CAST(uv_14_day AS DOUBLE )) AS pv_per_person 
        FROM (SELECT count(datetime_val) as "pv_14_day", count(DISTINCT smart_dw_sid) as "uv_14_day" FROM hive.{ims_app_id}.behavioral_page_view_web
        WHERE date(datetime_val) = date(now()) + interval '-14' day))
        SELECT * FROM pv_1, pv_2, pv_3, pv_4, pv_5, pv_6, pv_7, pv_8, pv_9, pv_10, pv_11, pv_12, pv_13, pv_14
        '''.format(ims_app_id = app_id)
        a = Presto()
        presto = a.presto_cursor
        presto.execute(page_view_sql)
        page_view = (presto.fetchone())
        return page_view
        pass


    # ------------| 회원의 일간 페이지 뷰 수 |------------
    elif category == "user_page_view":
        user_page_view_sql = '''
        WITH
        user_d_1 AS (
        SELECT COUNT(user_id) as user_d_1 FROM(
            SELECT user_id FROM(
                SELECT arbitrary(a.user_id) as user_id,arbitrary(user_name) as user_name,MIN_BY(referrer, "time") as initial_referrer,behavioral_page_view_web.smart_dw_sid
                FROM hive.{ims_app_id}.behavioral_page_view_web
                INNER JOIN (SELECT t.smart_dw_sid as smart_dw_sid, user_id FROM mongodw.{ims_app_id}.user_mapping CROSS JOIN UNNEST(smart_dw_sids) AS t (smart_dw_sid)) a ON behavioral_page_view_web.smart_dw_sid = a.smart_dw_sid
                WHERE (DATE(datetime_val) = (DATE(now())+INTERVAL '-1' DAY))
                GROUP BY behavioral_page_view_web.smart_dw_sid)
            GROUP BY user_id
            )
        ),

        user_d_2 AS (
        SELECT COUNT(user_id) as user_d_2 FROM(
            SELECT user_id FROM(
                SELECT arbitrary(a.user_id) as user_id,arbitrary(user_name) as user_name,MIN_BY(referrer, "time") as initial_referrer,behavioral_page_view_web.smart_dw_sid
                FROM hive.{ims_app_id}.behavioral_page_view_web
                INNER JOIN (SELECT t.smart_dw_sid as smart_dw_sid, user_id FROM mongodw.{ims_app_id}.user_mapping CROSS JOIN UNNEST(smart_dw_sids) AS t (smart_dw_sid)) a ON behavioral_page_view_web.smart_dw_sid = a.smart_dw_sid
                WHERE (DATE(datetime_val) = (DATE(now())+INTERVAL '-2' DAY))
                GROUP BY behavioral_page_view_web.smart_dw_sid)
            GROUP BY user_id
            )
        ),

        user_d_3 AS (
        SELECT COUNT(user_id) as user_d_3 FROM(
            SELECT user_id FROM(
                SELECT arbitrary(a.user_id) as user_id,arbitrary(user_name) as user_name,MIN_BY(referrer, "time") as initial_referrer,behavioral_page_view_web.smart_dw_sid
                FROM hive.{ims_app_id}.behavioral_page_view_web
                INNER JOIN (SELECT t.smart_dw_sid as smart_dw_sid, user_id FROM mongodw.{ims_app_id}.user_mapping CROSS JOIN UNNEST(smart_dw_sids) AS t (smart_dw_sid)) a ON behavioral_page_view_web.smart_dw_sid = a.smart_dw_sid
                WHERE (DATE(datetime_val) = (DATE(now())+INTERVAL '-3' DAY))
                GROUP BY behavioral_page_view_web.smart_dw_sid)
            GROUP BY user_id
            )
        ),

        user_d_4 AS (
        SELECT COUNT(user_id) as user_d_4 FROM(
            SELECT user_id FROM(
                SELECT arbitrary(a.user_id) as user_id,arbitrary(user_name) as user_name,MIN_BY(referrer, "time") as initial_referrer,behavioral_page_view_web.smart_dw_sid
                FROM hive.{ims_app_id}.behavioral_page_view_web
                INNER JOIN (SELECT t.smart_dw_sid as smart_dw_sid, user_id FROM mongodw.{ims_app_id}.user_mapping CROSS JOIN UNNEST(smart_dw_sids) AS t (smart_dw_sid)) a ON behavioral_page_view_web.smart_dw_sid = a.smart_dw_sid
                WHERE (DATE(datetime_val) = (DATE(now())+INTERVAL '-4' DAY))
                GROUP BY behavioral_page_view_web.smart_dw_sid)
            GROUP BY user_id
            )
        ),

        user_d_5 AS (
        SELECT COUNT(user_id) as user_d_5 FROM(
            SELECT user_id FROM(
                SELECT arbitrary(a.user_id) as user_id,arbitrary(user_name) as user_name,MIN_BY(referrer, "time") as initial_referrer,behavioral_page_view_web.smart_dw_sid
                FROM hive.{ims_app_id}.behavioral_page_view_web
                INNER JOIN (SELECT t.smart_dw_sid as smart_dw_sid, user_id FROM mongodw.{ims_app_id}.user_mapping CROSS JOIN UNNEST(smart_dw_sids) AS t (smart_dw_sid)) a ON behavioral_page_view_web.smart_dw_sid = a.smart_dw_sid
                WHERE (DATE(datetime_val) = (DATE(now())+INTERVAL '-5' DAY))
                GROUP BY behavioral_page_view_web.smart_dw_sid)
            GROUP BY user_id
            )
        ),

        user_d_6 AS (
        SELECT COUNT(user_id) as user_d_6 FROM(
            SELECT user_id FROM(
                SELECT arbitrary(a.user_id) as user_id,arbitrary(user_name) as user_name,MIN_BY(referrer, "time") as initial_referrer,behavioral_page_view_web.smart_dw_sid
                FROM hive.{ims_app_id}.behavioral_page_view_web
                INNER JOIN (SELECT t.smart_dw_sid as smart_dw_sid, user_id FROM mongodw.{ims_app_id}.user_mapping CROSS JOIN UNNEST(smart_dw_sids) AS t (smart_dw_sid)) a ON behavioral_page_view_web.smart_dw_sid = a.smart_dw_sid
                WHERE (DATE(datetime_val) = (DATE(now())+INTERVAL '-6' DAY))
                GROUP BY behavioral_page_view_web.smart_dw_sid)
            GROUP BY user_id
            )
        ),

        user_d_7 AS (
        SELECT COUNT(user_id) as user_d_7 FROM(
            SELECT user_id FROM(
                SELECT arbitrary(a.user_id) as user_id,arbitrary(user_name) as user_name,MIN_BY(referrer, "time") as initial_referrer,behavioral_page_view_web.smart_dw_sid
                FROM hive.{ims_app_id}.behavioral_page_view_web
                INNER JOIN (SELECT t.smart_dw_sid as smart_dw_sid, user_id FROM mongodw.{ims_app_id}.user_mapping CROSS JOIN UNNEST(smart_dw_sids) AS t (smart_dw_sid)) a ON behavioral_page_view_web.smart_dw_sid = a.smart_dw_sid
                WHERE (DATE(datetime_val) = (DATE(now())+INTERVAL '-7' DAY))
                GROUP BY behavioral_page_view_web.smart_dw_sid)
            GROUP BY user_id
            )
        ),

        user_d_8 AS (
        SELECT COUNT(user_id) as user_d_8 FROM(
            SELECT user_id FROM(
                SELECT arbitrary(a.user_id) as user_id,arbitrary(user_name) as user_name,MIN_BY(referrer, "time") as initial_referrer,behavioral_page_view_web.smart_dw_sid
                FROM hive.{ims_app_id}.behavioral_page_view_web
                INNER JOIN (SELECT t.smart_dw_sid as smart_dw_sid, user_id FROM mongodw.{ims_app_id}.user_mapping CROSS JOIN UNNEST(smart_dw_sids) AS t (smart_dw_sid)) a ON behavioral_page_view_web.smart_dw_sid = a.smart_dw_sid
                WHERE (DATE(datetime_val) = (DATE(now())+INTERVAL '-8' DAY))
                GROUP BY behavioral_page_view_web.smart_dw_sid)
            GROUP BY user_id
            )
        ),

        user_d_9 AS (
        SELECT COUNT(user_id) as user_d_9 FROM(
            SELECT user_id FROM(
                SELECT arbitrary(a.user_id) as user_id,arbitrary(user_name) as user_name,MIN_BY(referrer, "time") as initial_referrer,behavioral_page_view_web.smart_dw_sid
                FROM hive.{ims_app_id}.behavioral_page_view_web
                INNER JOIN (SELECT t.smart_dw_sid as smart_dw_sid, user_id FROM mongodw.{ims_app_id}.user_mapping CROSS JOIN UNNEST(smart_dw_sids) AS t (smart_dw_sid)) a ON behavioral_page_view_web.smart_dw_sid = a.smart_dw_sid
                WHERE (DATE(datetime_val) = (DATE(now())+INTERVAL '-9' DAY))
                GROUP BY behavioral_page_view_web.smart_dw_sid)
            GROUP BY user_id
            )
        ),

        user_d_10 AS (
        SELECT COUNT(user_id) as user_d_10 FROM(
            SELECT user_id FROM(
                SELECT arbitrary(a.user_id) as user_id,arbitrary(user_name) as user_name,MIN_BY(referrer, "time") as initial_referrer,behavioral_page_view_web.smart_dw_sid
                FROM hive.{ims_app_id}.behavioral_page_view_web
                INNER JOIN (SELECT t.smart_dw_sid as smart_dw_sid, user_id FROM mongodw.{ims_app_id}.user_mapping CROSS JOIN UNNEST(smart_dw_sids) AS t (smart_dw_sid)) a ON behavioral_page_view_web.smart_dw_sid = a.smart_dw_sid
                WHERE (DATE(datetime_val) = (DATE(now())+INTERVAL '-10' DAY))
                GROUP BY behavioral_page_view_web.smart_dw_sid)
            GROUP BY user_id
            )
        ),

        user_d_11 AS (
        SELECT COUNT(user_id) as user_d_11 FROM(
            SELECT user_id FROM(
                SELECT arbitrary(a.user_id) as user_id,arbitrary(user_name) as user_name,MIN_BY(referrer, "time") as initial_referrer,behavioral_page_view_web.smart_dw_sid
                FROM hive.{ims_app_id}.behavioral_page_view_web
                INNER JOIN (SELECT t.smart_dw_sid as smart_dw_sid, user_id FROM mongodw.{ims_app_id}.user_mapping CROSS JOIN UNNEST(smart_dw_sids) AS t (smart_dw_sid)) a ON behavioral_page_view_web.smart_dw_sid = a.smart_dw_sid
                WHERE (DATE(datetime_val) = (DATE(now())+INTERVAL '-11' DAY))
                GROUP BY behavioral_page_view_web.smart_dw_sid)
            GROUP BY user_id
            )
        ),

        user_d_12 AS (
        SELECT COUNT(user_id) as user_d_12 FROM(
            SELECT user_id FROM(
                SELECT arbitrary(a.user_id) as user_id,arbitrary(user_name) as user_name,MIN_BY(referrer, "time") as initial_referrer,behavioral_page_view_web.smart_dw_sid
                FROM hive.{ims_app_id}.behavioral_page_view_web
                INNER JOIN (SELECT t.smart_dw_sid as smart_dw_sid, user_id FROM mongodw.{ims_app_id}.user_mapping CROSS JOIN UNNEST(smart_dw_sids) AS t (smart_dw_sid)) a ON behavioral_page_view_web.smart_dw_sid = a.smart_dw_sid
                WHERE (DATE(datetime_val) = (DATE(now())+INTERVAL '-12' DAY))
                GROUP BY behavioral_page_view_web.smart_dw_sid)
            GROUP BY user_id
            )
        ),

        user_d_13 AS (
        SELECT COUNT(user_id) as user_d_13 FROM(
            SELECT user_id FROM(
                SELECT arbitrary(a.user_id) as user_id,arbitrary(user_name) as user_name,MIN_BY(referrer, "time") as initial_referrer,behavioral_page_view_web.smart_dw_sid
                FROM hive.{ims_app_id}.behavioral_page_view_web
                INNER JOIN (SELECT t.smart_dw_sid as smart_dw_sid, user_id FROM mongodw.{ims_app_id}.user_mapping CROSS JOIN UNNEST(smart_dw_sids) AS t (smart_dw_sid)) a ON behavioral_page_view_web.smart_dw_sid = a.smart_dw_sid
                WHERE (DATE(datetime_val) = (DATE(now())+INTERVAL '-13' DAY))
                GROUP BY behavioral_page_view_web.smart_dw_sid)
            GROUP BY user_id
            )
        ),

        user_d_14 AS (
        SELECT COUNT(user_id) as user_d_14 FROM(
            SELECT user_id FROM(
                SELECT arbitrary(a.user_id) as user_id,arbitrary(user_name) as user_name,MIN_BY(referrer, "time") as initial_referrer,behavioral_page_view_web.smart_dw_sid
                FROM hive.{ims_app_id}.behavioral_page_view_web
                INNER JOIN (SELECT t.smart_dw_sid as smart_dw_sid, user_id FROM mongodw.{ims_app_id}.user_mapping CROSS JOIN UNNEST(smart_dw_sids) AS t (smart_dw_sid)) a ON behavioral_page_view_web.smart_dw_sid = a.smart_dw_sid
                WHERE (DATE(datetime_val) = (DATE(now())+INTERVAL '-14' DAY))
                GROUP BY behavioral_page_view_web.smart_dw_sid)
            GROUP BY user_id
            )
        )
        SELECT * FROM user_d_1, user_d_2, user_d_3, user_d_4, user_d_5, user_d_6, user_d_7, user_d_8, user_d_9, user_d_10, user_d_11, user_d_12, user_d_13, user_d_14
        '''.format(ims_app_id = app_id)
        a = Presto()
        presto = a.presto_cursor
        presto.execute(user_page_view_sql)
        user_page_view = (presto.fetchone())
        return user_page_view
        pass


    # ------------| 회원의 등급 분석 |------------
    elif category == "user_grade":
        data_user_id = text_input[0][0]
        data_user_join = text_input[0][1]
        data_user_login = text_input[0][2]
        data_user_sms = text_input[0][3]
        data_user_email = text_input[0][4]
        data_user_grade = text_input[0][5]
        grade_list = list()

        if host_info == "cf":
            user_grade_url = '''
            SELECT count(user_id) as user_cnt, user_grade
            FROM (
                SELECT {user_id} AS user_id, max_by({user_grade},"time") as user_grade
                FROM hive.{ims_app_id}.user_{hosting}_user_all
                WHERE (date(datetime_val) > date(now()) + interval '-2' day) AND ({login} LIKE '%-%') AND ({user_join} LIKE '%-%') AND (DATE_PARSE(split_part({login},' ',1),'%Y-%m-%d') > (DATE(NOW())+ INTERVAL '-366' DAY))
                GROUP BY {user_id}
            )
            GROUP BY user_grade
            '''.format(user_id = data_user_id, user_grade = data_user_grade, login = data_user_login, ims_app_id = app_id, hosting = host_info, user_join = data_user_join)
            a = Presto()
            presto = a.presto_cursor
            presto.execute(user_grade_url)
            for grade in presto.fetchall():
                grade_list.append(grade)
            return grade_list
            pass

        elif host_info == "mk":
            user_grade_url = '''
            SELECT count(user_id) as user_cnt, user_grade            
            FROM (
                SELECT {user_id} AS user_id, max_by({user_grade},"time") as user_grade
                FROM hive.{ims_app_id}.user_{hosting}_user_all
                WHERE (date(datetime_val) > date(now()) + interval '-2' day) AND ({login} LIKE '%/%') AND ({user_join} LIKE '%/%') AND (DATE_PARSE(split_part({login},' ',1),'%Y/%m/%d') > (DATE(NOW())+ INTERVAL '-366' DAY))
                GROUP BY {user_id}
            )
            GROUP BY user_grade
            '''.format(user_id = data_user_id, user_grade = data_user_grade, login = data_user_login, ims_app_id = app_id, hosting = host_info, user_join = data_user_join)
            a = Presto()
            presto = a.presto_cursor
            presto.execute(user_grade_url)
            for grade in presto.fetchall():
                grade = str(grade)
                grade_list.append(grade)
            return grade_list
            pass
        else:
            return "카페24, 메이크샵을 제외한 호스팅은 현재 지원되지 않습니다."
            pass

    elif (category == "repurchase_web") or (category == "repurchase_app") or (category == "revisit_web") or (category == "revisit_app") or (category == "ims_repurchase_web") or (category == "ims_repurchase_app") or (category == "ims_revisit_web") or (category == "ims_revisit_app"):
        where_statement = ''
        if category == "repurchase_web":
            table = "behavioral_purchase_web"
        elif category == "repurchase_app":
            table = "behavioral_purchase_app"
        elif category == "revisit_web":
            table = "behavioral_page_view_web"        
        elif category == "revisit_app":
            table = "behavioral_page_view_app"

        elif category == "ims_repurchase_web":
            table = "behavioral_purchase_web"
            where_statement = """
                WHERE t.smart_dw_sid IN (
                SELECT DISTINCT(smart_dw_sid) smart_dw_sid
                FROM hive.{ims_app_id}.sender_log
                WHERE (MONTH(DATE(datetime_val)) = MONTH(DATE('{date}-01')))
                AND (utm_medium = 'sent')
                )
                """.format(date = text_input[0], ims_app_id = app_id)
        elif category == "ims_repurchase_app":
            table = "behavioral_purchase_app"
            where_statement = """
                WHERE t.smart_dw_sid IN (
                SELECT DISTINCT(smart_dw_sid) smart_dw_sid
                FROM hive.{ims_app_id}.sender_log
                WHERE (MONTH(DATE(datetime_val)) = MONTH(DATE('{date}-01')))
                AND (utm_medium = 'sent')
                )
                """.format(date = text_input[0], ims_app_id = app_id)
        elif category == "ims_revisit_web":
            table = "behavioral_page_view_web"
            where_statement = """
                WHERE t.smart_dw_sid IN (
                SELECT DISTINCT(smart_dw_sid) smart_dw_sid
                FROM hive.{ims_app_id}.sender_log
                WHERE (MONTH(DATE(datetime_val)) = MONTH(DATE('{date}-01')))
                AND (utm_medium = 'sent')
                )
                """.format(date = text_input[0], ims_app_id = app_id)  
        elif category == "ims_revisit_app":
            table = "behavioral_page_view_app"
            where_statement = """
                WHERE t.smart_dw_sid IN (
                SELECT DISTINCT(smart_dw_sid) smart_dw_sid
                FROM hive.{ims_app_id}.sender_log
                WHERE (MONTH(DATE(datetime_val)) = MONTH(DATE('{date}-01')))
                AND (utm_medium = 'sent')
                )
                """.format(date = text_input[0], ims_app_id = app_id)

        re_query=(
        "WITH "
        "user_mapping AS ("
            "SELECT t.smart_dw_sid AS sid, user_id "
            "FROM ("
                 "SELECT smart_dw_sids, user_id "
                 "FROM mongodw.{ims_app_id}.user_mapping"
                 ")"
            "CROSS JOIN UNNEST (smart_dw_sids) AS t (smart_dw_sid) "
            "{where}"
            "),"

        "purchase AS ("
            "SELECT "
                "datetime_val,"
                "smart_dw_sid "
            "FROM hive.{ims_app_id}.{target_table} "
            "WHERE (MONTH(DATE(datetime_val)) = MONTH(DATE('{date}-01'))) "
                  "AND (smart_dw_sid IN "
                      "(SELECT sid FROM user_mapping)) "
            "GROUP BY datetime_val, smart_dw_sid, smart_dw_session_id "
            ")"

        "SELECT"
        "visit_occurence_gap, "
        "COUNT(user_id) user_count "
        "FROM( "
          "SELECT "
          "user_id, "
          "visit_history, "
          "CASE"
            "WHEN visit_once = TRUE THEN '-1' "
            "WHEN visit_once = FALSE THEN "
            "TRY_CAST( "
              "DATE_DIFF( "
                "'day', "
                "TRY_CAST(ELEMENT_AT(ELEMENT_AT(visit_history,2),1) AS DATE), "
                "TRY_CAST(ELEMENT_AT(ELEMENT_AT(visit_history,1),1) AS DATE) "
              ") "
            "AS VARCHAR) "
            "ELSE NULL "
          'END AS "visit_occurence_gap" '
          "FROM( "
            "SELECT "
            "user_id, "
            "visit_history, "
            "TRY_CAST(CARDINALITY(visit_history) AS VARCHAR) LIKE '1' \"visit_once\" "
            "FROM( "
              "SELECT "
              "user_id,"
              "SLICE( "
                "ARRAY_SORT( "
                  "ARRAY_AGG( "
                    "ARRAY[datetime_val "
                      "] "
                    "), (x, y) -> IF(x < y, 1, IF(x = y, 0, -1) "
                  ")"
                ")"
              ',1,2) AS "visit_history" '
              "FROM( "
                "SELECT "
                "user_mapping.user_id user_id, "
                "datetime_val "
                "FROM purchase "
                "LEFT JOIN user_mapping ON user_mapping.sid = purchase.smart_dw_sid "
                "WHERE (purchase.smart_dw_sid IS NOT NULL OR purchase.smart_dw_sid <> '') AND "
                      "(user_mapping.user_id IS NOT NULL) "
                "GROUP BY user_id, datetime_val "
                "ORDER BY datetime_val "
              ")"
            "GROUP BY user_id "
            ")"
          ")"
        ")"
        "GROUP BY visit_occurence_gap "
        "ORDER BY CAST(visit_occurence_gap AS INT) "
        ).format(ims_app_id = app_id, date = text_input[0], target_table = table, where = where_statement)
        # print(re_query)
        re_list = list()
        a = Presto()
        presto = a.presto_cursor
        presto.execute(re_query)
        for reresult in presto.fetchall():
            reresult = str(reresult)
            re_list.append(reresult)
        return re_list
        pass

def send_coverage(category,app_id):
    if category == "send_view":
        send_coverage_sql = '''
        WITH
        sent_detail AS (
        SELECT COUNT (user_id) AS sent_user_cnt FROM (
            SELECT user_id FROM hive.{ims_app_id}.sender_log WHERE (utm_medium = 'sent') GROUP BY user_id)
        ),

        view_detail AS (
        SELECT COUNT(user_id) AS view_user_cnt FROM (
            SELECT user_id FROM hive.{ims_app_id}.sender_log WHERE (utm_medium like '%_view%') GROUP BY user_id)
        )

        SELECT
          sent_detail.sent_user_cnt AS sent_user_cnt,
          view_detail.view_user_cnt AS view_user_cnt
        FROM sent_detail, view_detail
        '''.format(ims_app_id = app_id)
        a = Presto()
        presto = a.presto_cursor
        presto.execute(send_coverage_sql)
        send_res = presto.fetchone()
        return send_res
        pass

def ims_in_active_detail(active_status):
    detail_list = list()
    detail_query = '''
    SELECT
        sdi.shop_name shop_name, sdi.domain "domain", trim(idomain.ims_app_id) ims_app_id, idomain.domain_id domain_id, idomain.end_date end_date,
        CASE WHEN bsl.status IS NULL THEN 'undefined' ELSE bsl.status END AS subscription_status
    FROM mysql.mmc.ims_domains idomain
    INNER JOIN (SELECT id,"domain", shop_name FROM mysql.uneedcomms_super_admin.super_domain_info) sdi
        ON cast(sdi.id as int) = cast(idomain.domain_id as int)
    LEFT JOIN mysql.uneedcomms_billing_center.bc_subscription_list bsl
        ON bsl.domain_id = idomain.domain_id
    WHERE (idomain.status like '{status}%') AND (ims_app_id not like 'tesilio%' AND ims_app_id not like 'bi_dummy%' AND ims_app_id not like 'bi_dummy%' AND ims_app_id not like 'default%' AND ims_app_id not like 'jay_testdb%' AND ims_app_id not like 'targetbook%' AND ims_app_id not like 'td%' AND ims_app_id not like 'test1%' AND ims_app_id not like 'uneedcomms' AND ims_app_id not like 'supersetds%' AND ims_app_id not like 'smartdw2%' AND ims_app_id not like 'eriktesthadoop%')
    ORDER BY end_date
    '''.format(status = active_status)
    a = Presto()
    presto = a.presto_cursor
    presto.execute(detail_query)
    for detail in presto.fetchall():
        detail_list.append(detail)
    return detail_list
    pass

    
