#-*-coding: utf-8
from django.conf import settings # import to call user object
from django.db import models # import for basic model formation
from django.db.models.signals import post_save, pre_save # using signals





class Hosting(models.Model):
    """
    호스팅사 정보를 대표하는 모델입니다.

    이 모델은 컴퍼니 오브젝트에서 참조를 하며, 모델 자체는 호스팅 스태프를 참조합니다.
    Stores a single blog entry, related to :model:`blog.Blog` and
    :model:`auth.User`.
	"""
    class Meta:
        verbose_name_plural		= 'Hostings'
        verbose_name			= 'Hosting'
        ordering				= ['name']

    name = models.CharField('호스팅사', null=False, blank=False, max_length=20,)
    website_url = models.URLField('웹사이트', null=True, blank=True, unique=True,
					help_text='http:// 혹은 https:// 를 포함하는 주소 입력',)
    tel = models.IntegerField('고객센터', null=True, blank=True, help_text='연락 가능한 대표번호 입력 (숫자만)',)

    def __str__(self):
        return self.name


class HostingStaff(models.Model):
    class Meta:
        verbose_name_plural		= 'Staffs'
        verbose_name			= 'Staff'
        ordering				= ['name']

    hosting_company = models.ForeignKey(Hosting, on_delete=models.DO_NOTHING, verbose_name='호스팅사', null=True, blank=True,)
    name = models.CharField('호스팅사 직원명', null=False, blank=False, max_length=20,)
    title = models.CharField('직책', null=True, blank=True, max_length=20,)
    email = models.EmailField('email', null=True, blank=True, max_length=255,)
    tel = models.IntegerField('직통번호', null=True, blank=True,)
    phone = models.IntegerField('휴대전화', null=True, blank=True,)
    def __str__(self):
        return f'{self.name} {self.title}'

class CompanyObject(models.Model):
    """
	컴퍼니 정보 영역입니다. 컨택 정보를 참조합니다.
	"""
    class Meta:
        verbose_name_plural		= 'Companies'
        verbose_name			= 'Company'
        ordering				= ['-id',]

    corporate_name = models.CharField('법인명', null=False, blank=False, max_length=150,
						help_text='"(주)", ".Inc", "co", "주식회사" 등을 제거한 순수 법인명',)
    
    company_name = models.CharField('업체명', null=False, blank=False,max_length=150,
						help_text='법인명과 같은 업체명인 경우 동일하게 입력',)

    address = models.CharField('업체 주소', null=True, blank=True, max_length=300,
						help_text='업체의 새도로 주소명',)

    website_url = models.URLField('도메인 주소', unique=True,
						help_text='http:// 혹은 https:// 를 포함하는 주소를 입력 (고유값)',)
	
    shop_cms = models.ForeignKey(Hosting, verbose_name='호스팅사 선택', null=False, blank=False,
						on_delete=models.DO_NOTHING,)

    invoice_email = models.EmailField('인보이스 이메일',  null=True, blank=True, max_length=255,
						help_text='이메일 형식에 맞추어 입력해 주시기 바랍니다',)

    business_number = models.IntegerField('사업자 등록번호', null=True, blank=True, 
						help_text='업체의 사업자 등록번호를 입력(숫자만)',)

    company_owner = models.ForeignKey(settings.AUTH_USER_MODEL,verbose_name='내부 담당자', 
						null=True, blank=True, on_delete=models.SET_NULL,
						default=4, help_text='업체 담당자를 선택해 주세요',)
    
    created_at = models.DateTimeField('최초 생성일', null=True, blank=True, auto_now_add=True,)

    updated_at = models.DateTimeField('최근 수정일', null=True, blank=True, auto_now=True,)

    columbus_script = models.CharField('스크립트 정보', null=True, blank=True, max_length=200,
						help_text='웹사이트 메인페이지 기준 사용중인 서비스 분석',)

    hub_business_number = models.CharField('hub_business_number', null=True, blank=True, max_length=100,)
    hub_company_id = models.IntegerField('HUB_company_id', null=True, blank=True,
						help_text='Archived data from HS | used to be a primary key in HS',)
    hub_postal_code = models.CharField('HUB_postal', null=True, blank=True, max_length=100,
						help_text='Archived data from HS',)
    hub_original_source_type = models.CharField('HUB_original_source',null=True, blank=True, max_length=200,
						help_text='Originally incoming channel',)
    hub_original_source_data1 = models.CharField('HUB_original_data1',null=True, blank=True, max_length=200,
						help_text='Originally incoming channel detail_1',)
    hub_original_source_data2 = models.CharField('HUB_original_data2',null=True, blank=True, max_length=200,
						help_text='Originally incoming channel detail_2',)
    hub_lifecycle_stage = models.CharField('lifecycle_stage',null=True, blank=True, max_length=100,
						help_text='lifecycle_stage',)
    hub_facebook_company_page = models.CharField('HUB_facebook_comapny_page', null=True, blank=True, max_length=200,
						help_text='Facebook Fan Page URL',)
    hub_domain_address = models.CharField('HUB_domain_address', null=True, blank=True, max_length=200,
						help_text='This is WebsiteURL named after domain address!',)

    def __str__(self):
    	return self.company_name

#    @staticmethod
#    def autocomplete_search_fields():
#        return 'company_name'


class ContactObject(models.Model):
    class Meta:
        verbose_name_plural		= 'Contacts'
        verbose_name			= 'Contact'
        ordering				= ['-created_at',]

    name = models.CharField('이름', null=False, blank=False, max_length=200, help_text='성을 포함하는 모든 이름을 입력',)
    email_opt_out = models.BooleanField('이메일 수신거부', null=True, blank=True, default=False, help_text='옵트 아웃 여부',)
    email = models.EmailField(help_text='이메일 형식에 맞춰 입력 (고유값)', null=False, blank=False, unique=True)
    mobile = models.CharField('휴대전화', null=False, blank=False, max_length=50, help_text='휴대전화 번호 입력("-" 미포함, 고유값)',)
    phone = models.CharField('유선전화', null=True, blank=True, max_length=50, help_text='유선전화("-" 미포함)',)
    related_company = models.ForeignKey(CompanyObject, null=True, blank=False, verbose_name='업체명', on_delete=models.PROTECT,
						help_text='회사 선택 (혹은 신규 생성)',)
    department = models.CharField('부서명', null=True, blank=True, max_length=100, help_text='부서명 입력',)
    title = models.CharField('직책', null=True, blank=True, max_length=100,)
    category = models.CharField('고객카테고리', null=True, blank=True,max_length=100,)
    created_at = models.DateTimeField('최초 생성일', null=True, blank=True, auto_now_add=True)
    updated_at = models.DateTimeField('최근 수정일', null=True, blank=True, auto_now=True)
    hub_contact_id = models.IntegerField('hub_contact_id', null=True, blank=True, help_text='contact id',)
    hub_first_name = models.CharField('hub_first_name', null=True, blank=True, max_length=200,)
    hub_last_name = models.CharField('hub_last_name', null=True, blank=True, max_length=200,)
    hub_ip_city = models.CharField('hub_ip_city', null=True, blank=True, max_length=200,)
    hub_number_of_pageviews = models.IntegerField('hub_number_of_pageviews', null=True, blank=True,)
    hub_company_name_title = models.CharField('checker', null=True, blank=True, max_length=400,)
    hub_original_source = models.CharField('hub_original_source', null=True, blank=True, max_length=200,)
    hub_orginal_source_drill_down_1 = models.CharField('hub_original_source_drill_down_1', null=True, blank=True, max_length=200,)
    hub_orginal_source_drill_down_2 = models.CharField('hub_original_source_drill_down_2', null=True, blank=True, max_length=200,)
    hub_ip_address = models.GenericIPAddressField('hub_ip_address', null=True, blank=True,)
    hub_associated_company = models.CharField('hub_associated_company', null=True, blank=True, max_length=200,)
    hub_associated_company_id = models.IntegerField('hub_associated_company_id', null=True, blank=True,)
    hub_user_category = models.CharField('hub_user_category', null=True, blank=True, max_length=200,)
    hub_lifecycle_stage = models.CharField('hub_lifecycle_stage', null=True, blank=True, max_length=200,)
    hub_shop_name = models.CharField('hub_shop_name', null=True, blank=True, max_length=200,)
    hub_job_title = models.CharField('hub_job_title', null=True, blank=True, max_length=200,)
    hub_website_url = models.CharField('hub_website_url', null=True, blank=True, max_length=200,)  
    hub_company_name = models.CharField('hub_company_name', null=True, blank=True, max_length=200,)

    def __str__(self):
        return f'{self.related_company} - {self.name} {self.title}'

class EventLog(models.Model):
    EVENT_TYPE = (
			('call','전화'),
			('message','메세지'),
			('meeting','미팅'),
	)
    MEMO_HIERARCHY = (
			('company','Company',),
			('contact','Contact',),
			('deal','Deal',)
	  )
	 
    class Meta:
        verbose_name_plural		= 'Events'
        verbose_name			= 'Event'
        ordering				= ['-id']

    event_related_company = models.ForeignKey(CompanyObject, null=True, blank=True,
			verbose_name='관련 company', on_delete=models.CASCADE)

    event_related_contact = models.ForeignKey(ContactObject, null=True, blank=True,
			verbose_name='관련 Contact', on_delete=models.SET_NULL)

    event_related_deal = models.ForeignKey('sales.DealObject', null=True, blank=True,
			verbose_name='관련 Deal', on_delete=models.SET_NULL)

    event_hierarchy = models.CharField(choices=MEMO_HIERARCHY, max_length=10,
			verbose_name='이벤트 입력 위치')

    event_type = models.CharField(choices=EVENT_TYPE, max_length=20, verbose_name='종류',)

    event_contents = models.TextField('활동 내용', null=True, blank=True,)

    event_occured = models.DateField('일자', null=True, blank=True)

    event_created_by = models.ForeignKey(settings.AUTH_USER_MODEL,verbose_name='내부 담당자',
                        null=True, blank=True, on_delete=models.SET_NULL,
                        help_text='업체 담당자를 선택해 주세요',)

    event_created_at = models.DateField('입력일', null=True, blank=True, auto_now_add=True)

    def __str__(self):
        return f'{self.event_type} - {self.event_related_company}'
