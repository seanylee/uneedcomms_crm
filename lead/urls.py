# URLConfiguration
#
# by Sean 2019.03



from django.urls import path, register_converter

from .views import (
	PostListView,
	PostDetailView,
	#LeadCreateView,
	leadcreate
  )

app_name = 'lead'

urlpatterns = [
    # path('', views.coming_soon_render, name='index'),
	# path('aboutus/', FormView.as_view(), name='aboutus'),
	# path('keepgrow/', FormView.as_view(), name='keepgrow')
	
    #path('form/submitted/',Form  name='formsubmit')
    path('form/', leadcreate, name='form'),
    path('posts/', PostListView.as_view(), name='postlist'),
    path('post/<int:id>/', PostDetailView.as_view(), name='postdetail'),
]

