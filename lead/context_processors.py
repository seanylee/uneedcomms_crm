# INSERT QUERYSET TO 'base.html'
# by Sean 2019.03
#
# The queryset returned by the function 'add_script_to_context'
# will be loaded when 'base.html' file is requested.
#
# Settings > Template > 'lead.context_processors.add_script_to_context'

from .models import Scripting

def add_script_to_context(request):
    qs = Scripting.objects.all()
    return {
		'scripts': qs
	}
