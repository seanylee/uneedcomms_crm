from django.template.response import TemplateResponse
from django.db.models import Count, Q
from django.utils.html import format_html  
from django.utils import timezone
from django.contrib import admin
from django.urls import path
from .models import Hosting, CompanyObject, ContactObject, HostingStaff, EventLog
from sales.models import DealObject
from service.models import TicketObject
from import_export.admin import ImportExportModelAdmin, ImportExportActionModelAdmin, ExportActionModelAdmin
from import_export import resources
from jet.admin import CompactInline
from bs4 import BeautifulSoup

from main.conf import slack_msg
from django.forms import Textarea
from django.db import models
#------------------------ HOSTING -----------------------
class HostingHostingStaffInline(admin.StackedInline):
    model					= HostingStaff
    verbose_name			= '호스팅사 직원'
    verbose_name_plural		= '호스팅사 직원들'
    extra					= 0
    max_num					= 20
    show_change_link		= True
    fields					= ('name', 'title', 'email', 'tel', 'phone',)
    readonly_fields			= ('name', 'title', 'email', 'tel', 'phone',)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, reuqest, obj=None):
        return False

@admin.register(Hosting)
class HostingAdmin(admin.ModelAdmin):
    list_display			= [host.name for host in Hosting._meta.fields]
    list_display_links		= ('id','name',)
    inlines					= (HostingHostingStaffInline,)

@admin.register(HostingStaff)
class HostingStaffAdmin(admin.ModelAdmin):
    list_display			= [staff.name for staff in HostingStaff._meta.fields]
    list_display_links		= ('id','hosting_company','name',)
    search_fields			= ('name','hosting_company','title',)
    list_filter				= ('hosting_company','title',)
    list_per_page			= 50

#---------------------- Contact Object -------------------
class CompanyObjectContactObjectInline(CompactInline):
    model					= ContactObject
    verbose_name			= '업제 직원'
    verbose_name_plural		= '업체 직원들'
    extra					= 0
    max_num					= 30
    show_change_link		= False
    fields					= ('contact_related_company', 'department', 'name', 'title', 'mobile', 'email',)
    readonly_fields			= ('contact_related_company',)
    autocomplete_field		= ('contact_related_company',)

    def contact_related_company(self, instance):
        return instance.related_company
    contact_related_company.allow_tags			= True
    contact_related_company.short_description	= "업체명 (자동 매핑)"

    def has_add_permission(self, request):
        return True
    def has_delete_permission(self, request, obj=None):
        return True


class ContactObjectTicketObjectInline(admin.TabularInline):
    model                   = TicketObject
    verbose_name            = 'To-Do'
    verbose_name_plural     = 'To-Do List'
    show_change_link        = True
    radio_fields            = {'ticket_status': admin.HORIZONTAL}
    empty_value_display     = '-'
    extra                   = 0
    fields                  = ('ticket_created_at','ticket_owner','ticket_product','ticket_category',
								'ticket_status','ticket_content',)
    readonly_fields         = ('ticket_contact','ticket_created_at','ticket_owner')
    formfield_overrides		= {models.TextField: {'widget': Textarea(attrs={'rows':2, 'cols':45})},}

    def has_add_permission(self, request):
        return True

    def has_delete_permission(self, request, obj=None):
        return False

class CompanyObjectEventLogInline(admin.TabularInline):
    model					= EventLog
    verbose_name			= '활동 로그'
    verbose_name_plural		= '활동 로그'
    extra					= 0
    raw_id_fields			= ('event_related_company',)
    fields					= ('event_created_at','event_hierarchy','event_related_contact',
								'event_related_deal','event_created_by','event_type','event_contents')
    readonly_fields			= ('event_created_at','event_hierarchy','event_related_contact',
								'event_related_deal','event_type','event_contents')
    formfield_overrides		= {models.TextField: {'widget': Textarea(attrs={'rows':2, 'cols':45})},}

    def company_related_event(self, instance):
        return instance.event_related_company
    company_related_event.allow_tags			= True
    company_related_event.short_description		= '회사'

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class ContactObjectEventLogInline(admin.TabularInline):
    model					= EventLog
    verbose_name			= '활동 로그'
    verbose_name_plural		= '활동 로그'
    extra					= 0
    raw_id_fields	        = ('event_related_contact',)
    fields                  = ('event_created_at','event_hierarchy','event_related_deal', 'event_created_by',
								'event_type','event_contents')
    readonly_fields         = ('event_created_at','event_hierarchy','event_related_deal')

    formfield_overrides = {models.TextField: {'widget': Textarea(attrs={'rows':2, 'cols':45})},}

    def contact_related_event(self, instance):
        return instance.related_contact
    contact_related_event.allow_tags			= True
    contact_related_event.short_description		= '직원'

    def company_related_event(self, instance):
        return instance.related_company
    company_related_event.allow_tags			= True
    company_related_event.short_description		= '업체'

    def get_queryset(self, request):
        return super(ContactObjectEventLogInline,self).get_queryset(request).filter(
					Q(event_hierarchy='contact') | Q(event_hierarchy='deal'))

    def has_delete_permission(self, request, obj=None):
        return False

class ContactObjectResource(resources.ModelResource):
    class Meta:
        model				= ContactObject
        report_skipped		= False
        fields				= ('name','email','mobile','phone','related_company','department',
								'title','category',)
        import_id_fields	= ('email',)

@admin.register(ContactObject)
class ContactObjectAdmin(admin.ModelAdmin):
#class ContactObjectAdmin(ExportActionModelAdmin, admin.ModelAdmin):
    resource_class			= ContactObjectResource
    list_display			= ('related_company','department','name','title','mobile_call','email','category',
								'created_at',)
    list_display_links		= ('department','name','title',)
    list_filter				= ('related_company','department','title','category',)
    search_fields			= ('name','phone','mobile','email','related_company__company_name')
    list_per_page			= 50
    empty_value_display		= '-'
    inlines					= (ContactObjectEventLogInline,ContactObjectTicketObjectInline)
    readonly_fields			= ('hub_company_name_title','hub_company_name','hub_shop_name',)
    fieldsets=(
	    ('고객 기본 정보', {
			'fields': ('related_company','department','name','title','email',
			  'mobile','phone','category',)
			}
		),
	)

    def mobile_call(self, ContactObject):
        if ContactObject.mobile:
            return format_html('<a herf="tel:{mob}">{mob}</a>', mob=ContactObject.mobile)
        else:
            return '-'
    mobile_call.allow_tags					= True
    mobile_call.short_description			= "휴대전화"

    def get_urls(self):
        urls				= super(ContactObjectAdmin, self).get_urls()
        contactobject_urls	= [path('document/', self.admin_site.admin_view(self.contact_events)),]
        return contactobject_urls + urls

    def contact_events(self, request):
        context = dict(
            self.admin_site.each_context(request),
            test_object = CompanyObject.objects.all(),
        )
        return TemplateResponse(request, "crm/contact_docs.html", context)

    def save_formset(self, request, form, formset, change):
        target = str(form)
        soup = BeautifulSoup(target)
        company_id = soup.find("select", {"id" : "id_related_company"}).find('option', {'selected':True})
        company_id_value = company_id['value']

        instances = formset.save(commit=False)
        for instance in instances:
            instance.event_created_by			= request.user
            instance.event_hierarchy			= 'contact'
            instance.event_related_company_id	= company_id_value
            instance.ticket_company_id			= company_id_value
            instance.ticket_owner				= request.user
            instance.save()
        formset.save_m2m()

#--------------------------- Company Object ----------------------------

class CompanyObjectTicketObjectInline(admin.TabularInline):
    model					= TicketObject
    verbose_name			= 'To-Do'
    verbose_name_plural		= 'To-Do List'
    show_change_link		= True
    radio_fields            = {'ticket_status': admin.HORIZONTAL}
    empty_value_display     = '-'
    extra					= 0
    fields					= ('ticket_created_at','ticket_owner','ticket_contact','ticket_product',
								'ticket_category','ticket_status','ticket_content',)
    readonly_fields			= ('ticket_contact','ticket_product','ticket_content','ticket_created_at','ticket_owner','ticket_category',)
    
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, reuqest, obj=None):
        return False

class CompanyObjectDealObjectInline(admin.TabularInline):
    model                   = DealObject
    verbose_name            = '거래 내역'
    verbose_name_plural     = '거래 내역들'
    show_change_link        = False
    empty_value_display     = '-'
    extra                   = 0
    fields                  = ('deal_status','deal_customer','deal_pipeline','price_pprint','deal_owner','successed_at',)
    readonly_fields         = ('deal_status','deal_pipeline','price_pprint','deal_customer','deal_owner','successed_at',)

    def has_delete_permission(self, reuqest, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    def price_pprint(self, instance):
        raw_price = int(round(instance.amount))
        raw_currency = instance.currency
        pretty_price = '{0:,d}'.format(raw_price)
        return (pretty_price + ' ' + raw_currency)
    price_pprint.allow_tags = True
    price_pprint.short_description = "금액(반올림)"

class CompanyObjectResource(resources.ModelResource):
    class Meta:
        model				= CompanyObject
        report_skipped		= False
        fields				= ('corporate_name', 'company_name', 'website_url', 'shop_cms','address', 
								'invoice_email', 'company_owner',)
        import_id_fields	= ('website_url',)

@admin.register(CompanyObject)
class CompanyObjectAdmin(admin.ModelAdmin):
#class CompanyObjectAdmin(ExportActionModelAdmin, admin.ModelAdmin):
    resource_class = CompanyObjectResource
    class Meta:
        model		= CompanyObject
        ordering	= ['-id']

    list_display			= ('corporate_name','company_name', 'firm_url','shop_cms','address',
								'related_deal_num', 'related_contact_num','company_owner',)
    list_display_links		= ('corporate_name','company_name',)
    list_filter				= ('shop_cms','company_owner',)
    search_fields			= ('corporate_name','company_name','invoice_email','website_url','address',)
    list_per_page			= 50
    inlines					= (
								CompanyObjectContactObjectInline,
								CompanyObjectEventLogInline,
								CompanyObjectDealObjectInline,
								CompanyObjectTicketObjectInline,
							 )
    empty_value_display		= '-'
    readonly_fields			= ('created_at','updated_at',)
    fieldsets = (
        ('업체 기본 정보', {
            'fields': ('corporate_name', 'company_name', 'address','shop_cms','website_url', 'invoice_email', 
						'company_owner', 'updated_at','created_at'),
			'description': "회사 정보"
            }
        ),
    )

    def firm_url(self, CompanyObject):
        return format_html('<a target="_blank" href="{url}">{url}</a>',url=CompanyObject.website_url)
    firm_url.allow_tags						= True
    firm_url.short_description				= "도메인 주소"

    def email(self, CompanyObject):
        return format_html('<a href="mailto:{addr}?subject=subject">{addr}</a>',addr=CompanyObject.invoice_email)
    email.allow_tags						= True
    email.short_description					= "인보이스 이메일"

    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['show_save_and_add_another'] = True
        return super(CompanyObjectAdmin, self).changeform_view(request, object_id, extra_context=extra_context)

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        queryset = queryset.annotate(
			_contacts	= Count('contactobject'),
			_deals		= Count('dealobject'),
			)
        return queryset

    def related_contact_num(self, obj):
        return obj._contacts
    related_contact_num.allow_tags          = True
    related_contact_num.short_description   = "업체 직원"
    related_contact_num.admin_order_field   = '_contacts'

    def related_deal_num(self, obj):
        return obj._deals
    related_deal_num.allow_tags				= True
    related_deal_num.short_description		= "거래 내역"
    related_deal_num.admin_order_field		= '_deals'

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for instance in instances:
            instance.event_created_by = request.user
            instance.event_hierarchy = 'company'
            instance.save()
        formset.save_m2m()


@admin.register(EventLog)
class EventLogAdmin(admin.ModelAdmin):
    class Meta:
        model = EventLog

    list_display = [c.name for c in EventLog._meta.fields]
    readonly_fields = [c.name for c in EventLog._meta.fields]

    def save_model(self, request, obj, form, change):
        obj.event_created_at = timezone.now
        obj.event_created_by = request.user
        super().save_model(request, obj, form, change)
