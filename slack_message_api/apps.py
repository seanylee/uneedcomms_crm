from django.apps import AppConfig


class SlackMessageApiConfig(AppConfig):
    name = 'slack_message_api'
