from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.db.models import Q

from .models import Lead, Post, Subscription
#from .forms import LeadForm

from django.views.generic import ListView, DetailView, CreateView


#Retrieve List
class PostListView(ListView):
    model = Post
    context_object_name = 'post_list'
    template_name = 'lead/newsletter_list.html'
    paginate_by = 6

    def get_queryset(self, *args, **kwargs):
        qs = Post.objects.all()
        query = self.request.GET.get("q", None)
        if query is not None:
            qs = qs.filter(
					Q(post_title__icontains=query) | 
					Q(post_preview__icontains=query) |
					Q(post_body__icontains=query) | 
					Q(post_registered_by__username__icontains=query) |
					Q(post_keywords__icontains=query)
				)
        return qs


class PostDetailView(DetailView):
    template_name = 'lead/newsletter_detail.html'
	
    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Post, pk=id_)

	
#class LeadCreateView(CreateView):
#    model = Lead
#    fields = ('lead_company_corporation','lead_company_name','lead_company_website_url',
#				'lead_contact_department','lead_contact_email','lead_contact_name',
#				'lead_contact_phone','lead_contact_title','lead_event_inquiry','lead_event_interests',
#				)
#    template_name = 'lead/lead_form.html'

# 반드시 수정하기!
#    success_url = reverse_lazy("lead:postlist")	

#    def get_success_url(self):
#        return reverse_lazy("lead:formsubmit", kwargs={"pk": int(self.object.pk)})


from django.shortcuts import render
from .forms import LeadForm

def leadcreate(request):
    form = LeadForm(request.POST or None)
        # form = LeadForm(request.POST)
    if form.is_valid():
        form = form.save(commit=False)
        form.lead_event_ipaddress = request.META['REMOTE_ADDR']
        form.lead_event_referrer = request.META['HTTP_REFERER']
        form.lead_event_user_agent = request.META['HTTP_USER_AGENT']
        form.lead_event_location = request.META['REQUEST_URI']
        form.save()

    context = {
			'form' : form
	}

    return render(request, 'lead/lead_form.html', context)


#index = ListView.as_view(model=Post, template_name='lead/posts.html')
#post_detail = DetailView.as_view(model=Post)


#CRUD SET

#post_new = CreateView.as_view(model=Post)

#post_edit = UpdateView.as_view(model=Post, fields='__all__')


#post_delete = DeleteView.as_view(model=Post, success_url=reverse_lazy('lead:index'))

#class PostDeleteView(DeleteView):
#    model = Post
#    success_url = reverse_lazy('lead:index')
#post_delete = PostDeleteView.as_view()
