from django.contrib import admin
from .models import Lead, Post, Subscription, Scripting
 
@admin.register(Lead)
class LeadAdmin(admin.ModelAdmin):
    class Meta:
        model = Lead

    search_fields		= ('lead_company_name','lead_contact_name',)
    list_filter			= ('lead_contact_department', 'lead_event_interests',)
    list_display		= ('lead_company_name','lead_contact_department','lead_contact_name',
							'lead_contact_title','lead_event_interests',
							'lead_contact_phone','lead_contact_email','lead_event_submitted_at',)
    readonly_fields		= [c.name for c in Lead._meta.fields]
    list_per_page		= 50

    def has_add_permission(self, request):
        return False
    def has_delete_permission(self, request, obj=None):
        return False
    
@admin.register(Subscription)
class SubscriptionAdmin(admin.ModelAdmin):
    class Meta:
        model = Subscription

    search_fields       = ('subs_company','subs_name',)
    list_filter         = ('subs_department', 'subs_interests',)
    list_display        = ('subs_company','subs_department','subs_name','subs_title','subs_interests',
                            'subs_phone','subs_email','subs_submitted_at',)
    readonly_fields     = [c.name for c in Subscription._meta.fields]
    list_per_page       = 50

    def has_add_permission(self, request):
        return False
    def has_delete_permission(self, request, obj=None):
        return False 

@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    class Meta:
        model = Post

    search_fields		= ('post_title','post_body',)
    list_filter			= ('post_type','post_publication','post_registered_by',)
    list_display		= ('post_title','post_type','post_publication','post_registered_by','post_created_at',)
    list_per_page		= 50
    readonly_fields		= ('post_created_at','post_updated_at',)
    fieldsets=(
        (
        '게시물 정보', {
            'fields': (
				'post_publication','post_type','post_registered_by','post_thumbnail',
				'post_title','post_preview','post_body', 'post_keywords','post_exposed_date',
				'post_created_at','post_updated_at',
                ),
            'description': "게시물 상세 정보"
            }
        ),
	)

@admin.register(Scripting)
class PostAdmin(admin.ModelAdmin):
    class Meta:
        model = Scripting

    list_display	= ('script_title','script_expose','script_created_at','script_updated_at',)
    list_per_page	= 50
    readonly_fields = ('script_created_at','script_updated_at',)
    fieldsets = (
        (
         '스크립팅', {
			'fields': (
				'script_expose','script_title','script_body','script_created_at','script_updated_at',  
				),
			'description': "다이나믹 스크립팅 기능을 통해 하단에 작성된 스크립트 코드는 “keepgrow.com” 의 모든 페이지에 반영 됩니다. 이때, 반드시 스크립트 태그를 함께 넣어주시기 바랍니다."
		 }
	  ),
	)

